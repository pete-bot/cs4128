

#include <iostream>
#include <cstdlib>
#include <cstring>

const int MAX_N = 2010;

int connections[MAX_N][MAX_N];
int nodes, edges;
int highest;

int parent[MAX_N];
int key[MAX_N];
bool mstSet[MAX_N];


void prim_min_span_tree(void);

int minKey(void);
void print_graph(void);
void printMST(int parent[], int n, int connections[MAX_N][MAX_N]);
void primMST(void);


int main(int argc, char* argv[])
{
	
	// init keys to inf, init mstSet to false (ie no keys are in set)
	for (int i = 0; i < MAX_N; i++){
		key[i] = 1e9+7, mstSet[i] = false;
	}

	highest = 0;
	std::cin >> nodes >> edges;
	
	int a, b, cost;
	for(int i = 1; i <= edges; ++i){
		std::cin >> a >> b >> cost;
		// check for secondary roads etc.
		if( (connections [a][b] != 0) && (connections[a][b] < cost) ) {continue;} 	
			connections[a][b] = cost;
			connections[b][a] = cost;			
	}

	//print_graph();

	primMST();


	return EXIT_SUCCESS;
}




// Function to construct and print MST for a graph represented using adjacency
// matrix representation
void primMST(void)
{

	// start at farm 1
	key[1] = 0;     
	parent[1] = 1;  

	for (int count = 1; count <= nodes; count++){
		
		// find node with lowest cost in visited set
		int u = minKey();
		mstSet[u] = true;
		
		for (int v = 1; v <= nodes; v++){
		  if (connections[u][v] && mstSet[v] == false &&  connections[u][v] <  key[v] ){
		     parent[v]  = u;
		     key[v] = connections[u][v];
		  }
		}
	}

	// print the constructed MST
	//printMST(parent, MAX_N, connections);

	std::cout << highest << std::endl;
}
 


// may not be fast enough
int minKey(void)
{
   // Initialize min value
   int min = 1e9+7, min_index;
 
   for (int v = 1; v <= nodes; v++){
     if (mstSet[v] == false && key[v] < min ){
         min = key[v], min_index = v;
     }
   }
 	
	//printf("considering u:%d, cost:%d\n", u, key[min_index]);
	if(key[min_index] > highest){highest = key[min_index];}

    return min_index;
}


// A utility function to print the constructed MST stored in parent[]
void printMST(int parent[], int n, int connections[MAX_N][MAX_N])
{
   std::cout << "Edge   Weight" << std::endl;
   for (int i = 1; i <= nodes; i++){
      std::cout <<  parent[i] << " - " <<  i << ": " << connections[i][parent[i]] << std::endl;;
   }
}
 
void print_graph(void)
{
	const int PRINT_MAX = nodes;
	std::cout << "\t";
	for(int i = 1; i <= PRINT_MAX; ++i){ std::cout << i << "\t";}
	std::cout << std::endl;

	// print cost
	for(int i = 1; i <= PRINT_MAX; ++i){
		std::cout <<  i << "\t";
		for(int j = 1; j <= PRINT_MAX; ++j){
			std::cout << connections[j][i] << "\t";
		}
		std::cout << std::endl;

	}

}
