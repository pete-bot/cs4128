#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <algorithm>

#define pair_vec std::vector<std::pair<int, std::pair<int, int> > >

#define MAX_N 210

int parent[MAX_N];


void init_parent(void);
void print_edges(const pair_vec& edges);
int union_find(int n);
void union_join(int a, int b);
int kruskals(pair_vec& edges, int fields);


int main(int argc, char* argv[])
{

	int cases;
	scanf("%d", &cases);
	int case_count = 1;
	int fields, weeks, weight, start, end;
	pair_vec edges;

	while(cases--){
		edges.clear();
		printf("Case %d:\n", case_count);
		scanf("%d %d", &fields, &weeks);
		
		for(int i = 0; i < weeks; ++i){
			scanf("%d %d %d", &start, &end, &weight);
			std::pair<int,int> a(start, end);
			std::pair<int, std::pair<int, int> > b(weight, a);
			edges.push_back(b);
			
			if(i < fields-1){
				printf("-1\n");
				continue;
			} else {
				printf("%d\n", kruskals(edges, fields));
			}
		}
		//print_edges(edges);
		++case_count;
	}

	return EXIT_SUCCESS;
}


int kruskals(pair_vec& edges, int fields)
{
	init_parent();
	std::sort(edges.begin(), edges.end());

	int min = 0, count = 0;
	int r = -1;
	for(int i = 0; i < edges.size(); ++i){

		//printf("%d %d\n", edges[i].second.first, edges[i].second.second);
		int a_par = union_find(edges[i].second.first);
		int b_par = union_find(edges[i].second.second);

		//printf("a_par: %d, b_par: %d\n", a_par, b_par);
		// edges are already connnected.
		if(a_par == b_par){
		    r = i;
		    continue;
		
		} else {		// edge is not already in set
		 	//printf("parent[%d] = %d\n",a_par, b_par);
		 	parent[a_par] = b_par;
		 	min += edges[i].first; 
		 	count++;
		}
		// remove edge?
	}

	if(r != -1){edges.erase(edges.begin()+r);}

	if(count == fields-1 ){
		return min;
	} else {
		return -1;
	}

	return 0;
}


void init_parent(void)
{
	for(int i = 0; i < MAX_N; ++i){
		parent[i] = i;
	}
}


int union_find(int n)
{
	if(parent[n] == n) {return n;}
	parent[n] = union_find(parent[n]);
	return parent[n];
}


// needed?
void union_join(int a, int b)
{}

void print_edges(const pair_vec& edges)
{
	for(unsigned int i = 0; i < edges.size(); ++i){
		printf("%d, %d: %d\n",edges[i].second.first, 
			edges[i].second.second, edges[i].first);   
	}
}
