#include <cstdio>
#include <cstdlib>
#include <cstring>

#define MAX_N 100100

int tree[MAX_N];


int main(int argc, char* argv[])
{
	int edge_count;
	int node_count;
	int parent, child;
	int case_count = 1;
	while(true){
		edge_count = 0;
		node_count = 0;
		while(true){
			scanf("%d %d", &parent, &child);
			
			if( parent == 0 && child == 0) {break;}
			if( parent == -1 && child == -1) {return 0;}

			if(tree[child] == 0){
				++node_count;
				tree[child] = 1;
			}

			if(tree[parent] == 0){
				++node_count;
				tree[parent] = 1;
			}

			++edge_count;

		}

		
		if(edge_count == (node_count-1) || (edge_count == 0 && node_count == 0) ){
			printf("Case %d is a tree.\n", case_count);
		} else {
			printf("Case %d is not a tree.\n", case_count);
		}


		++case_count;
		memset(tree, 0, MAX_N);
	}

	

	return EXIT_SUCCESS;
}

 