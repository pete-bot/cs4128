
#include <cstdlib>
#include <iostream>

#define MAX_INT 2147483647

const int MAX_N = 2010;

int connections[MAX_N][MAX_N];
int nodes, edges;
int highest;


int key[MAX_N];   
bool mstSet[MAX_N];  


void bessie_traversal(void);
void prim_min_span_tree(void);

int minKey(int key[], bool mstSet[]);
void print_graph(void);
void printMST(int parent[], int n, int connections[MAX_N][MAX_N]);
void primMST(void);


int main(int argc, char* argv[])
{

	bessie_traversal();

	return EXIT_SUCCESS;
}

void bessie_traversal(void)
{
	std::cin >> nodes >> edges;
	
	int a, b, cost;
	for(int i = 1; i <= edges; ++i){
		std::cin >> a >> b >> cost;
		if( connections[a][b] != 0 && connections[a][b] < cost){continue;}
		connections[a][b] = cost;
		connections[b][a] = cost;
			
	}

	//print_graph();

	primMST();

}


// Function to construct and print MST for a graph represented using adjacency
// matrix representation
void primMST(void)
{
	// init keys to inf, init mstSet to false (ie no keys are in set)
	for (int i = 0; i < MAX_N; i++){
		key[i] = MAX_INT, mstSet[i] = false;
	}

	// start at farm 1
	key[1] = 0;     
	//parent[1] = 1;  

	for (int count = 1; count <= nodes; count++){
		
		// find node with lowest cost in visited set
		int u = minKey(key, mstSet);
		mstSet[u] = true;

		//printf("considering u:%d, cost:%d\n", u, key[u]);
		if(key[u] > highest){highest = key[u];}

		for (int v = 1; v <= nodes; v++){
		  if (connections[u][v] && mstSet[v] == false && connections[u][v] <  key[v]){
		     //parent[v]  = u;
		     key[v] = connections[u][v];
		  }
		}
	}

	// print the constructed MST
	//printMST(parent, MAX_N, connections);

	std::cout << highest << std::endl;
}

// may not be fast enough
int minKey(int key[], bool mstSet[])
{
   // Initialize min value
   int min = MAX_INT, min_index;
 
   for (int v = 1; v <= nodes; v++)
     if (mstSet[v] == false && key[v] < min)
         min = key[v], min_index = v;
 
   return min_index;
}


void print_graph(void)
{
	const int PRINT_MAX = nodes;
	printf("\t");
	for(int i = 1; i <= PRINT_MAX; ++i){ printf("%d\t", i);}
	printf("\n");

	// print cost
	for(int i = 1; i <= PRINT_MAX; ++i){
		printf("%d\t", i);
		for(int j = 1; j <= PRINT_MAX; ++j){
			printf("%d\t", connections[j][i]);
		}
		printf("\n");

	}

}