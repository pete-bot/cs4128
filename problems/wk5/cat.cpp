#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <stack>
#include <fstream>
#include <istream>
#include <iostream>
#include <string>

const int ALPHABET_SIZE = 26;
const int OFFSET = 'a';

struct node{
    int in_str_count;
    std::vector<std::string> outgoing_strings;
    std::vector<bool>visited;
};



void dfs(node* graph, int node, int current_words ,int total_words, std::stack<std::string> &cat);
void init_graph(node *graph, std::stack<std::string> &cat);
void sort_graph(node *graph);
int get_start_node(node* graph);
bool is_valid(node * graph);
void print_result(int num_words, std::stack<std::string> &cat);


void print_graph(node *graph);


int main(int argc, char* argv[])
{

    // init global data here, ie, arrays etc.
    node graph[26];
    std::stack<std::string> cat;

    int cases;
    int num_words;
    std::cin >> cases;
    std::string input;
    for(int i = 0; i < cases; ++i){

        init_graph(graph, cat);


        std::cin >> num_words;

        for(int j = 0; j < num_words; ++j){
            std::cin >> input;
            graph[input[0]-OFFSET].outgoing_strings.push_back(input);
            graph[input[0]-OFFSET].visited.push_back(false);
            graph[input[input.size()-1]-OFFSET].in_str_count++;
        }

        if(!is_valid(graph)){
            std::cout << "***" << std::endl;
            continue;
        }

        // sort graph to create legicographic ordering
        sort_graph(graph);
        int start = get_start_node(graph);

        dfs(graph, start, 0, num_words, cat);

        print_result(num_words, cat);
        //print_graph(graph);
    }

    return EXIT_SUCCESS;
}


// need to create a set of boolean - visited
void dfs(node* graph, int node, int current_words ,int total_words, std::stack<std::string> &cat)
{
    // base case
    //if(current_words == total_words){return;}

    for(int i = 0; i < graph[node].outgoing_strings.size(); ++i){
        if(graph[node].visited[i] == true){
            continue;
        }
        graph[node].visited[i] = true;
        int size = graph[node].outgoing_strings[i].size() -1;
        int next_node = graph[node].outgoing_strings[i].at(size) - OFFSET;
        //std::cout<< "size: " << size << ", " << next_node << std::endl;
        dfs(graph, next_node, current_words+1, total_words, cat);
        cat.push(graph[node].outgoing_strings[i]);
    }
    // output node
}



bool is_valid(node * graph)
{
    int odd_count = 0, start_count = 0, end_count = 0 ;
    for(int i = 0; i < ALPHABET_SIZE; ++i){
        int degree = graph[i].in_str_count + graph[i].outgoing_strings.size();
        if( degree % 2 != 0) {
            //std::cout << "deg: "<< graph[i].in_str_count + graph[i].outgoing_strings.size() << std::endl;
            odd_count++;

            int temp = graph[i].outgoing_strings.size() - graph[i].in_str_count;
            if(temp == 1){
                start_count++;
            } else if ( temp == -1){
                end_count++;
            }
        }
    }

    if( (start_count > 1) || (end_count > 1) ) {return false;}
    //std::cout << "odd_count: " << odd_count << std::endl;
    if(odd_count > 2){
        return false;
    }
    return true;
}

bool is_ok(node * graph){
    int odd_count = 0, start_count = 0, end_count = 0 ;
    for(int i = 0; i < ALPHABET_SIZE; ++i){
        int degree = graph[i].in_str_count + graph[i].outgoing_strings.size();
        if( degree % 2 != 0) {
            //std::cout << "deg: "<< graph[i].in_str_count + graph[i].outgoing_strings.size() << std::endl;
            odd_count++;

            int temp = graph[i].outgoing_strings.size() - graph[i].in_str_count;
            if(temp == 1){
                start_count++;
            } else if ( temp == -1){
                end_count++;
            }
        }
    }

    if( (start_count > 1) || (end_count > 1) ) {return false;}
    //std::cout << "odd_count: " << odd_count << std::endl;
    if(odd_count > 2){
        return false;
    }
    return true;

}

int get_start_node(node* graph)
{
    int start = -1;
    for(int i = 0; i < ALPHABET_SIZE; ++i){
        if( (graph[i].outgoing_strings.size() - graph[i].in_str_count)  == 1){
            start = i;
            break;
        }
    }

    if(start == -1){
        for(int i = 0; i < ALPHABET_SIZE; ++i){
            if(graph[i].outgoing_strings.size() != 0){
                start = i;
                break;
            }
        }
    }

    //std::cout<< "start was: " << start << std::endl;
    return start;
}


void init_graph(node *graph, std::stack<std::string> &cat)
{
    for(int i = 0; i < ALPHABET_SIZE; ++i){
        graph[i].in_str_count = 0;
        graph[i].outgoing_strings.clear();
        graph[i].visited.clear();
    }

    while( !cat.empty()){
        cat.pop();
    }
}

void sort_graph(node *graph)
{
    for(int i = 0; i < ALPHABET_SIZE; ++i){

        std::sort(graph[i].outgoing_strings.begin(), graph[i].outgoing_strings.end());
    }
}


void print_result(int num_words, std::stack<std::string>& cat){

    if(cat.size() != num_words){std::cout << "***" << std::endl; return;}

    // need to check sanity here
    std::string s;
    int offset = 1;
    while(!cat.empty()){

        //if())
        if(offset == 2){
            if (cat.top()[0] != s[s.size()-offset]){
                std::cout << "***" << std::endl; return;
            }
            //std::cout << "first_char: "<<  << std::endl;
            //std::cout << "last_char: " <<  << std::endl;
        }

        s.append(cat.top());
        cat.pop();
        if(cat.size() != 0){
            s.append(".");
        }
        offset = 2;
    }


    std::cout << s << std::endl;

}

void print_graph(node *graph)
{

    for(int i = 0; i < ALPHABET_SIZE; ++i){
        if(graph[i].in_str_count + graph[i].outgoing_strings.size() == 0) {continue;}
        //std::cout << "in:" << graph[i].in_str_count<<std::endl;
        for(int j = 0; j < graph[i].outgoing_strings.size(); ++j){
            //	std::cout << graph[i].outgoing_strings[j] << ": ";
            //	std::cout << graph[i].visited[i] << std::endl;
        }
        //
    }

}
