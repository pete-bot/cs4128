#include <iostream>
#include <cstdio>
#include <cstring>
#include <string>
#include <vector>
#include <algorithm>
#include <queue>
#include <stack>

#define ALPHABET 26

using namespace std;

typedef vector<string> vstr;
typedef struct _vertex vertex;

struct _vertex {
  int insize;
  vstr out;
  vstr loop;
};

int end(string s) {
	return (s.at(s.size() - 1)) - 'a';
}


int main () {

	int t;
	scanf("%d\n", &t);
	for (int i = 0; i < t; ++i) {
		int n;
		scanf("%d\n", &n);

		bool invalid = false;
		vertex v[ALPHABET];
		int actualStart = -1;
		vstr stringList;
		
		populateVertices(v, n);
		findStartLetter(v, &actualStart, &invalid);
		findPath(v, stringList, n, actualStart, &invalid);
		printString(stringList, invalid);
	}

	return 0;
}

bool dfs(int node, vertex* v, vector<bool>* visited, int* visitedCount, vstr& stringList, int depth, int max) {
	
	int numChildren = visited[node].size();

	if (stringList.size() == max) {
		return true;
	}

	if (visitedCount[node] == numChildren) {
		return false;
	}

	vstr& children = v[node].out;

	for (int i = 0; i < numChildren; ++i) {

		if (!visited[node].at(i)) {

			// Add on!
			visited[node].at(i) = true;
			stringList.push_back(children[i]);
			visitedCount[node]++;

			if (dfs(end(children[i]), v, visited, visitedCount, stringList, depth + 1, max)) {
				return true;
			}

			// Retract!
			visited[node].at(i) = false;
			visitedCount[node]--;
			stringList.pop_back();
		}

	}
	return false;
}

void populateVertices(vertex* v, int n) {
	for (int j = 0; j < ALPHABET; j++) {
		v[j].insize = 0;
	}

	for (int j = 0; j < n; ++j) {
		char temp[50];
		scanf("%s\n", temp);
		int first = (temp[0]) - 'a';
		int last = (temp[strlen(temp) - 1]) - 'a';

		string s(temp);
		v[first].out.push_back(s);
		v[last].insize++;
	}

	// Sort all vectors internally
	for (int j = 0; j < ALPHABET; ++j) {
		sort(v[j].out.begin(), v[j].out.end());
	}
}

void findStartLetter(vertex* v, int* actualStart, bool* invalid) {
	// Find is a valid Euler path or tour, and find the start node
	int highestLetter = ALPHABET;
	int discrepency = 0;
	for (int j = 0; j < ALPHABET; ++j) {
		int in = v[j].insize;
		int out = (v[j].out.size());
		if (in != out) {
			discrepency++;
			if (in + 1 == out) {
				*actualStart = j;
			} else if (out + 1 != in) {
				*invalid = true;
				break;
			}
		}
		if (out > 0) {
			if (highestLetter > j) {
				highestLetter = j;
			}
		}
	}
	if (discrepency == 0 && highestLetter != ALPHABET) {
		*actualStart = highestLetter;
	} else if (discrepency != 2) {
		*invalid = true;
	}
}

void findPath(vertex* v, vstr& stringList, int n, int start, bool* invalid) {
	if (!(*invalid)) {
		vector<bool> visited[ALPHABET];
		int visitedCount[ALPHABET];
		for (int j = 0; j < ALPHABET; ++j) {
			visited[j] = vector<bool>(v[j].out.size(), 0);
			visitedCount[j] = 0;
		}

		*invalid = !dfs(start, v, visited, visitedCount, stringList, 0, n);
	}
}

void printString(vstr& stringList, bool invalid) {
	if (invalid) {
		cout << "***" << endl;
	} else {
		if (stringList.size() > 0) {
			cout << stringList.at(0);
			for (int i = 1; i < stringList.size(); ++i) {
				cout << "." << stringList.at(i);
			}
			cout << endl;
		}
	}
}
