#include <iostream>
#include <cstdlib>
#include <vector>
#include <cmath>

#define x first
#define y second
#define point std::pair<double,double>

#define RADIUS 2.5
#define DIAMETER 5.0

double getSquaredDistance(point a, point b);
int countChips(std::vector<point> chips, point pos);
point getPoint(point a, point b);

int main(int argc, char* argv[])
{
	std::vector<point> chips;
	double x = 0, y = 0;
	while(std::cin >> x >> y){
		chips.push_back(std::make_pair(x,y));
	}

	int chipCount = 0, tmp_count = 0;
	
	for( unsigned int topLeft = 0; topLeft < chips.size(); topLeft++ ){
		for( unsigned int bottomRight = 0; bottomRight < chips.size(); bottomRight++ ){
			
			point center = getPoint(chips[topLeft], chips[bottomRight]);
			//std::cout << "centerpoint " << center.x << ", " << center.y << std::endl; 
			tmp_count = countChips(chips, center);
			//std::cout << "chips: " << tmp_count << std::endl;
			if(tmp_count > chipCount){ chipCount = tmp_count;}
		
		}
	}

	//std::cout << "chips: " << chipCount << std::endl;
	std::cout << chipCount << std::endl; 

	return EXIT_SUCCESS;
}


double getSquaredDistance(point a, point b)
{
	double ans = sqrt((a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y)); 
	//std::cout << "dist: " << ans << std::endl;
	return  ans;
}


// need to check if there are chips within 2.5 cm of pos in any direction. 
int countChips(std::vector<point> chips, point pos)
{
	int chipCount = 0;

	// need to iterate over chip set, check if position is within 2.5 of pos.
	for(unsigned int i = 0; i < chips.size(); ++i){
		if( getSquaredDistance( chips[i], pos) <= RADIUS){
			//std::cout << "chip detected." << std::endl;
			++chipCount;
		}
	}	
	
	return chipCount;
}

// get circle with perimeter on point a, 'pointing' towards b
point getPoint(point a, point b)
{
	
	// std::cout << "==================================" << std::endl;
	// std::cout << a.x << "   " << b.x << std::endl;
	// std::cout << a.y << "   " << b.y << std::endl;

	point v = point((b.x-a.x),(b.y-a.y)); 
	if(v.x == 0 && v.y == 0){ return a; }
	
	// std::cout << "pt: " << b.x << "-" << a.x << "," << b.y << "-" << a.y << std::endl;
	// std::cout << "v: " << v.x << "," << v.y << std::endl;

	double magnitude = sqrt( v.x*v.x + v.y*v.y); 

	if(magnitude == 0) {magnitude = 1.0;}
	// std::cout << "mag: " << magnitude << std::endl;
	
	
	//if(v.x == 0) {v.x = 1;}
	//if(v.y == 0) {v.y = 1;}
	
	point unit(RADIUS*(v.x/magnitude) , RADIUS*(v.y/magnitude)); 
	//	std::cout << "unit: " << unit.x << "," << unit.y << std::endl;
	

	unit.x+=a.x; unit.y+=a.y;


	return unit;
}
