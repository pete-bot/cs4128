#include <iostream>
#include <cstdlib>

unsigned int countFives(unsigned int seed);

int main(int argc, char* argv[])
{

	int num_cases = 0;
	std::cin >> num_cases;

	int base = 0;
	for(int i = 0; i < num_cases; ++i){
		std::cin >> base;
		std::cout << countFives(base) << std::endl;; 
	}

	return EXIT_SUCCESS;
}

unsigned int countFives(unsigned int seed)
{
	unsigned int a = 0, b = 5, sum = 0;

	// similar to euclidean algorithm
	a = seed;
	while( a > 1 ){
		a /= 5;
		sum += a;
	}

	return sum;
}