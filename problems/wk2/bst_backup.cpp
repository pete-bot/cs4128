#include <iostream>
#include <cstdlib>


inline bool isEven( unsigned int a) {	return (a%2 == 0); }
bool isDoubling(unsigned int a);
unsigned int closestDoubling(unsigned int a);
unsigned int absolute( int a);

int main(int argc, char* argv[])
{
	int num_input = 0;
	std::cin >> num_input;
	unsigned int start = 0, left = 0, right = 0, correction = 0;
	unsigned int doubleNum = 0;

	// main loop
	for(int i = 0; i < num_input; ++i){
		std::cin >> start;
		
		//std::cout << "===================================================" << std::endl;
		//std::cout << "start: " << start << " -> ";// << std::endl;
		if( isEven(start)){
			left = right = start;
			//std::cout << "start: "<< start << " " << "L: " << left << ", " << "R: " << right << std::endl;
			//std::cout << "isdbl: " << isDoubling(start) << std::endl;

			if(isDoubling(start)){

				correction = start/2;
				doubleNum = start;
			} else {

				doubleNum = closestDoubling(start);
				correction = absolute(doubleNum - start)/2;
			}

			//std::cout << "doubling: " << doubleNum<< std::endl;
			//std::cout << "correction: " << correction << std::endl;
			
			while( isEven(left) ){
				
				left -=correction;
				right+=correction;

				//std::cout << "left: " << left << std::endl;
				//std::cout << "right: " << left << std::endl;

				correction/=2;
			}
			
			//std::cout << "L: " << left << ", " << "R: " << right << std::endl;
			std::cout << left  << " " << right << std::endl;
		} else {
			std::cout << start << " " << start << std::endl;
		}


	}

	return EXIT_SUCCESS;
}

unsigned int closestDoubling(unsigned int a)
{
	unsigned int doubleNum_lower = 1, doubleNum_upper = 1;
	int count = 0;

	while (doubleNum_lower*2 <= a){
		doubleNum_lower*=2;
		count++;
	}

	doubleNum_upper = doubleNum_lower*2;

	//std::cout << "upper: " << doubleNum_upper << ", " << "lower: " << doubleNum_lower << std::endl;

	if( absolute( a - doubleNum_lower) < absolute( a -doubleNum_upper ) ){ 
		return doubleNum_lower;
	} else {
		return doubleNum_upper;
	}
} 

unsigned int absolute( int a)
{
	return (a < 0 ? -1*a : a);
}

bool isDoubling(unsigned int a)
{
	while( a%2 == 0 && a > 1){
		a/=2;
	}
	return (a == 1);
}