#include <iostream>
#include <cstdlib>
#include <bitset>


inline bool isEven( unsigned int a) {	return (a%2 == 0); }

int main(int argc, char* argv[])
{
	int num_input = 0;
	std::cin >> num_input;
	unsigned int start = 0, left = 0, leftIso = 0, right = 0, correction = 0;

	// main loop
	for(int i = 0; i < num_input; ++i){
		std::cin >> start;
		left = start;
		right = start;
		
		if( isEven(start)){
			
			while(isEven(right)){
				right = right | (right-1);
			}

			leftIso = left & (-left);
			left -=  leftIso; 
			
			while(isEven(leftIso)){
				leftIso = leftIso>>1;
			}
			left += leftIso;

		} else {
			left = right = start;
		}
		
		std::cout << left  << " " << right << std::endl;
	}

	return EXIT_SUCCESS;
}
