// heehaa.cpp
// prog chals sol
// peter kydd

#include <iostream>
#include <cstdlib>
#include <iomanip>
#include <cmath>   

#define PI 3.14159265359

int main(int argc, char* argv[])
{
	int num_scenarios = 0;
	std::cin >> num_scenarios;
	std::cout << std::setprecision(3);
	std::cout.setf( std::ios::fixed, std:: ios::floatfield );

	// variables for problem.
	double R = 0, circumference = 0, diameter = 0, r = 0, theta = 0, sin_theta = 0;
	int n = 0;

	for(int count = 1; count <= num_scenarios; ++count){
		std::cout << "Scenario #" << count << ":" << std::endl;
		std::cin >> R >> n;		
		theta = PI/static_cast<double>(n);
		sin_theta = sin(theta);
		r = R*sin_theta/(1+sin_theta);
		std::cout <<  r << std::endl<<std::endl; 
	}

	return EXIT_SUCCESS;
}

