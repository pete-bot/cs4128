#include <iostream>
#include <cstdlib>
#include <cstdlib>
#include <vector>
#include <cmath>
#include <bitset>	
#include <string>


// get the m value for our n value
int getM(long long n);
bool isInteger(double a);

int main(int argc, char* argv[])
{

	long long n = 0;//, m = 0;

	while(std::cin >> n){
		if(n == 0) {break;}
		std::cout << getM(n) << std::endl;
		//std::cout <<n << "*" << m << ": " <<  n*m << std::endl;
		//std::cout << n*m <<std::endl;
		
	}

	return EXIT_SUCCESS;
}

int getM(long long n)
{
	double n_double = static_cast<double>(n);
	std::string b;
	double c = 0;
	double m = 0;
	//std::cout<<"testing: " << n << std::endl;
	
	for (long long i = 1; i < 1000; ++i){
		std::bitset<50> a(i);
		//std::cout << a.to_string() << std::endl; 
		
		c = std::atol(a.to_string().c_str());
		m = c/n_double;

		if( isInteger(m) ){
			//std::cout << c << "/" << n_double << " = " << c/n_double << std::endl;
		    //std::cout<< "n*c/n=" << c << std::endl;
		    return c;
		}
		// std::cout << a << std::endl;
	}


	
	return n;
}

bool isInteger(double a)
{
	return ((a-static_cast<int>(a)) == 0);
}

