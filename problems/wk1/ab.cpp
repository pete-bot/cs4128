// ab.cpp
// solution for A + B problem
// peter kydd pkydd@cse.unsw.edu.au

#include <iostream>
#include <cstdlib>

int main (int argc, char* argv[])
{
    int a,b;
    std::cin >> a >> b;
    std::cout << a+b << std::endl;

    return EXIT_SUCCESS;
}
