// square.cpp
// progchal wk1
// Peter Kydd 2016

#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>


int main(int argc, char* argv[])
{

	unsigned int n;
	std::cin >> n;

	std::vector< std::string> grid;
	
	std::string tmp;
	while(std::cin >> tmp ){
		grid.push_back(tmp);
	}

	std::cout << "Grid: " << std::endl;
	for (int i = 0; i < n; ++i ){
		std::cout << grid[i] << std::endl;
	}

	std::cout << std::endl << std::endl;
	std::cout << grid[4][2] << std::endl;


	return EXIT_SUCCESS;
}