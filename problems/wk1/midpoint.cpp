// square.cpp
// progchal wk1
// Peter Kydd 2016

#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <cmath>
#include <cassert>


// note coords in point are (x,y)
// the higher the Y value, the lower on the board

double getDistance(std::pair<int,int> a, std::pair<int,int> b);
int checkHorizontal( const std::pair<int, int>& a, const std::pair<int, int>& b );
int checkVertical( const std::pair<int, int>& a, const std::pair<int, int>& b) ;

std::pair<double, double> rotateLeft(std::pair<double, double> in);
std::pair<double, double> rotateRight(std::pair<double, double> in);
bool isInteger(double a);
bool isCell(std::pair<double, double> a);
bool inField(std::pair<int,int> a, int n);
bool isCow(std::vector<std::string > field, std::pair<int,int> a);
bool opCow(std::vector<std::string > field, std::pair<int,int> a);
int getArea(std::pair<int,int> a, std::pair<int,int> b);


int main(int argc, char* argv[])
{
	int area = 0;
	double currSide = 0;

	// take in n
	int n;
	std::cin >> n;
	std::vector<std::string> field;

	// sets aof cow coords
	std::vector<std::pair<int,int> > jSet;
	std::vector<std::pair<int,int> > bSet;
	
	std::string tmp;
	
	for (int i = 0; i < n; ++i ){
		std::cin >> tmp;	
		field.push_back(tmp);
		for (int j = 0; j < n; ++j){
			//std::cout << tmp[j] << " ";// << std::endl;
			// add coords for J set
			if(tmp[j] == 'J'){
				jSet.push_back(std::make_pair(j,i));
			} else if(tmp[j] == 'B'){
				bSet.push_back(std::make_pair(j,i));
			}

		}
		//std::cout << std::endl; 
	}

	
	for(unsigned int topLeft = 0; topLeft < jSet.size(); ++topLeft){
		for (unsigned int bottomRight = jSet.size()-1; topLeft < bottomRight; --bottomRight){
			if(currSide > getDistance(jSet[topLeft], jSet[bottomRight] )) {break;}
			
			std::pair<int,int> a = jSet[topLeft];			
			std::pair<int,int> b = jSet[bottomRight];
			
			// horizontal case
			if ( checkHorizontal(a, b) ){
				//std::cout << "checking horizontal" << std::endl;
				
				//std::cout << "hor. sq possibly found" << std::endl;
				int diff = abs(a.first - b.first);
				
				//std::cout << "diff: " << diff << std::endl;

				std::pair<int, int> a1(a.first,a.second+diff); 
				std::pair<int, int> a2(a.first,a.second-diff);  
				
				std::pair<int, int> b1(b.first,b.second+diff);
				std::pair<int, int> b2(b.first,b.second-diff); 

				
				//std::cout << "a1: " << a1.first << ", " << a1.second << std::endl;
				//std::cout << "b1: " << b1.first << ", " << b1.second << std::endl;
				//std::cout << "a2: " << a2.first << ", " << a2.second << std::endl;
				//std::cout << "b2: " << b2.first << ", " << b2.second << std::endl;
				

				// case where a1 and b1 are in field (the field is inverted)
				if(inField(a1, n)){
					//std::cout << "a1,b1 in field" << std::endl;
					if( (isCow(field, a1) || isCow(field, b1)) && !( opCow(field, a1) || opCow(field, b1) ) ){
						//std::cout << "a1 or b1 is cow" << std::endl;
						int diff2 = diff*diff;
						if(diff2 > area){
							
							//std::cout << 
							//	"updating area: a1, b1 satisfied. (need to check for opposing cow)" 
							//	<< std::endl;
							currSide = diff;
							area = diff2; 
						}
					}
				
				} else if( inField(a2, n)){
					//std::cout << "a2,b2 in field" << std::endl;
					if( (isCow(field, a2) || isCow(field, b2)) && !( opCow(field, a2) || opCow(field, b2))  ){
						int diff2 = diff*diff;
						if(diff2 > area){
							
							//std::cout << 
							//	"updating area: a1, b1 satisfied. (need to check for opposing cow)" 
							//	<< std::endl;
							
							area = diff2; 
						}
					}
				}
				
			// vertical case
			} else if( checkVertical(a,b) ){
				//std::cout << "checking vertical" << std::endl;
				
				//std::cout << "ver. sq possibly found" << std::endl;

				int diff = abs(a.second - b.second);
				
				//std::cout << "diff: " << diff << std::endl;

				std::pair<int, int> a1(a.first+diff,a.second); 
				std::pair<int, int> a2(a.first-diff,a.second);  
				
				std::pair<int, int> b1(b.first+diff,b.second);
				std::pair<int, int> b2(b.first-diff,b.second); 

				
				//std::cout << "a1: " << a1.first << ", " << a1.second << std::endl;
				//std::cout << "b1: " << b1.first << ", " << b1.second << std::endl;
				//std::cout << "a2: " << a2.first << ", " << a2.second << std::endl;
				//std::cout << "b2: " << b2.first << ", " << b2.second << std::endl;
				

				// case where a1 and b1 are in field (the field is inverted)
				if(inField(a1, n)){
					//std::cout << "a1,b1 in field" << std::endl;
					if( (isCow(field, a1) || isCow(field, b1)) && !( opCow(field, a1) || opCow(field, b1) ) ){
						//std::cout << "a1 or b1 is cow" << std::endl;
						int diff2 = diff*diff;
						if(diff2 > area){
							
							//std::cout << 
							//	"updating area: a1, b1 satisfied. (need to check for opposing cow)" 
							//	<< std::endl;
							currSide = diff;
							area = diff2; 
						}
					}
				
				} else if( inField(a2, n)){
					//std::cout << "a2,b2 in field" << std::endl;
					if( (isCow(field, a2) || isCow(field, b2)) && !( opCow(field, a2) || opCow(field, b2))  ){
						int diff2 = diff*diff;
						if(diff2 > area){
							
							//std::cout << 
							//	"updating area: a2, b2 satisfied. (need to check for opposing cow)" 
							//	<< std::endl;
							currSide = diff;
							area = diff2; 
						}
					}
				}
				
			// diagonal case
			} 

				// create midpoint values
				std::pair<double, double> midPoint((b.first-a.first)/2.0, (b.second-a.second)/2.0 );
				std::pair<double, double> L = rotateLeft(midPoint);
				std::pair<double, double> R = rotateRight(midPoint);
				
				midPoint.first += static_cast<double>(a.first);
				midPoint.second += static_cast<double>(a.second);
				
				std::pair<double, double> leftPos(midPoint.first + L.first, 
					midPoint.second + L.second );

				std::pair<double, double> rightPos(midPoint.first + R.first, 
					midPoint.second + R.second );

				//std::cout << "mid: " << midPoint.first << ", " << midPoint.second << std::endl;

				//std::cout << "midCell: " << a.first + midPoint.first << 
				//	", " << a.second + midPoint.second << std::endl;
				

				if(inField(leftPos, n) && inField(rightPos, n)){
				
					if(isCell(leftPos) && isCell(rightPos)){
						
						if(inField(leftPos, n) &&  inField(rightPos, n)){
							
							//std::cout << std::endl <<  "Possible diagonal square found!" << std::endl;
							// check if the other squares contain a cow
							
							/*
							std::cout << "Top-pos: " << a.first << ": " << a.second << std::endl;
							std::cout << "Bot-pos: " << b.first << ": " << b.second << std::endl;
							
							std::cout << "leftpos: " << leftPos.first << ": " << leftPos.second << std::endl;
							std::cout << "rightpos: " << rightPos.first << ": " << rightPos.second << std::endl;
							*/
							//if (inField(leftPos,n)) {std::cout << "leftPos in field" << std::endl;}
							//if (inField(rightPos,n)) {std::cout << "rightPos in field" << std::endl;}
							
							if( (isCow(field, leftPos) || isCow(field, rightPos)) && !( opCow(field, leftPos) || opCow(field, rightPos) ) ){
								//std::cout << "leftPos or rightPos is cow." << std::endl;
								//std::cout << "updating area. " << std::endl;
								
								int diff = getArea(leftPos, a);
								

								//std::cout << "diff: " << diff << std::endl; 
								if(diff > area){
									/*
									std::cout << 
										"updating area: " << diff 
										<< std::endl;
									*/
									currSide = getDistance(a,b);
									area = diff; 
								}
							}
						
						}

						/*
						std::cout << "leftPos field: " << 
							field[leftPos.second][leftPos.first] << std::endl;
						std::cout << "rightPos field: " << 
							field[rightPos.second][rightPos.first] << std::endl;
						
						
						*/

					}
				}
			
		}

	}
	
	
	/*
	for (int i = 0; i < n; ++i){
		for(int j = 0; j < n; ++j){
			std::cout << field[i][j] << " ";

		}
		std::cout << std::endl;
	}
	*/


	std::cout << area << std::endl; 
	return EXIT_SUCCESS;
}



// returns if the point is in a valid position - in the field
bool inField(std::pair<int,int> a, int n)
{

	if (a.first < 0 || a.first >= n){
		return false;
	} else if (a.second < 0 || a.second >= n){
		return false;
	}
	return true;
}

bool opCow(std::vector<std::string > field, std::pair<int,int> a)
{
	return field[a.second][a.first] == 'B';	
}

// returns if the cell contains Johns cow.
bool isCow(std::vector<std::string > field, std::pair<int,int> a)
{
	return field[a.second][a.first] == 'J';
}

// returns if the position is a discrete point, not a double point etc.
bool isCell(std::pair<double, double> a)
{
	double b = a.first - static_cast<int>(a.first);
	double c = a.second - static_cast<int>(a.second);
	return (b == 0) && (c == 0);
}

int getArea(std::pair<int,int> a, std::pair<int,int> b)
{
	return (a.first-b.first)*(a.first-b.first) 
		+ (a.second-b.second)*(a.second-b.second);	
}

double getDistance(std::pair<int,int> a, std::pair<int,int> b)
{
	return std::sqrt(static_cast<double>((a.first-b.first)*(a.first-b.first) 
		+ (a.second-b.second)*(a.second-b.second))); 

}

// take in two coordinate pairs (horiz axis), check if they can form a square
int checkHorizontal( const std::pair<int, int>& a, const std::pair<int, int>& b)
{
	//std::cout << "checking: " << a.second << "==" << b.second << std::endl;  
	return a.second == b.second;
}

// take in two coordinate pairs (verti axis), check if they can form a square
int checkVertical( const std::pair<int, int>& a, const std::pair<int, int>& b)
{
	//std::cout << "checking: " << a.first << "==" << b.first << std::endl;
	return a.first == b.first;
}

bool isValid(int pos, int n)
{
	return (pos >= 0 && pos < n);
}

// this will check the downwards and to the righ tmovement
std::pair<double, double> rotateLeft(std::pair<double, double> in)
{
	return std::pair<double, double> ( (-1)*in.second, (1)*in.first );
}


// this will check the downwards and to the righ tmovement
std::pair<double, double> rotateRight(std::pair<double, double> in)
{
	return std::pair<double, double> ( (1)*in.second, (-1)*in.first );
}