// herd.cpp
// progchal wk1
// herd problem solver imp.
// Peter Kydd 2016

#include <iostream>

bool isHalf(double in);
bool isInteger(double i);
bool isEven(double i);

int main()
{
	unsigned int n, count = 0;
	std::cin >> n;

	for(double i = 1; i/2.0 < static_cast<double>(n)/i; ++i){
		
		double frac = n/i;
		count += ((isEven(i) && isHalf(frac))) || ((!isEven(i) && isInteger(frac)));
		if(((isEven(i) && isHalf(frac))) || ((!isEven(i) && isInteger(frac)))){
			//std::cout << "n/i: " << n << "/" << i << std::endl; 
		}
	} 

	//std::cout << "count: " << count << std::endl;
	std::cout << count << std::endl; 
	return 0;
}

bool isHalf(double i)
{
	i = i - static_cast<long>(i);
	
	return (i == 0.5);
}

bool isInteger(double i)
{
	i = i - static_cast<long>(i);
	return (i == 0.0);
}

bool isEven(double i)
{
	return !(static_cast<int>(i)%2);
} 