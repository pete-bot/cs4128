// dna.cpp
// progchal wk1
// hangover problem solver imp.
// Peter Kydd 2016

#include <iostream>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <string>

inline void swap(char& a, char& b){ char tmp = a; a = b; b = tmp; }
int inversions(std::string dna, const int& length);

int main(int argc, char* argv[])
{
	int length, number;
	std::cin >> length >> number;

	std::string input;
	std::vector<std::pair<int, std::string> > seqs;
	
	// read in seqs
	while(std::cin >> input){
		seqs.push_back( std::make_pair(inversions(input, length), input) );
		//std::cout << input << std::endl;
	}

	// sort seqs based on inversion value
	std::sort(seqs.begin(), seqs.end());

	for(int i = 0; i < number; ++i){
		std::cout << seqs[i].second << std::endl;
	}



	return EXIT_SUCCESS;
}

// sort string and count number of inversions required to order sequence.
int inversions(std::string dna, const int& length)
{
	int inversions = 0;
	bool swapFlag = true;

	for(int j = length; swapFlag && j > 0; --j ){
		swapFlag = false;
		
		for(int i =1; i < j; ++i){
			if( dna[i-1] > dna[i] ){
				swap(dna[i-1], dna[i]);
				swapFlag = true;
				++inversions;
			}
		}
	}

	return inversions;
}
