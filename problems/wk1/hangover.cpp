// hangover.c
// progchal wk1
// hangover problem solver imp.
// Peter Kydd 2016

#include <iostream>
#include <cstdlib>

unsigned int hangover(const double& overhang);

int main(int argc, char *argv[]) {
    double overhang = 0;

    while (std::cin >> overhang && overhang != 0.0){
        std::cout << hangover(overhang) << " card(s)" << std::endl;
    }

    return EXIT_SUCCESS;
}


// calculate sum and return no. cards required for overhang
unsigned int hangover(const double& overhang)
{
    double denominator = 2;
    double sum = 0;
    int count = 0;

    while(sum < overhang){
        sum += 1.0/denominator;
        ++denominator;
        ++count;
    }

    return count;
}

