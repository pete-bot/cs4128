// square.cpp
// progchal wk1
// Peter Kydd 2016

#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <cmath>
#include <cassert>
//#include <cstdint>

#define x first
#define y second
#define intPair std::pair<int,int>

// note coords in point are (x,y)
// the higher the Y value, the lower on the board

double getDistance(const intPair& a, const intPair& b);
bool inField(const intPair& a, const int& n);
bool isCow(const std::vector<std::string >& field,const intPair& a);
bool oppCow(const std::vector<std::string >& field, const intPair& a);
int getArea(const intPair& a,const intPair& b);
intPair rotateR(const intPair& a);

int main(int argc, char* argv[])
{
	int currArea = 0;
	double currSide = 0;

	// take in n
	int n;
	std::cin >> n;
	std::vector<std::string> field;

	// sets of cow coords
	std::vector<intPair > jSet;
	
	std::string tmp;
	
	// capture set of points, read in 
	for (int i = 0; i < n; ++i ){
		std::cin >> tmp;	
		field.push_back(tmp);
		for (int j = 0; j < n; ++j){
			// add coords for J set
			if(tmp[j] == 'J'){
				jSet.push_back(std::make_pair(j,i));
				//std::cout << "J: " << j << "," << i << std::endl;
			}
		}
	}


	for(unsigned int topLeft = 0; topLeft < jSet.size(); ++topLeft){
		for (unsigned int bottomRight = jSet.size()-1; topLeft < bottomRight; --bottomRight){
			if(currSide > getDistance(jSet[topLeft], jSet[bottomRight] )) {break;}
			
			// get vector representing difference between two points
			intPair diff(jSet[bottomRight].x-jSet[topLeft].x,jSet[bottomRight].y-jSet[topLeft].y);
			
			// apply transform to difference vector
			intPair rotate = rotateR(diff);

			// add difference vector to both original points
			intPair a1(jSet[topLeft].x + rotate.x, jSet[topLeft].y + rotate.y );
			intPair a2(jSet[topLeft].x - rotate.x, jSet[topLeft].y - rotate.y );
			intPair b1(jSet[bottomRight].x + rotate.x, jSet[bottomRight].y + rotate.y );
			intPair b2(jSet[bottomRight].x - rotate.x, jSet[bottomRight].y - rotate.y );


			// check for pointsof square in field etc. 
			if(inField(a1, n) && inField(b1, n)){
				if( (isCow(field, a1) || isCow(field, b1)) && 
								!( oppCow(field, a1) || oppCow(field, b1) ) ){
					currSide = getDistance(a1, b1);
					currArea = getArea(a1,b1);
				}
			}
			
			if(inField(a2, n) && inField(b2, n)){
				if( (isCow(field, a2) || isCow(field, b2)) && 
								!( oppCow(field, a2) || oppCow(field, b2) ) ){
					currSide = getDistance(a2, b2);;
					currArea = getArea(a2,b2);
				}
			}
		}
	}



	/*
	// print out game board (for entertainment purposes only )
	for (int i = 0; i < n; ++i){
		for(int j = 0; j < n; ++j){
			std::cout << field[i][j] << " ";

		}
		std::cout << std::endl;
	
	}
	std::cout << "area: " << currArea << std::endl; 
	*/
	

	std::cout << currArea << std::endl; 
	return EXIT_SUCCESS;
}

intPair rotateR(const intPair& a)
{
	return intPair(-a.y, a.x);
}

// returns if the point is in a valid position - in the field
bool inField(const intPair& a,const  int& n)
{
	if (a.x < 0 || a.x >= n){
		return false;
	} else if (a.y < 0 || a.y >= n){
		return false;
	}
	return true;
}

bool oppCow(const std::vector<std::string >& field, const intPair& a)
{
	return field[a.y][a.x] == 'B';	
}

bool isCow(const std::vector<std::string >& field, const intPair& a)
{
	return field[a.y][a.x] == 'J';
}


int getArea(const intPair& a, const intPair& b)
{
	return (a.x-b.x)*(a.x-b.x) 
	    + (a.y-b.y)*(a.y-b.y);	
}

double getDistance(const intPair& a, const intPair& b)
{
	return std::sqrt(static_cast<double>((a.x-b.x)*(a.x-b.x) 
		+ (a.y-b.y)*(a.y-b.y))); 

}
