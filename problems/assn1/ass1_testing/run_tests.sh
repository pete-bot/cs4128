#!/bin/bash

for i in $(seq 1 500)
do
    echo "test$i.in"
    ./ass1 < tests/test$i.in | diff -y --suppress-common-lines tests/test$i.out -
done

echo "if there is no diff output, then the test was passed"
