# Generate random input for assignment 1
from random import randint as random
from Queue import Queue


k = 1000  # add zeros up to 1000 to increase the problem size
max_range = 100  # for subtask 1
queue = Queue()


# inputs
n_integers = random(2, 20 * k)
print n_integers
for x in xrange(n_integers):
    print random(1, 50),
print


# queries
n_queries = random(2, 10 * k)
print n_queries

for x in xrange(n_queries):
    query_type = 'QMD'[random(0,2)]
    ij_range = random(2, max_range)
    from_idx = random(1, n_integers - 1)
    to_idx = random(from_idx + 1, min(from_idx + ij_range, n_integers))
    factor = random(1, 50)

    if (query_type == 'Q'):
	print 'Q',
	print from_idx, to_idx
	continue

    if (query_type == 'M'):
	print 'M',
	queue.put((from_idx, to_idx, factor))
	print from_idx, to_idx, factor
	continue

    if (query_type == 'D'):
	if queue.empty():
	    print 'Q',
	    print from_idx, to_idx
	else:
	    print 'D',
	    from_idx, to_idx, factor = queue.get()
	    print from_idx, to_idx, factor







