#!/bin/bash

mkdir tests

max=500

echo "making $max tests..."

for i in $(seq 1 $max)
do
    python2.7 fuzz.py > tests/test$i.in
    ./stanis < tests/test$i.in > tests/test$i.out
done

echo "finished making tests."
