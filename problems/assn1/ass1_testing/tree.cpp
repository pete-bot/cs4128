// cs4128
// assignment 1
// peter kydd z3367463

//#include <iostream>
#include <cstdio>
#include <iomanip>
#include <iostream>
#include <cstdlib>
#include <climits>
#include <cmath>
#include <cstring>
#include <cassert>
#include <sstream>

#define IS_LEAF true
#define NOT_LEAF false
#define IS_MULT true
#define IS_DIV false
#define INIT true

#define INF -1
#define PRIME_COUNT 15
#define N_MAX 20010
#define PRIME_MAX 50
#define MOD  1000000007

struct node{
    int l;
    int r;
    int mid;
    int factors[PRIME_COUNT];
    int lazy[PRIME_COUNT];
    bool update_required;
};

int N;
int source[N_MAX];
node tree[4*N_MAX+10];
int prime_t[PRIME_MAX];
int prime_t_rev[PRIME_COUNT];

int pending_buffer[PRIME_COUNT];
int query_buffer[PRIME_COUNT];



// init functions
void build(int node, int left, int right);
void init_node(int node, int left, int right);
void init_primes(void);


// primary functions
void update_handler(int node, int left, int right, bool is_mult, int value);
void update(int node, int left, int right);
void query_handler(int start, int end);
void query(int node, int left, int right, int (&query_buffer)[PRIME_COUNT]);
void check_and_update(int node);

// helper functions
void update_node_gcd(int (&dest)[PRIME_COUNT], int (&left)[PRIME_COUNT], int (&right)[PRIME_COUNT]);
void apply_lazy_buffer_to_factors(int node);
void update_lazy_buffer_from_buffer(int node, int (&buffer)[PRIME_COUNT]);
void apply_buffer_directly_to_factors(int node, int (&buffer)[PRIME_COUNT]);
long long recombine_factors(int (&factors)[PRIME_COUNT]);
void get_factors( long long value, bool is_mult, int (&buffer)[PRIME_COUNT]);

long long mod_pow( long long base, long long exp );
int min( int a, int b) { return a < b ? a : b; }
int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }
void print_primes(const node& a);

int main(int argc, char* argv[])
{
    
    init_primes();

    int scan_collect;
    scan_collect = scanf("%d", &N);

    int val;

    // populate source
    for(int i = 0; i < N; ++i){
        scan_collect = scanf("%d",&val);
        //printf("val: %d\n", val);
        source[i] = val;
    }

    build(1, 0,N-1);

    int queries;
    scan_collect = scanf("%d", &queries);
    //printf("%d\n", queries);
    char op[5];
    int start, end, x;

    // get and operate on queries
    for(int i = 0; i < queries; ++i){

        scan_collect = scanf("%s", op);
        
        if(op[0] == 'Q'){
            scan_collect = scanf("%d %d", &start, &end);
            //printf("Q %d %d\n", start, end);
            query_handler(start, end-1);
        
        } else if (op[0] == 'D'){
            scan_collect = scanf("%d %d %d", &start, &end,&x);
            //printf("D %d %d %d\n", start, end, x);
            update_handler(1, start, end-1, IS_DIV, x);
            
        } else if(op[0] == 'M'){
            scan_collect = scanf("%d %d %d",&start, &end, &x);
            //printf("M %d %d %d\n", start, end, x);
            update_handler(1, start, end-1, IS_MULT, x);
    
        }

        if(scan_collect){}
    }

    return EXIT_SUCCESS;
}


void update_handler(int node, int left, int right, bool is_mult, int value)
{
    get_factors(value, is_mult, pending_buffer);
    update(node, left, right);

}

void update(int node, int left, int right)
{
    if(tree[node].update_required){
        apply_lazy_buffer_to_factors(node);

        if(tree[node].l != tree[node].r){
            tree[2*node].update_required = true;
            update_lazy_buffer_from_buffer(2*node, tree[node].lazy);

            tree[2*node+1].update_required = true;
            update_lazy_buffer_from_buffer(2*node+1, tree[node].lazy);
        }

        memset(tree[node].lazy, 0, sizeof(int)*PRIME_COUNT);
        tree[node].update_required = false;

    }
    if(tree[node].l == left && tree[node].r == right){
        apply_buffer_directly_to_factors(node, pending_buffer);

        if(tree[node].l != tree[node].r){
            tree[2*node].update_required = true;
            update_lazy_buffer_from_buffer(2*node, pending_buffer);
            tree[2*node+1].update_required = true;
            update_lazy_buffer_from_buffer(2*node+1, pending_buffer);
        }
        return;
    }

    if(right <= tree[node].mid ){
        update(2*node, left, right);
    } else if (left > tree[node].mid){
        update(2*node+1, left, right );
    } else {
        update(2*node, left, tree[node].mid );
        update(2*node+1, tree[node].mid+1, right );
    }

    check_and_update(node);

}

void query_handler(int start, int end)
{
    int query_buffer[PRIME_COUNT];
    memset(query_buffer, 0, sizeof(int)*PRIME_COUNT);
    query(1, start, end, query_buffer);
    printf("%llu\n", recombine_factors(query_buffer));
}

void query(int node, int left, int right, int (&query_buffer)[PRIME_COUNT])
{
    // uopdate as with update
    if(tree[node].update_required){
        apply_lazy_buffer_to_factors(node);

        if(tree[node].l != tree[node].r){
            tree[2*node].update_required = true;
            update_lazy_buffer_from_buffer(2*node, tree[node].lazy);

            tree[2*node+1].update_required = true;
            update_lazy_buffer_from_buffer(2*node+1, tree[node].lazy);
        }

        /* the memset below was also missing - this was an oversight, 
        as it should always have been here */

        tree[node].update_required = false;
        memset(tree[node].lazy, 0, sizeof(int)*PRIME_COUNT);
        
    }
    
    // we have hit the node
    if(tree[node].l == left && tree[node].r == right){
        memcpy(query_buffer, tree[node].factors, sizeof(int)*PRIME_COUNT);
        return;
    }

    
    int mid  = tree[node].mid;
    if(right <= mid ){
        //printf("mid > right : %d, %d\n", tree[node].mid, right );
        query(2*node, left, right, query_buffer);
    
    } else if(left > mid ){
        //printf("left > mid: %d, %d\n", left, tree[node].mid );
        query(2*node+1, left, right, query_buffer);
    
    } else {
        //printf("left < mid, mid < right: %d %d | %d %d\n", left, tree[node].mid, tree[node].mid, right );
        int left_result[PRIME_COUNT];
        int right_result[PRIME_COUNT];
        memset(left_result, 0 , sizeof(int)*PRIME_COUNT);
        memset(right_result, 0 , sizeof(int)*PRIME_COUNT);

        query(2*node, left, mid, left_result);
        query(2*node+1, mid+1, right, right_result);
        
        update_node_gcd(query_buffer, left_result, right_result);
        return;

    }
}



void check_and_update(int node)
{
    int left = 2*node;
    int right = 2*node+1;

    bool left_updated = false, right_updated = false;

    if(tree[left].update_required){
        apply_lazy_buffer_to_factors(left);


        /* note - issue was here, the update below was not left - it was node, and 
        giving the wrong solution. */

        if(tree[left].l != tree[left].r){
            tree[2*left].update_required = true;
            update_lazy_buffer_from_buffer(2*left, tree[left].lazy);
            tree[2*left+1].update_required = true;
            update_lazy_buffer_from_buffer(2*left+1, tree[left].lazy);
        }

        tree[left].update_required = false;
        memset(tree[left].lazy, 0, sizeof(int)*PRIME_COUNT);
        left_updated = true;

    }

    if(tree[right].update_required){
        apply_lazy_buffer_to_factors(right);

        if(tree[right].l != tree[right].r){
            tree[2*right].update_required = true;
            update_lazy_buffer_from_buffer(2*right, tree[right].lazy);
            tree[2*right+1].update_required = true;
            update_lazy_buffer_from_buffer(2*right+1, tree[right].lazy);
        }

        tree[right].update_required = false;
        memset(tree[right].lazy, 0, sizeof(int)*PRIME_COUNT);
        right_updated = true;
    }

    //if(left_updated || right_updated){
    update_node_gcd(tree[node].factors, tree[left].factors, tree[right].factors);
    //}
}





void build(int node, int l, int r)
{
    if(l == r){
        init_node(node, l, r);
    } else {
        int mid = (r+l)/2;
        build(2*node, l, mid);
        build(2*node+1, mid+1, r);
        init_node(node, l, r );
        
        check_and_update(node);
    }
}

void init_node(int node, int left, int right)
{
    memset(tree[node].factors, 0, PRIME_COUNT*sizeof(int));
    memset(tree[node].lazy, 0, PRIME_COUNT*sizeof(int));
    
    tree[node].l = left;
    tree[node].r = right;

    if(left==right){
        get_factors(source[left], INIT, tree[node].factors );
        tree[node].update_required = false;
        tree[node].mid = left;
        return;
    
    }

    update_node_gcd(tree[node].factors, tree[2*node].factors, tree[2*node+1].factors);
    tree[node].update_required = false;

    tree[node].mid = (left+right)/2;
}

void update_node_gcd(int (&dest)[PRIME_COUNT], int (&left)[PRIME_COUNT], int (&right)[PRIME_COUNT] )
{
    for(int i = 0; i < PRIME_COUNT; i++){
        dest[i] = min(left[i], right[i]); 
    }
}


void apply_lazy_buffer_to_factors(int node)
{
    for(int i = 0; i < PRIME_COUNT; ++i){
        tree[node].factors[i] += tree[node].lazy[i];
    }
    
}


void update_lazy_buffer_from_buffer(int node, int (&buffer)[PRIME_COUNT])
{
    for(int i = 0; i < PRIME_COUNT; ++i){
        tree[node].lazy[i] += buffer[i];
    }
}


void apply_buffer_directly_to_factors(int node, int (&buffer)[PRIME_COUNT])
{
    for(int i = 0; i < PRIME_COUNT; ++i){
        tree[node].factors[i] += buffer[i];
    }
}





// #########################################################
//                          PASSED
// #########################################################



long long recombine_factors(int (&factors)[PRIME_COUNT])
{
    //print_primes(tree[node]);
    long long product = 1;
    for(int i = 0; i < PRIME_COUNT; ++i){
        product = (product * mod_pow(prime_t_rev[i], factors[i])) % MOD;
    }
    return product;
}




void get_factors( long long value, bool is_mult, int (&buffer)[PRIME_COUNT])
{
    memset(buffer, 0, sizeof(int)*PRIME_COUNT);
    if(value == 0) {return;}
    while (value%2 == 0){
        
        if(is_mult){
            buffer[0]++;
        } else {
            buffer[0]--;
        }
        value = value/2;
    }
 
    for (int i = 3; i <= sqrt(value); i = i+2)
    {
        while (value%i == 0){
            
            if(is_mult){
                buffer[prime_t[i]]++;
            } else {
                buffer[prime_t[i]]--;
            }
            value = value/i;
        }
    }
 
    if (value > 2){
        if(is_mult){
            buffer[prime_t[value]]++;
        } else {
            buffer[prime_t[value]]--;
        }
    }
}




long long mod_pow( long long base, long long exp )
{
    if(exp == 0){
        return 1;
    }
    long long a = mod_pow(base, exp/2);
    a = (a * a)%MOD;
    if(exp % 2){
        a = (a * base) % MOD;
    }
    return a;
}




void init_primes(void)
{
    prime_t[2] = 0;
    prime_t[3] = 1;
    prime_t[5] = 2;
    prime_t[7] = 3;
    prime_t[11] = 4;
    prime_t[13] = 5;
    prime_t[17] = 6;
    prime_t[19] = 7;
    prime_t[23] = 8;
    prime_t[29] = 9;
    prime_t[31] = 10;
    prime_t[37] = 11;
    prime_t[41] = 12;
    prime_t[43] = 13;
    prime_t[47] = 14;

    prime_t_rev[0] = 2;
    prime_t_rev[1] = 3;
    prime_t_rev[2] = 5;
    prime_t_rev[3] = 7;
    prime_t_rev[4] = 11;
    prime_t_rev[5] = 13;
    prime_t_rev[6] = 17;
    prime_t_rev[7] = 19;
    prime_t_rev[8] = 23;
    prime_t_rev[9] = 29;
    prime_t_rev[10] = 31;
    prime_t_rev[11] = 37;
    prime_t_rev[12] = 41;
    prime_t_rev[13] = 43;
    prime_t_rev[14] = 47;
}


long long recombine_lazy(int node)
{
    //print_primes(tree[node]);
    long long product = 1;
    for(int i = 0; i < PRIME_COUNT; ++i){
        product = (product * mod_pow(prime_t_rev[i], tree[node].lazy[i]))%MOD;
    }

    return product;
}
