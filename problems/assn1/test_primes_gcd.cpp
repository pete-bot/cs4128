
void test_gcd(void)
{
    printf("Testing gcd....\n");

    node a,b,c,d,e,f,g,h,i,j,k;

    
    // test against self
    set_prime_factors(a, 50);
    assert(get_gcd(a,a) == 50);

    set_prime_factors(b, 2262);
    assert(get_gcd(b,b) == 2262);
  
    set_prime_factors(c, 510510);
    assert(get_gcd(c,c) == 510510);
    
    set_prime_factors(d, 405000);
    assert(get_gcd(d,d) == 405000);
    
    set_prime_factors(e, 340605000);
    assert(get_gcd(e,e) == 340605000);

    set_prime_factors(f, 2498396672);
    assert(get_gcd(f,f) == 2498396672);
    
    set_prime_factors(g, 572994802228616704);
    assert(get_gcd(g,g) == 572994802228616704);
    
    set_prime_factors(h, 614889782588491410);
    assert(get_gcd(h,h) == 614889782588491410);

    set_prime_factors(i, 12342123427);
    set_prime_factors(j, 187326493);


    assert(get_gcd(a,b) == 2);


    //printf("get get_gcd: %d\n", get_gcd(a,a));


    printf("PASSED.\n");
}

void test_set_prime_factors(void)
{
    printf("Testing prime factorisation....");
    node a,b,c,d,e,f,g,h,i,j,k;

    set_prime_factors(a,400);
    assert(a.primes[prime_t[2]] ==  4 
        && a.primes[prime_t[3]] ==  0
        && a.primes[prime_t[5]] ==  2 
        && a.primes[prime_t[7]] ==  0
        && a.primes[prime_t[11]] == 0
        && a.primes[prime_t[13]] == 0
        && a.primes[prime_t[17]] == 0
        && a.primes[prime_t[19]] == 0
        && a.primes[prime_t[23]] == 0
        && a.primes[prime_t[29]] == 0
        && a.primes[prime_t[31]] == 0
        && a.primes[prime_t[37]] == 0
        && a.primes[prime_t[41]] == 0
        && a.primes[prime_t[43]] == 0
        && a.primes[prime_t[47]] == 0);

    set_prime_factors(b, 2262);


    assert(b.primes[prime_t[2]] ==  1 
        && b.primes[prime_t[3]] ==  1
        && b.primes[prime_t[5]] ==  0 
        && b.primes[prime_t[7]] ==  0
        && b.primes[prime_t[11]] == 0
        && b.primes[prime_t[13]] == 1
        && b.primes[prime_t[17]] == 0
        && b.primes[prime_t[19]] == 0
        && b.primes[prime_t[23]] == 0
        && b.primes[prime_t[29]] == 1
        && b.primes[prime_t[31]] == 0
        && b.primes[prime_t[37]] == 0
        && b.primes[prime_t[41]] == 0
        && b.primes[prime_t[43]] == 0
        && b.primes[prime_t[47]] == 0);

    set_prime_factors(c, 510510);
    assert(c.primes[prime_t[2]] ==  1 
        && c.primes[prime_t[3]] ==  1
        && c.primes[prime_t[5]] ==  1 
        && c.primes[prime_t[7]] ==  1
        && c.primes[prime_t[11]] == 1
        && c.primes[prime_t[13]] == 1
        && c.primes[prime_t[17]] == 1
        && c.primes[prime_t[19]] == 0
        && c.primes[prime_t[23]] == 0
        && c.primes[prime_t[29]] == 0
        && c.primes[prime_t[31]] == 0
        && c.primes[prime_t[37]] == 0
        && c.primes[prime_t[41]] == 0
        && c.primes[prime_t[43]] == 0
        && c.primes[prime_t[47]] == 0);

    set_prime_factors(d, 405000);
    assert(d.primes[prime_t[2]] ==  3 
        && d.primes[prime_t[3]] ==  4
        && d.primes[prime_t[5]] ==  4 
        && d.primes[prime_t[7]] ==  0
        && d.primes[prime_t[11]] == 0
        && d.primes[prime_t[13]] == 0
        && d.primes[prime_t[17]] == 0
        && d.primes[prime_t[19]] == 0
        && d.primes[prime_t[23]] == 0
        && d.primes[prime_t[29]] == 0
        && d.primes[prime_t[31]] == 0
        && d.primes[prime_t[37]] == 0
        && d.primes[prime_t[41]] == 0
        && d.primes[prime_t[43]] == 0
        && d.primes[prime_t[47]] == 0);


    set_prime_factors(e, 340605000);
    assert(e.primes[prime_t[2]] ==  3 
        && e.primes[prime_t[3]] ==  4
        && e.primes[prime_t[5]] ==  4 
        && e.primes[prime_t[7]] ==  0
        && e.primes[prime_t[11]] == 0
        && e.primes[prime_t[13]] == 0
        && e.primes[prime_t[17]] == 0
        && e.primes[prime_t[19]] == 0
        && e.primes[prime_t[23]] == 0
        && e.primes[prime_t[29]] == 2
        && e.primes[prime_t[31]] == 0
        && e.primes[prime_t[37]] == 0
        && e.primes[prime_t[41]] == 0
        && e.primes[prime_t[43]] == 0
        && e.primes[prime_t[47]] == 0);

    set_prime_factors(f, 2498396672);
    assert(f.primes[prime_t[2]] ==  9 
        && f.primes[prime_t[3]] ==  0
        && f.primes[prime_t[5]] ==  0 
        && f.primes[prime_t[7]] ==  0
        && f.primes[prime_t[11]] == 0
        && f.primes[prime_t[13]] == 0
        && f.primes[prime_t[17]] == 0
        && f.primes[prime_t[19]] == 0
        && f.primes[prime_t[23]] == 0
        && f.primes[prime_t[29]] == 0
        && f.primes[prime_t[31]] == 0
        && f.primes[prime_t[37]] == 0
        && f.primes[prime_t[41]] == 0
        && f.primes[prime_t[43]] == 0
        && f.primes[prime_t[47]] == 4);

    set_prime_factors(g,572994802228616704 );
    assert(g.primes[prime_t[2]] ==  9 
        && g.primes[prime_t[3]] ==  0
        && g.primes[prime_t[5]] ==  0 
        && g.primes[prime_t[7]] ==  0
        && g.primes[prime_t[11]] == 0
        && g.primes[prime_t[13]] == 0
        && g.primes[prime_t[17]] == 0
        && g.primes[prime_t[19]] == 0
        && g.primes[prime_t[23]] == 0
        && g.primes[prime_t[29]] == 0
        && g.primes[prime_t[31]] == 0
        && g.primes[prime_t[37]] == 0
        && g.primes[prime_t[41]] == 0
        && g.primes[prime_t[43]] == 0
        && g.primes[prime_t[47]] == 9);

    set_prime_factors(h, 614889782588491410);
    
    // printf("primes: \n");
    // for(int counter = 0; counter < PRIME_COUNT; ++counter){
        
    //     printf("%d: %d\n", prime_t_rev[counter], h.primes[counter]);
    // }

    assert(h.primes[prime_t[2]] ==  1 
        && h.primes[prime_t[3]] ==  1
        && h.primes[prime_t[5]] ==  1 
        && h.primes[prime_t[7]] ==  1
        && h.primes[prime_t[11]] == 1
        && h.primes[prime_t[13]] == 1
        && h.primes[prime_t[17]] == 1
        && h.primes[prime_t[19]] == 1
        && h.primes[prime_t[23]] == 1
        && h.primes[prime_t[29]] == 1
        && h.primes[prime_t[31]] == 1
        && h.primes[prime_t[37]] == 1
        && h.primes[prime_t[41]] == 1
        && h.primes[prime_t[43]] == 1
        && h.primes[prime_t[47]] == 1);


    // not sure if this is supposed to happen!
    //set_prime_factors(i, 1844674407370955161);
    
    // printf("primes: \n");
    // for(int counter = 0; counter < PRIME_COUNT; ++counter){
    //     printf("%d: %d\n", prime_t_rev[counter], i.primes[counter]);
    // }
    /*
    assert(i.primes[prime_t[2]] ==  1 
        && i.primes[prime_t[3]] ==  1
        && i.primes[prime_t[5]] ==  1 
        && i.primes[prime_t[7]] ==  1
        && i.primes[prime_t[11]] == 1
        && i.primes[prime_t[13]] == 1
        && i.primes[prime_t[17]] == 1
        && i.primes[prime_t[19]] == 1
        && i.primes[prime_t[23]] == 1
        && i.primes[prime_t[29]] == 1
        && i.primes[prime_t[31]] == 1
        && i.primes[prime_t[37]] == 1
        && i.primes[prime_t[41]] == 1
        && i.primes[prime_t[43]] == 1
        && i.primes[prime_t[47]] == 1);
    */

    //unsigned long long  glf = 614889782588491410;

    printf("PASSED.\n");    
}
