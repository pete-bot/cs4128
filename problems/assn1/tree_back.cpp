// cs4128
// assignment 1
// peter kydd z3367463

//#include <iostream>
#include <cstdio>
#include <iomanip>
#include <iostream>
#include <cstdlib>
#include <climits>
#include <cmath>
#include <cstring>
#include <cassert>
#include <sstream>

#define IS_LEAF true
#define NOT_LEAF false
#define IS_MULT true
#define IS_DIV false

#define INF -1
#define PRIME_COUNT 15
#define N_MAX 20010
#define PRIME_MAX 50
#define MOD  1000000007

struct node{
    int l;
    int r;
    int mid;
    int prime_buffer[PRIME_COUNT];
    int lazy_buffer[PRIME_COUNT];
    bool update_required;
};

// prime translation array
int prime_t[PRIME_MAX];
int prime_t_rev[PRIME_MAX];
int source[N_MAX];
node tree[4*N_MAX+10];
int N;

int pending_buffer[PRIME_COUNT];
int query_buffer[PRIME_COUNT];

// main functions
void build ( int i, int l, int r);
void handler(int left, int right, long long value);
void update(int node, int left, int right);

void query_handler(int node, int left, int right);
void query_tree(int node, int left, int right, int (&query_buffer)[PRIME_COUNT]);

void check_and_merge(int node);



// helper functions
void init_primes(void);
void update_node(int node, bool is_leaf, long long value, int left, int right);
void update_handler(int left, int right, long long value, bool is_mult);
void apply_lazy_buffer(int node);
void update_node_gcd_buffer(int (&dest)[PRIME_COUNT], int (&l)[PRIME_COUNT], int (&r)[PRIME_COUNT]);
void set_prime_factors(int node, long long value);
long long recombine(int node);
long long recombine_query(int (&query_buffer)[PRIME_COUNT]);
void update_lazy_buffer(int node);



long long recombine_lazy(int node);




void populate_pending_buffer(long long value, bool is_mult);
void update_node_gcd(int source, int left, int right);

long long mod_pow( long long base, long long exp );
int min( int a, int b) { return a < b ? a : b; }
int gcd(int a, int b) { return b == 0 ? a : gcd(b, a % b); }

void print_primes(const node& a);
void postorder(int p, int indent);

// test functions
void test_gcd(void);
void test_set_prime_factors(void);
void test_recombine(void);
void test_update_primes_buffer(void);
void test_populate_pending_buffer(void);

int main(int argc, char* argv[])
{
    
    init_primes();

    int scan_collect;
    scan_collect = scanf("%d", &N);

    int val;

    // populate source
    for(int i = 0; i < N; ++i){
        scan_collect = scanf("%d",&val);
        //printf("val: %d\n", val);
        source[i] = val;
    }

    build(1, 0,N-1);
    //postorder(1, 0);

    int queries;
    scan_collect = scanf("%d", &queries);
    //printf("%d\n", queries);
    char op[5];
    int start , end, x;
    // get and operate on queries
    for(int i = 0; i < queries; ++i){
        
        // printf("\n\n-------------------------------------------------------------");
        //printf("%d:\n",i);
        scan_collect = scanf("%s", op);
        //printf("%c\n", op);
        
        // multiply 
        if(op[0] == 'M'){

            scan_collect = scanf("%d %d %d",&start, &end, &x);
            if(start == N) {start -= 1;}
            if(x == 1){continue;}
            //printf("M: %d %d %d\n", start, end, x);
            populate_pending_buffer(x, IS_MULT);
            update(1, start, end-1);

        // divide
        } else if (op[0] == 'D'){

            scan_collect = scanf("%d %d %d", &start, &end,&x);
            if(start == N) {start -= 1;}
            if(x == 1){continue;}
            //printf("D: %d %d %d\n", start, end, x);
            populate_pending_buffer(x, IS_DIV);
            update(1, start, end-1);
        
        // query - this needs to update the GCD on the specified range
        } else if(op[0] == 'Q'){

            scan_collect = scanf("%d %d", &start, &end);
            if(start == N) {start -= 1;}
            //printf("Q: %d %d\n", start, end-1);

            int query_buffer[PRIME_COUNT];
            memset(query_buffer, 0, sizeof(int)*PRIME_COUNT);
            query_tree(1, start, end-1, query_buffer);
            printf("%llu\n", recombine_query(query_buffer));

        }

        // printf("\n-------------------------------------------------------------\n\n");
        if(scan_collect){}
        //postorder(1, 0);
    }
    

    return EXIT_SUCCESS;
}



void query_tree(int node, int left, int right, int (&query_buffer)[PRIME_COUNT])
{

    // update node
    if(tree[node].update_required){
        //update_lazy_buffer(node);
        apply_lazy_buffer(node);
        
        // check if node is a leaf, if not mark children as require update
        if (tree[node].l != tree[node].r){
            
            update_lazy_buffer(2*node);
            update_lazy_buffer(2*node+1);
        }

        tree[node].update_required = false;// reset current node update require status
    }

    
    /* XXX
    if(tree[node].l != tree[node].r){
        check_and_merge(node);
        update_node_gcd(node,2*node,2*node+1);
    }
    */    

    if(tree[node].l == left && tree[node].r == right){
        check_and_merge(node);
        
        memcpy(query_buffer, tree[node].prime_buffer, sizeof(int)*PRIME_COUNT);
        return;
    }
    int mid  = tree[node].mid;

    if(right <= mid ){
        //printf("mid > right : %d, %d\n", tree[node].mid, right );
        query_tree(2*node, left, right, query_buffer);
        return;
    
    } else if(left > mid ){
        //printf("left > mid: %d, %d\n", left, tree[node].mid );
        query_tree(2*node+1, left, right, query_buffer);
        return;
    
    } else {
        //printf("left < mid, mid < right: %d %d | %d %d\n", left, tree[node].mid, tree[node].mid, right );
        int left_result[PRIME_COUNT];
        memset(left_result, 0 , sizeof(int)*PRIME_COUNT);
        int right_result[PRIME_COUNT];
        memset(right_result, 0 , sizeof(int)*PRIME_COUNT);

        query_tree(2*node, left, mid, left_result);
        query_tree(2*node+1, mid+1, right, right_result);
        
        update_node_gcd_buffer(query_buffer, left_result, right_result);
        return;

    }
}


void update(int node, int left, int right) 
{
    // if node requires update
    if(tree[node].update_required){
        
        // XXX
        //update_lazy_buffer(node);
        apply_lazy_buffer(node);
        
        // check if node is a leaf, if not mark children as require update
        if (tree[node].l != tree[node].r){
            tree[2*node].update_required = true;
            update_lazy_buffer(2*node);

            tree[2*node+1].update_required = true;
            update_lazy_buffer(2*node+1);
        }

        tree[node].update_required = false;// reset current node update require status
    }

    // segment is in range
    if(tree[node].l == left && tree[node].r == right){
        
        // XXX
        update_lazy_buffer(node);
        apply_lazy_buffer(node);           

        if (tree[node].l != tree[node].r){

            tree[2*node].update_required = true;
            update_lazy_buffer(2*node);
        
            tree[2*node+1].update_required = true;
            update_lazy_buffer(2*node+1);

            check_and_merge(node);

        }

        return;
    }

    int mid = tree[node].mid;

    if(right <= mid ){
        update(2*node, left, right);
    } else if (left > mid){
        update(2*node+1, left, right );
    } else {
        update(2*node, left, mid );
        update(2*node+1, mid + 1, right );
    }


    // update any children
    check_and_merge(node);
    
    //XXX
    //update_node_gcd(node,2*node,2*node+1);


}


















// #########################################################
//                          PASSED
// #########################################################



void check_and_merge(int node)
{
   
    int left = 2*node;
    int right = 2*node+1;

    bool left_updated = false, right_updated = false;

    if(tree[left].update_required){
        apply_lazy_buffer(left);
        
        if (tree[left].l != tree[left].r){
            tree[2*left].update_required = true;
            update_lazy_buffer(2*left);

            tree[2*left+1].update_required = true;
            update_lazy_buffer(2*left+1);
        }

        tree[left].update_required = false;
        left_updated = true;
    }

    if(tree[right].update_required){
        apply_lazy_buffer(right);
        
        if (tree[right].l != tree[right].r){
            tree[2*right].update_required = true;
            update_lazy_buffer(2*right);

            tree[2*right+1].update_required = true;
            update_lazy_buffer(2*right+1);
        }

        tree[right].update_required = false;
        right_updated = true;
    }

    if(left_updated || right_updated){
        update_node_gcd(node, left, right);
    }

}


void update_node_gcd(int source, int left, int right)
{
    memset(tree[source].prime_buffer, 0, sizeof(int)*PRIME_COUNT);
    for(int i = 0; i < PRIME_COUNT; i++){
        tree[source].prime_buffer[i] = min(tree[left].prime_buffer[i], tree[right].prime_buffer[i]);     
    }
}

void update_node_gcd_buffer(int (&dest)[PRIME_COUNT], int (&l)[PRIME_COUNT], int (&r)[PRIME_COUNT])
{
    memset(query_buffer, 0, sizeof(int)*PRIME_COUNT);
    for(int i = 0; i < PRIME_COUNT; i++){
        dest[i] = min(l[i], r[i]); 
    }
}



// clears and sets the global update buffer - used to update nodes/gcd nodes
void populate_pending_buffer( long long value, bool is_mult)
{
    memset(pending_buffer, 0, sizeof(int)*PRIME_COUNT);
    //printf("set_prime_value: %llu\n", value);
    if(value == 0) {return;}

    // Print the number of 2s that divide n
    while (value%2 == 0){
        //printf("2\n");
        if(is_mult){
            pending_buffer[0]++;
        } else {
            pending_buffer[0]--;
        }
        value = value/2;
    }
 
    // n must be odd at this point.  So we can skip one element (Note i = i +2)
    for (int i = 3; i <= sqrt(value); i = i+2)
    {
        // While i divides values, print i and divide values
        while (value%i == 0){
            //printf("%d ", i);
            
            if(is_mult){
                pending_buffer[prime_t[i]]++;
            } else {
                pending_buffer[prime_t[i]]--;
            }

            value = value/i;
        }
    }
 
    // This condition is to handle the case whien n is a prime number
    // greater than 2
    if (value > 2){
    
    
        if(is_mult){
                pending_buffer[prime_t[value]]++;
        } else {
                pending_buffer[prime_t[value]]--;
        }

        //printf ("3rd case: %llu ", value);
    }
}



void apply_lazy_buffer(int node)
{
    for(int i = 0; i < PRIME_COUNT; ++i){
        tree[node].prime_buffer[i] += tree[node].lazy_buffer[i];
    }   
    memset(tree[node].lazy_buffer, 0, sizeof(int)*PRIME_COUNT);
}


void update_lazy_buffer(int node)
{
    for(int i = 0; i < PRIME_COUNT; ++i){
        
        tree[node].lazy_buffer[i] += pending_buffer[i];
    }
}


long long recombine(int node)
{
    //print_primes(tree[node]);
    long long product = 1;
    for(int i = 0; i < PRIME_COUNT; ++i){
        product = (product * mod_pow(prime_t_rev[i], tree[node].prime_buffer[i])) % MOD;
    }

    return product;
}



void build ( int node, int l, int r)
{
    if(l == r){
        update_node(node, IS_LEAF, source[l], l, r);
    } else {
        int mid = (r+l)/2;
        build(2*node, l, mid);
        build(2*node+1, mid+1, r);
        update_node(node, NOT_LEAF, 0, l, r );
    }
}


// XXX
void set_prime_factors(int node, long long value)
{
    //printf("set_prime_value: %llu\n", value);
    if(value == 0) {return;}

    // Print the number of 2s that divide n
    while (value%2 == 0){
        //printf("2\n");
        tree[node].prime_buffer[0]++;
        value = value/2;
    }
 
    // n must be odd at this point.  So we can skip one element (Note i = i +2)
    for (int i = 3; i <= sqrt(value); i = i+2)
    {
        // While i divides values, print i and divide values
        while (value%i == 0){
            //printf("%d ", i);
            tree[node].prime_buffer[prime_t[i]]++;
            value = value/i;
        }
    }
 
    // This condition is to handle the case whien n is a prime number
    // greater than 2
    if (value > 2){
        tree[node].prime_buffer[prime_t[value]]++;
        //printf ("3rd case: %llu ", value);
    }
}

void update_node(int node, bool leaf, long long value, int left, int right)
{
    memset(tree[node].prime_buffer, 0, PRIME_COUNT*sizeof(int));
    memset(tree[node].lazy_buffer, 0, PRIME_COUNT*sizeof(int));
    
    tree[node].l = left;
    tree[node].r = right;

    if(leaf){
        set_prime_factors(node, value);
        tree[node].update_required = false;
        tree[node].mid = left;
        return;
    
    }

    update_node_gcd(node, 2*node, 2*node+1);
    tree[node].update_required = false;

    tree[node].mid = (left+right)/2;
}



long long recombine_query(int (&query_buffer)[PRIME_COUNT])
{
    //print_primes(tree[node]);
    long long product = 1;
    for(int i = 0; i < PRIME_COUNT; ++i){
        product = (product * mod_pow(prime_t_rev[i], query_buffer[i])) % MOD;
    }

    //if(product == 1) {return 0;}
    return product;
}



long long mod_pow( long long base, long long exp )
{
    base %= MOD;
    long long result = 1;
    
    while (exp > 0) {
        if (exp & 1) result = (result * base);
        base = (base * base) % MOD;
        exp >>= 1;
    }
    //printf("%llu\n", result);
    return result;
}



void init_primes(void)
{
    prime_t[2] = 0;
    prime_t[3] = 1;
    prime_t[5] = 2;
    prime_t[7] = 3;
    prime_t[11] = 4;
    prime_t[13] = 5;
    prime_t[17] = 6;
    prime_t[19] = 7;
    prime_t[23] = 8;
    prime_t[29] = 9;
    prime_t[31] = 10;
    prime_t[37] = 11;
    prime_t[41] = 12;
    prime_t[43] = 13;
    prime_t[47] = 14;

    prime_t_rev[0] = 2;
    prime_t_rev[1] = 3;
    prime_t_rev[2] = 5;
    prime_t_rev[3] = 7;
    prime_t_rev[4] = 11;
    prime_t_rev[5] = 13;
    prime_t_rev[6] = 17;
    prime_t_rev[7] = 19;
    prime_t_rev[8] = 23;
    prime_t_rev[9] = 29;
    prime_t_rev[10] = 31;
    prime_t_rev[11] = 37;
    prime_t_rev[12] = 41;
    prime_t_rev[13] = 43;
    prime_t_rev[14] = 47;
}



void postorder(int p, int indent)
{
    
    if(p > 2*(N-1)+1 ){return;}
    postorder(2*p+1, indent+4);
    if (indent) {
        std::cout << std::setw(indent) << ' ';
    }
    std::cout<<" /\n" << std::setw(indent) << ' ';
    
    if(tree[p].update_required){
            long long num = recombine_lazy(p);
            std::stringstream ss;
            ss << num;
            std::string r = "\033[1;31m(" + ss.str()+ ")\033[0m";
        std::cout << r;
    }
    
    std::cout<< recombine(p) << ", ["<<tree[p].l << ","<< tree[p].r <<"]" << "\n ";
    
    std::cout << std::setw(indent) << ' ' <<" \\\n";
    postorder(2*p, indent+4);
    
}




// #########################################################
//                      UNUSED
// #########################################################

long long recombine_lazy(int node)
{
    //print_primes(tree[node]);
    long long product = 1;
    for(int i = 0; i < PRIME_COUNT; ++i){
        product = (product * mod_pow(prime_t_rev[i], tree[node].lazy_buffer[i]))%MOD;
    }

    return product;
}



long long get_gcd(const node& a, const node& b)
{
    long long gcd = 1;
    for (int i = 0; i < PRIME_COUNT; ++i){
        long long count = min(a.prime_buffer[i], b.prime_buffer[i]);
        //printf("min: %d\n", count);
        
        for (int j = 0; j < count; ++j){
            gcd*=prime_t_rev[i];
            //printf("gcd*=%d: %llu\n",prime_t_rev[i], gcd );
        }       
    }
    // need to iterate through both node value sets 
    // if they are both not 0, need to take the minimum 
    return gcd;
}