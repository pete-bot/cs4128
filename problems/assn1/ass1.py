import sys, operator
from fractions import gcd

def accumulate(iterable, func=operator.add):
    it = iter(iterable)
    total = next(it)
    for element in it:
        total = func(total, element)
    return total


n_numbers = raw_input()
numbers = map(int, raw_input().split())
n_queries = raw_input()

for query in sys.stdin.readlines():
    input = query.strip().split()
    from_idx = int(input[1])
    to_idx = int(input[2])
    if (input[0] == 'Q'):
	#print numbers[from_idx:to_idx]
        print accumulate(numbers[from_idx:to_idx], gcd) % 1000000007
    elif (input[0] == 'M'):
        numbers = map(lambda x: x * int(input[3]), numbers)
    elif (input[0] == 'D'):
        numbers = map(lambda x: x / int(input[3]), numbers)
