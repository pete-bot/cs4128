#include <iostream>
#include <algorithm>
#include <cstdio>
#include <cstring>
#include <cassert>
#include <iomanip>
#include <string>
#include <sstream>
#define N 20010

#define MP 50
#define FACTORS 15
using namespace std;
// ############### Maps for prime factorisation##########
int primeMap[MP];
int reverseMap[FACTORS];

// ############### Tree ################

int arr[N];
int mod = 1000000007;

struct node{
    int val[FACTORS];
    int l,r,mid;
}tree[4*N];

// ############### Lazy tree############

struct lazy_elem{
	bool isLazy;
	int val[FACTORS];
}lazy[4*N];

void read_array(int n);
void init_mapping(void);
// ############### Lazy related ########
void apply_lazy_update(int index, int (&val)[FACTORS]);
void add_lazy(int index, int (&val)[FACTORS]);
void subtract_lazy(int index, int (&val)[FACTORS]);
// ############## Tree related ##############
void build_tree(int st_index, int l, int r);
void query(int st_index, int i, int j, int (&result)[FACTORS]);
void update(int st_index, int l, int r, int (&val)[FACTORS], bool action);

void merge_non_leaf(int index);
void update_node(int index, int (&val)[FACTORS], bool action);

void gcd(int (&left)[FACTORS], int (&right)[FACTORS], int (&parent)[FACTORS]);

//###########Number factorisation related ##############
void factorize(int n, int (&num)[FACTORS]);
long long power(long long x, long long k);
int assemble(int (&num)[FACTORS]);
// ################# Testing ###############
void postorder(int p, int indent);
void printTree(void);
int t;



int main(){
    char q[2];
    int m, n;  
    int z = scanf("%d", &n);
    assert(n  > 0);
    t = n;
    if(z == 0){
    	exit(1);
    }

    init_mapping();
    read_array(n);
    build_tree(1, 0, n-1);
    z = scanf("%d", &m);
	
	if(z == 0){
    	exit(1);
    }

    while(m--){
        z = scanf("%s", q);
        if(z == 0){
    		exit(1);
    	}

        int i, j;
        if(q[0] == 'Q'){
            z = scanf("%d %d", &i, &j);
            if(z == 0){
    			exit(1);
    		}
            int num[FACTORS];
            memset(num, 0, sizeof(int) * FACTORS);
            query(1,i,j-1,num);
            //printTree();
            printf("%d\n",assemble(num));
        }else if(q[0] == 'M'){
            int k;
            z = scanf("%d %d %d", &i, &j, &k);
            if(z == 0){
    			exit(1);
    		}
    		if(k == 1) continue;
            int num[FACTORS];
            memset(num, 0, sizeof(int) * FACTORS);
            factorize(k,num);
            update(1,i,j-1, num, false);
            //printTree();
        }else if(q[0] == 'D'){
            int k;
            z = scanf("%d %d %d", &i, &j, &k);
            if(z == 0){
    			exit(1);
   			}
   			if(k == 1) continue;
            int num[FACTORS];
            memset(num, 0, sizeof(int) * FACTORS);
            factorize(k,num);
            update(1,i,j-1,num, true);
            //printTree();
        }
    }
    return 0;
}

//Tree operations
void build_tree(int st_index, int l, int r){
    if(l == r){
    	factorize(arr[l], tree[st_index].val);
        tree[st_index].l = l;
        tree[st_index].r = r;
        tree[st_index].mid = l;
        return;
    }

    int mid = (l + r) >> 1;
    int left = st_index << 1;
    int right = left + 1;

    build_tree(left, l, mid);
    build_tree(right, mid + 1, r);
    tree[st_index].l = l;
    tree[st_index].r = r;
    tree[st_index].mid = mid;
    merge_non_leaf(st_index);
}

void query(int st_index, int i, int j, int (&result)[FACTORS]){
  
    int left_child = st_index << 1;
    int right_child  = left_child + 1;

    if(lazy[st_index].isLazy){
        apply_lazy_update(st_index,lazy[st_index].val);
        
        if(tree[st_index].l !=tree[st_index].r){
       
            add_lazy(left_child, lazy[st_index].val);
            add_lazy(right_child, lazy[st_index].val);
        }
        lazy[st_index].isLazy = false;
        memset(lazy[st_index].val, 0, sizeof(int) * FACTORS);
    }

    if(i == tree[st_index].l && j == tree[st_index].r){
        memcpy(result,tree[st_index].val, sizeof(int) * FACTORS);
        return;
    }

    int mid = tree[st_index].mid;
    if(j <= mid){
        query(left_child, i, j, result);
    }else if(i > mid){
        query(right_child, i, j,result);
    }else{
    	int leftResult[FACTORS];
    	memset(leftResult,0, sizeof(int) * FACTORS);
    	int rightResult[FACTORS];
    	memset(rightResult,0, sizeof(int) * FACTORS);
        query(left_child, i, mid, leftResult);
        query(right_child, mid+1, j,rightResult);
        gcd(leftResult, rightResult, result);
    }
}



void update(int st_index, int i, int j, int (&val)[FACTORS], bool action){
	int left_child = st_index << 1;
    int right_child  = left_child + 1;
    
     if(lazy[st_index].isLazy){
        apply_lazy_update(st_index,lazy[st_index].val);
        
        if(tree[st_index].l !=tree[st_index].r){
       
            add_lazy(left_child,lazy[st_index].val);
            add_lazy(right_child, lazy[st_index].val);
        }
        lazy[st_index].isLazy = false;
        memset(lazy[st_index].val, 0, sizeof(int) * FACTORS);
    }

    if(i == tree[st_index].l &&  j == tree[st_index].r){
    //if(tree[st_index].l== tree[st_index].r){
        update_node(st_index, val, action);
        if(tree[st_index].l !=tree[st_index].r){
        	if(action){
        		subtract_lazy(left_child, val);
            	subtract_lazy(right_child, val);
        	}else{
        		add_lazy(left_child, val);
            	add_lazy(right_child, val);
        	}
        }
        return;
    }

    int mid = tree[st_index].mid;
    if(j <= mid){
        update(left_child, i, j, val, action);
    }else if( i > mid){
        update(right_child, i, j, val, action);
    }else{

        update(left_child, i, mid, val, action);
        update(right_child, mid + 1, j, val, action);

    }
    merge_non_leaf(st_index);
}







void merge_non_leaf(int index){
    int left = index << 1;
    int right = left + 1;
    if(lazy[left].isLazy){
        apply_lazy_update(left,lazy[left].val);
        
        if(tree[left].l !=tree[left].r){
       		int lleft = left<<1;
            add_lazy(lleft,lazy[left].val);
            add_lazy(lleft+ 1, lazy[left].val);
        }
        lazy[left].isLazy = false;
        memset(lazy[left].val, 0, sizeof(int) * FACTORS);
    }
    if(lazy[right].isLazy){
        apply_lazy_update(right,lazy[right].val);
        
        if(tree[right].l !=tree[right].r){
       		int lright = right<<1;
            add_lazy(lright,lazy[right].val);
            add_lazy(lright + 1, lazy[right].val);
        }
        lazy[right].isLazy = false;
        memset(lazy[right].val, 0, sizeof(int) * FACTORS);
    }
    gcd(tree[left].val, tree[right].val, tree[index].val);
}











void update_node(int index, int (&val)[FACTORS], bool action){
    if(action){
    	for(int i = 0; i < FACTORS; i++){
    		tree[index].val[i]-=val[i];
    	}
        return;
    }
    for(int i = 0; i < FACTORS; i++){
    		tree[index].val[i]+=val[i];
    	}
}




//Weird gcd stuff
void gcd(int (&left)[FACTORS], int (&right)[FACTORS], int (&parent)[FACTORS]){
	for(int i = 0; i < FACTORS; i++){
		parent[i] = min(left[i],right[i]);
	}
}












void subtract_lazy(int index, int (&val)[FACTORS]){
	lazy[index].isLazy = true;
    for(int i = 0; i < FACTORS; ++i){
    	lazy[index].val[i] -= val[i];
	}
}

void add_lazy(int index, int (&val)[FACTORS]){
    lazy[index].isLazy = true;
    for(int i = 0; i < FACTORS; ++i){
    	lazy[index].val[i] += val[i];
	}
}

void apply_lazy_update(int index, int (&val)[FACTORS]){
    for(int i = 0; i < FACTORS; i++){
    		tree[index].val[i]+=val[i];
    	}
}








void factorize(int n, int (&num)[FACTORS]){
	for(int i = 2; i <= n; ++i){
		while(n % i == 0){
			num[primeMap[i]]++;
			n/=i;
		}
	}
}

int assemble(int (&num)[FACTORS]){
	int result = 1;
	for(int i = 0; i < FACTORS; i++){
		if(!num[i]){
			continue;
		}
    	result = (result * power(reverseMap[i], num[i])) % mod;
    }  
    return result;
}

long long power(long long x, long long k){
	if(k == 0){
		return 1;
	}
	long long a = power(x, k/2);
	a = (a * a) % mod;
	if(k % 2){
		a = (a * x) % mod;
	}
	return a;
}







void read_array(int n){
	int z;
    for(int i = 0; i < n; i++){
        z =scanf("%d", &arr[i]);
		if(z == 0){
    		exit(1);
    	}
    }
}

void init_mapping(void){
	primeMap[2] = 0;
	primeMap[3] = 1;
	primeMap[5] = 2;
	primeMap[7] = 3;
	primeMap[11] = 4;
	primeMap[13] = 5;
	primeMap[17] = 6;
	primeMap[19] = 7;
	primeMap[23] = 8;
	primeMap[29] = 9;
	primeMap[31] = 10;
	primeMap[37] = 11;
	primeMap[41] = 12;
	primeMap[43] = 13;
	primeMap[47] = 14;

	reverseMap[0] = 2;
	reverseMap[1] = 3;
	reverseMap[2] = 5;
	reverseMap[3] = 7;
	reverseMap[4] = 11;
	reverseMap[5] = 13;
	reverseMap[6] = 17;
	reverseMap[7] = 19;
	reverseMap[8] = 23;
	reverseMap[9] = 29;
	reverseMap[10] = 31;
	reverseMap[11] = 37;
	reverseMap[12] = 41;
	reverseMap[13] = 43;
	reverseMap[14] = 47;

}


unsigned long long assemble_unmod(int (&num)[FACTORS]){
	unsigned long long result = 1;
	for(int i = 0; i < FACTORS; i++){
		if(!num[i]){
			continue;
		}
    	result = (result * power(reverseMap[i], num[i]));
    }  
    return result;
}

long long power_unmod(long long x, long long k){
	if(k == 0){
		return 1;
	}
	long long a = power(x, k/2);
	a = (a * a);
	if(k % 2){
		a = (a * x);
	}
	return a;
}
void postorder(int p, int indent){
        
        
        if(p > 2*(t-1)+1 ){return;}
        
        postorder(2*p+1, indent+4);

        if (indent) {
            std::cout << std::setw(indent) << ' ';
        }
        std::cout<<" /\n" << std::setw(indent) << ' ';
        
        if(lazy[p].isLazy){
        	unsigned long long num = assemble_unmod(lazy[p].val);
        	stringstream ss;
        	ss << num;
        	std::string r = "\033[1;31m(" + ss.str()+ ")\033[0m";
        	std::cout << assemble_unmod(tree[p].val) << r << "[" << tree[p].l << ","<<tree[p].r <<"]" << "\n ";
    	}else{
    		std::cout << assemble_unmod(tree[p].val) << "[" << tree[p].l << ","<<tree[p].r <<"]" << "\n ";
    	}
        
        std::cout << std::setw(indent) << ' ' <<" \\\n";
        postorder(2*p, indent+4);

        /*for (int i = 0; i < 4*N+1; ++i){
            std::cout << tree[i] << " ";
        }
        std::cout << std::endl;*/
}
void printTree(void){
	printf("----------------------------------------\n");
	postorder(1,0);
    printf("----------------------------------------\n");
}
