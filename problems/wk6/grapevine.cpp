#include <cmath>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <cfloat>

#define LINE "###################################\n"
const double RAVEEN_CONSTANT = 1e9+7;
const int MAX_N = 200;


int num_brokers;
int graph[MAX_N][MAX_N];
std::vector<int> degree(MAX_N);


void init_graph(void);
void floyd_warshal(void);
void grapevine(void);
void print_graph(void);
void reset_degree(void);
void print_solution(void);
int min_dbl(int a, int b){return a < b ? a : b;}
int max_dbl(int a, int b){return a > b ? a : b;}


int main(int argc, char* argv[])
{	
	grapevine();
	return EXIT_SUCCESS;
}


void grapevine(void)
{
	num_brokers = -1; 
	int num_contacts = 0, broker_target, weight;
	
	while(true){
		scanf("%d",&num_brokers);
		if(num_brokers == 0){break;}

		init_graph();

		int broker_pos = 1;
		while(broker_pos <= num_brokers){
			scanf("%d", &num_contacts);
			degree[broker_pos]+=num_contacts;
			while(num_contacts--){
				scanf("%d %d", &broker_target, &weight );
				graph[broker_target][broker_pos] = weight;
				degree[broker_target]++;
			}

			broker_pos++;
		}

		bool break_flag =false;
		for(int i = 1; i <= num_brokers; ++i){
			if(degree[i] == 0){
				printf("disjoint\n");
				break_flag = true;	
				break;
			}
		}

		if (break_flag) break;
		//print_graph();
		floyd_warshal();
		//print_graph();

		// now need to search for an edge that 
		// has every vertex set to something that is not raveen constant

		// if a vertex has every edge == to raveen constant, disjoint set


		/*
		for(int i = 1; i < num_brokers; ++i ){
			raveen_count = 0;
			for(int j = 1; j < num_brokers; ++j){
				if(graph[j][i] == RAVEEN_CONSTANT){printf("RAV\t")}
			}
		}
		*/
		
		print_solution();
		reset_degree();
	}


}

void reset_degree(void)
{
	for(int i = 0; i < MAX_N; ++i){
		degree[i] = 0;
	}
}

void init_graph(void)
{

	for(int i = 0; i < MAX_N; ++i){
		for(int j = 0; j < MAX_N; ++j){
			graph[i][j] = RAVEEN_CONSTANT;	
		}
	}

	for(int i = 0; i < MAX_N; ++i){
		graph[i][i] = 0;
	}
}

/*
FLOYD_WARSHAL
1 let dist be a |V| × |V| array of minimum distances initialized to ∞ (infinity)
2 for each vertex v
3    dist[v][v] ← 0
4 for each edge (u,v)
5    dist[u][v] ← w(u,v)  // the weight of the edge (u,v)
6 for k from 1 to |V|
7    for i from 1 to |V|
8       for j from 1 to |V|
9          if dist[i][j] > dist[i][k] + dist[k][j] 
10             dist[i][j] ← dist[i][k] + dist[k][j]
11         end if
*/

void floyd_warshal(void)
{
	// floyd warshal
	for (int k = 1; k <= num_brokers; ++k){
	    for(int i = 1; i <= num_brokers; ++i){
	        for( int j = 1; j <= num_brokers; ++j){
		        graph[i][j] = min_dbl(graph[i][j], graph[i][k] +graph[k][j]);
	        }
	    }
	} 
}


void print_graph(void)
{
	int PRINT_MAX = num_brokers;
	printf(LINE);

	/*
	for(int i = 0; i < pairs.size(); ++i){
		printf("%d: %d, %d\n",i, pairs[i].first,pairs[i].second );
	}
	*/

	printf("\t");
	for(int i = 1; i <= PRINT_MAX; ++i){ printf("%d\t", i);}
	printf("\n");

	// print cost
	for(int i = 1; i <= PRINT_MAX; ++i){
		printf("%d\t", i);
		for(int j = 1; j <= PRINT_MAX; ++j){
			if(graph[j][i] == RAVEEN_CONSTANT){
				printf("INF\t");
			} else {
				printf("%d\t", graph[j][i]);
			}
		}
		printf("\n");

	}

}


void print_solution(void)
{
	int max = -RAVEEN_CONSTANT, min = RAVEEN_CONSTANT;
	long long row_sum = 0;

	int row_count, row = 0, i, j;
	for( i = 1; i <= num_brokers; ++i){
		row_sum = 0;
		for( j = 1; j <= num_brokers; ++j){
			row_sum += graph[j][i];
			if(row_sum > min){
				break;
			}
		}
		if(row_sum < min){
			row = i;
			min = row_sum;
		}
	}

	//printf("row: %d\n", row);
	for(int k = 1; k <= num_brokers; ++k){
		//printf("%d ", graph[k][row]);
		if( graph[k][row] > max) {max = graph[k][row]; }
	}
	printf("%d %d\n", row, max);


}