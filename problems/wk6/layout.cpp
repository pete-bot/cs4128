#include <cstdio>
#include <cstdlib>
#include <vector>

const int MAX_N = 1001;
const int RAVEEN_CONSTANT = 1e9+7;

int n_cows, n_like, n_dislike;
//int graph[MAX_N][MAX_N];

struct edge{
    int from;
    int to;
    int weight;
};

std::vector<edge> edges;
int dist[MAX_N];
//int par[MAX_N];

void layout(void);
void print_graph(void);
void init_dist(void);
bool has_neg_cycle(void);
void bellman_ford(void);
void print_dist(void);

int main(int argc, char* argv[])
{
    layout();
    
    return EXIT_SUCCESS;
}


void layout(void)
{
    scanf("%d %d %d", &n_cows, &n_like, &n_dislike);
    int a, b, weight;
    
    // positive arrangement, ie b-a <= weight
    while(n_like--) {
        scanf("%d %d %d",&a, &b, &weight);   
        // graph[a][b] = graph[b][a] = weight;
    	edge e = {a, b, weight};
    	edges.push_back(e);
    }
    
    // negative arrangement, negative weight ie a-b <= weight
    while(n_dislike--) {
        scanf("%d %d %d", &a, &b, &weight);
        // graph[a][b] = graph[b][a] = -weight;
    	edge e = {b, a, -weight};
    	edges.push_back(e);
    }   
    init_dist();
    //print_dist();

    //print_graph();
    bellman_ford();
	//print_dist();
    
    if(has_neg_cycle()){ printf("-1\n"); return; }
    if(dist[n_cows] >= RAVEEN_CONSTANT){ printf("-2\n"); return;}
    printf("%d\n",dist[n_cows]);
    // run Bellman-Ford
    
    
}

void init_dist(void)
{
	for(int i = 0; i < MAX_N; ++i){
		dist[i] = RAVEEN_CONSTANT;
	}
	dist[1] = 0;
}


// relax n_cows-1 times
void bellman_ford(void) 
{
    for (int i = 0; i < n_cows - 1; ++i) {
	    for (int edge_idx = 0; edge_idx < edges.size(); ++edge_idx) {
	        edge e = edges[edge_idx];
	        if (dist[e.to] > dist[e.from] + e.weight) { 
	        	dist[e.to] = dist[e.from] + e.weight;
	        }
	    }
    }
    //print_dist();
}

bool has_neg_cycle(void) {
    
    for (int edge_idx = 0; edge_idx < edges.size(); ++edge_idx) {
        edge e = edges[edge_idx];
        if (dist[e.to] > dist[e.from] + e.weight) { return true; }
    }
    return false;
}



void print_graph(void)
{
	for (int i = 0; i < edges.size(); ++i){
		printf("%d -> %d: %d\n", edges[i].from, edges[i].to, edges[i].weight );
	}
}

void print_dist(void)
{
	for(int i = 0; i <= n_cows; ++i){
		printf("%d: %d\n",i, dist[i]);
	}
}

/*
// returns true if the graph has a negative weight cycle
bool has_cycle() {
    bool relaxed = false;
    for (int edge_idx = 0; edge_idx < n_cows; ++edge_idx) {
        int edge = edges[edge_idx];
        if (dist[edge.to] > dist[edge.from] + edge.weight) { relaxed = true; }
    }
    return relaxed;
}
*/