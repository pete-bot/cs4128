#include <cmath>
#include <cstdio>
#include <vector>
#include <cstdlib>
#include <climits>
#include <cstring>
#include <cfloat>


const int MAX_N = 200;
const int START = 0;
const int END = 1;
const double RAVEEN_CONSTANT = 1e9+7;

int stones;
int PRINT_MAX;

std::vector<std::pair<int, int> >pairs;
double graph[MAX_N][MAX_N];

void frogger(void);
int abs_local(int a) {return a < 0 ? -1*a : a;}
void print_graph(void);
double get_dist(int a_x, int a_y, int b_x, int b_y);
double floyd_warshall(void);
double min_dbl(double a, double b){return a < b ? a : b;}
double max_dbl(double a, double b){return a > b ? a : b;}

int main(int argc, char* argv[])
{	
	//printf("int_max: %d\n", INT_MAX);
	frogger();	
	return EXIT_SUCCESS;
}

void frogger(void)
{

	int case_count = 1;
	int a, b;
	
	while(true){
		pairs.clear();
		memset(graph, RAVEEN_CONSTANT, sizeof(double)*MAX_N*MAX_N);

		scanf("%d", &stones);
		if(stones == 0) {break;}
		if(case_count > 1){printf("\n");}
		printf("Scenario #%d\n", case_count);
		
		PRINT_MAX = stones;

		int counter = 0;
		
		// construct graph
		while(counter < stones){
			scanf("%d %d", &a, &b);
			//printf("a: %d, b:%d\n", a, b);
			for(int i = 0; i < pairs.size(); ++i){
				double dist = get_dist(pairs[i].first, pairs[i].second, a, b );				
				graph[i][counter] = graph[counter][i] = dist;
			}
			pairs.push_back(std::make_pair(a,b));

			counter++;
		}
		
		//print_graph();


		printf("Frog Distance = %.3lf\n",floyd_warshall());
		case_count++;
	}


}


double get_dist(int a_x, int a_y, int b_x, int b_y)
{
	double x = a_x - b_x;
	double y = a_y - b_y;
	return sqrt(x*x + y*y);
}


void print_graph(void)
{
	
	for(int i = 0; i < pairs.size(); ++i){
		printf("%d: %d, %d\n",i, pairs[i].first,pairs[i].second );
	}

	printf("\t");
	for(int i = 0; i < PRINT_MAX; ++i){ printf("%d\t", i);}
	printf("\n");

	// print cost
	for(int i = 0; i < PRINT_MAX; ++i){
		printf("%d\t", i);
		for(int j = 0; j < PRINT_MAX; ++j){
			printf("%.3lf\t", graph[j][i]);
		}
		printf("\n");

	}

}

/*
FLOYD_warshall
1 let dist be a |V| × |V| array of minimum distances initialized to ∞ (infinity)
2 for each vertex v
3    dist[v][v] ← 0
4 for each edge (u,v)
5    dist[u][v] ← w(u,v)  // the weight of the edge (u,v)
6 for k from 1 to |V|
7    for i from 1 to |V|
8       for j from 1 to |V|
9          if dist[i][j] > dist[i][k] + dist[k][j] 
10             dist[i][j] ← dist[i][k] + dist[k][j]
11         end if
*/

double floyd_warshall(void)
{
	// init all vertex costs to 0
	/*
	for(int i = 0; i < stones; ++i){
		graph[i][i] = 0.0;
	}
*/
	// floyd warshall
	for (int k = 0; k < stones; ++k){
	    for(int i = 0; i < stones; ++i){
	        for( int j = 0; j < stones; ++j){
		        graph[i][j] = min_dbl(graph[i][j], max_dbl(graph[i][k], graph[k][j]));
	        }
	    }
	} 

	return graph[START][END];
}
