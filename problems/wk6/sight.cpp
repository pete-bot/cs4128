#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>
#include <algorithm> // find
#include <cassert>

#define LINE "------------------- new graph -----------------\n"
const double RAVEEN_CONSTANT = 1e9+7;
const int MAX_N = 1000;


int N, test_cases, M, from, to, weight, S, F;
int graph[MAX_N][MAX_N];
int duplicates[MAX_N][MAX_N];

int dist[MAX_N];
int master_dist[MAX_N];

bool master_flag;

bool sptSet[MAX_N];
int parent[MAX_N];
std::vector<int> path;

bool in_master[MAX_N];
int path_dist;


void sight(void);
void print_graph(void);
void init_graph(void);
void reset_graph(void);
void dijkstra(int src, int dst);
int minDistance(void);
void printSolution(void);
void populate_path(int dst);
void print_master(int dst);

int main(int argc, char* argv[])
{

	sight();
	
	return EXIT_SUCCESS;	
}



void sight(void)
{
	
	scanf("%d", &test_cases);

	while(test_cases--){
		//printf("\n");
		printf(LINE);
		init_graph();

		scanf("%d %d", &N, &M);
		//printf("nodes: %d, verts: %d\n", N, M);
		while(M--){
			assert( scanf("%d %d %d", &from, &to, &weight ) == 3);
			if(graph[from][to] != 0 && weight <= graph[from][to] ){
				graph[from][to] = weight;
				if(graph[from][to] == weight){
					duplicates[from][to]++;
				}
			}
		}

		scanf("%d %d", &S, &F);
		master_dist[S] = 0;
		in_master[S] = true;
		//printf("S: %d, F:%d\n", S, F);
		//print_graph();
		//printf("\n");
		
		
		  
		// find shortests path, store its total distance
		dijkstra(S, F);  // sets path and path_dist


		const std::vector<int> shortest_path = path;
		
		const int shortest_dist = path_dist;
		
		assert(shortest_path.size()>0);
		int valid_paths = 1;
		

		// for each node in the path
		for (int i = 0; i < (int)shortest_path.size(); ++i) {
		    int curr_node = shortest_path[i];
		
		    // for each adjacent node
		    for (int j = 0; j < MAX_N; ++j) {
		        if (graph[curr_node][j] == 0) { continue; }
		        // adjacent node is in master path
		        //if (std::find(shortest_path.begin(), shortest_path.end(),
        		//        graph[curr_node][j]) == shortest_path.end()) { continue; }
		        if (in_master[j]) { continue; }
		        
		        //printf("j: %d\n",j);
		        
		        reset_graph();
		        

		        // find the shortest path from that node to the destination
        		dijkstra(j, F);  // sets path and path_dist
		        


		        printf(" p_dist: %d\n graph[curr_node][j]: %d\n master_dist: %d\n both: %d\n",
		        		path_dist, graph[curr_node][j], master_dist[curr_node], path_dist + graph[curr_node][j] + master_dist[curr_node]);
		        printf("curr_node: %d \t j: %d\n", curr_node, j);
		        // if it is identical to the shortest path length, increment count
		        int new_dist = path_dist + graph[curr_node][j] + master_dist[curr_node];
		        if ( new_dist == shortest_dist) { 
		        	++valid_paths; 
		        }
		        
		        // if it is identical to the shortest path length + 1, increment count
		        if (new_dist == shortest_dist + 1) { 
		        	++valid_paths;
		        }
		    }
		}
		
		// print output
		printf("valid paths %d\n", valid_paths);
        	    
	}
}


void init_graph(void)
{
	memset(graph, 0, sizeof(int)*MAX_N*MAX_N);
	memset(duplicates, 0, sizeof(int)*MAX_N*MAX_N);
	
	path.clear();
	path_dist = 0;
	master_flag = false;

	for(int i = 0; i < MAX_N; ++i){
		dist[i] = RAVEEN_CONSTANT;
		master_dist[i] = RAVEEN_CONSTANT;
		sptSet[i] = false;
		parent[i] = -1;
		in_master[i] = false;
	}

	//dist[1] = 0;
}

void reset_graph(void)
{
   	path.clear();
	path_dist = 0;

	for(int i = 0; i < MAX_N; ++i){
		dist[i] = RAVEEN_CONSTANT;
		sptSet[i] = false;
		parent[i] = -1;
	}

	//dist[1] = 0; 
}


void print_graph(void)
{
	int PRINT_MAX = N;
	/*
	for(int i = 0; i < pairs.size(); ++i){
		printf("%d: %d, %d\n",i, pairs[i].first,pairs[i].second );
	}
	*/

	printf("\t");
	for(int i = 1; i <= PRINT_MAX; ++i){ printf("%d\t", i);}
	printf("\n");

	// print cost
	for(int i = 1; i <= PRINT_MAX; ++i){
		printf("%d\t", i);
		for(int j = 1; j <= PRINT_MAX; ++j){
			if(graph[i][j] == RAVEEN_CONSTANT){
				printf("INF\t");
			} else {
				printf("%d\t", graph[i][j]);
			}
		}
		printf("\n");

	}

}

void print_master(int dst)
{
	printf("Master dist:\n");
	for(int i = 1; i <= N; ++i){
		printf("%d: %d\n",i, master_dist[i] );
	}
	printf("in_master:\n");
	for(int i = 1; i <= N; ++i){
		printf("%d: %d\n",i, in_master[i] );
	}

}


void dijkstra(int src, int dst)
{
 	dist[src] = 0;
 	for (int count = 1; count <= N; count++){
		int u = minDistance();

		sptSet[u] = true;

		for (int v = 1; v <= N; v++)
			if (!sptSet[v] && graph[u][v] && dist[u] != RAVEEN_CONSTANT 
		                               && dist[u]+graph[u][v] < dist[v]){
		    	dist[v] = dist[u] + graph[u][v];
		    	master_dist[v] = dist[v];
		    	parent[v] = u;
			}
	}
	//printSolution();
	populate_path(dst);

    print_master(dst);
}



void printSolution()
{
    printf("Vertex\t Par\t Dist\n");
    for (int i = 1; i <= N; i++)
        if(dist[i] == RAVEEN_CONSTANT){
            printf("%d\t %d\t INF\n", i, parent[i]);
        }else{
        	printf("%d\t %d\t %d\n", i, parent[i] ,dist[i]);
        }
}

void populate_path(int dst)
{
    path.clear();
    //	printf("par:\n");
	int tmp = dst;
	path.push_back(tmp);
	
	in_master[tmp] = true;
	while(parent[tmp]!=-1){
		//printf("%d\n",parent[tmp]);
		path.push_back(parent[tmp]);
		if(!master_flag){in_master[parent[tmp]] = true;}
		tmp = parent[tmp];
	    
	}
	
    master_flag = true;


	path_dist = dist[dst];
	
	for(int i = (int)path.size()-1; i >= 0; --i ){
		printf("%d", path[i]);
		if(i != 0){printf("->");}
	}
	printf("\n");

	//printf("\t:%d\n", path_dist);

	
    
//    printf("in_master:\n");
//	for(int i = 1; i <= N; ++i ){
//		printf("%d: %d\n",i, in_master[i]);
//		//if(i != path.size()-1){printf("->");}
//	}
	
    
    //printf("path_cost: %d\n", path_dist);
}

int minDistance(void)
{
   // Initialize min value
   int min = RAVEEN_CONSTANT, min_index;
 
    for (int v = 1; v <= N; v++){
		if (sptSet[v] == false && dist[v] <= min){
		    min = dist[v], min_index = v;
		}
    }
 
   return min_index;
}