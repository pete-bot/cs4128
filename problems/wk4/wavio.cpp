#include <cstdio>
#include <cstdlib>
#include <cstring> 
#include <iostream>

//#define DEBUG

const int MAX_SEQ = 10010;
//const int RAVEEN_CONST = 1e9+7;
int seq[MAX_SEQ];
int rev_seq[MAX_SEQ];
int lis[MAX_SEQ];	// longest INCREASING

int lis_val[MAX_SEQ];
int lds_val[MAX_SEQ];

int lds[MAX_SEQ];	// longest DECREASING

void wavio(void);
void print_seq(int sequence[], int seq_num);
void get_lis(int seq[], int size, int lis[], int lis_val_in[]);
//void get_lds( int size);
int partition(int A[], int l, int r, int key);

inline void print_lis(int seq_num){ for(int i = 0; i < seq_num; ++i){ std::cout << lis[i] << " "; } std::cout << std::endl; }
inline void print_lds(int seq_num){ for(int i = 0; i < seq_num; ++i){ std::cout << lds[i] << " "; } std::cout << std::endl; }

inline void init_lis(int seq_num){ for(int i = 0; i < seq_num; ++i){ lis[i] = 1; lis_val[i] = 1;} }
inline void init_lds(int seq_num){ for(int i = 0; i < seq_num; ++i){ lds[i] = 1; lds_val[i] = 1;} }

//inline void init_lis(int seq_num){ memset(lis, 0, MAX_SEQ); memset(lis_val, 0, MAX_SEQ); }
//inline void init_lds(int seq_num){ memset(lds, 0, MAX_SEQ); memset(lds_val, 0, MAX_SEQ); }




inline void reset_seq(void){ 
	memset(seq, 0, sizeof(int)*MAX_SEQ);
	memset(rev_seq, 0, sizeof(int)*MAX_SEQ);
}

int get_min(int a, int b){return a< b?a:b;}




int main(int argc, char* argv[])
{
	wavio();
	return EXIT_SUCCESS;
}

/*

// new prog
seq:    5 4 1 2 3 2 1 4 5 
r_seq:  5 4 1 2 3 2 1 4 5 
lis:    1 1 1 2 3 2 1 4 5  
lds:    1 1 1 1 1 1 1 1 1

// old prog
seq:    5 4 1 2 3 2 1 4 5 
lis:        1 2 3 2   4 5 
lds:    5 4 1 2 3 2 1 1 1 
*/



void wavio(void)
{
	int seq_num = 0;
	
	while(std::cin >> seq_num){
		int in;
		for(int i = 0; i < seq_num; ++i){
			std::cin >> in;
			seq[i] = in;
			rev_seq[seq_num-(1+i)] = in;	
		}
		if(seq_num == 0){
			std::cout << 1 << std::endl;
			continue; 
		}
		
		init_lis(seq_num);
		init_lds(seq_num);
		
		
		get_lis(seq, seq_num, lis, lis_val);
		get_lis(rev_seq, seq_num, lds, lds_val);
		
		
	
		#ifdef DEBUG		

			std::cout << "seq:\t";
			print_seq(seq, seq_num);

			std::cout << "r_seq: \t";
			print_seq(rev_seq, seq_num);
			


			std::cout << "lis: \t";
			//print_lis(seq_num);
			

			for(int i = 0; i < seq_num; ++i){
				std::cout << lis_val[i] << " ";
			}	
			std::cout << std::endl;


			std::cout << "lds: \t";
			
			for(int i = 0; i < seq_num; ++i){
				std::cout << lds_val[seq_num-(1+i)] << " ";
			}	
			std::cout << std::endl;
			//print_lds(seq_num);
		#endif



		


		int min,max = 0;
		for(int i = 0; i < seq_num; ++i){
			//std::cout << "comparing: " << lis_val[i] << ", " << lds_val[seq_num-(1+i)] << std::endl;  
			min = get_min(lis_val[i], lds_val[seq_num-(1+i)]);
			if(min > max){
			//	std::cout << "updating max: " << min << std::endl;   
				max = min;
			}
		}	
		max = (max*2)-1;
		std::cout << max << std::endl; 
		

		/*
		//init_lds(seq_num);

		get_lds(seq_num);
		

		//std::cout << "lis: " << std::endl;
		//print_lis(seq_num);

		std::cout << "lds: " << std::endl;
		print_lds(seq_num);
		
		*/
		
		#ifdef DEBUG
			std::cout << "----------------------------------------\n" << std::endl;
		#endif

		reset_seq();
	}
}




void get_lis(int sequence[], int size, int lis_in[], int lis_val_in[])
{
    int len;
 
    lis_in[0] = -1;
    len = 0;
    //int lis_val_len = 1;
    for (int i = 0; i < size; i++){

        // if (sequence[i] < lis_in[0]){
        //     lis_in[0] = sequence[i];

        // } else 
        if (sequence[i] > lis_in[len]){
            lis_in[++len] = sequence[i];
    		lis_val_in[i] = len;
        
        } else {
            int index = partition(lis_in, 0, len-1, sequence[i]);
            lis_in[index] = sequence[i];
    		lis_val_in[i] = index;
    	}
    	
    }
}





int partition(int A[], int l, int r, int key)
{
    while(l <= r){
        int m = (l + r) >> 1;
        if(A[m]>=key){
            r = m-1;
        }else{
            l = m+1;
        }
    }
    return l;
}



void print_seq(int sequence[], int seq_num)
{
	for(int i = 0; i < seq_num; ++i ){
		std::cout << sequence[i] << " ";
	}
	printf("\n");
}


