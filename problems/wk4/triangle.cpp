#include <cstdio>
#include <cstdlib>

const int MAX_N = 110;
int tri[MAX_N][MAX_N];
int N;

void start_tri(void);
void print_tri(void);
int dp(int i, int j);
inline int max(int a, int b){ return a < b ? b : a; }


int main(int argc, char* argv[])
{

	start_tri();
	//print_tri();

	return EXIT_SUCCESS;
}

void start_tri(void)
{
	scanf("%d", &N);

	// read in 
	int count = 1;
	for(int i = 1; i <= N; ++i){
		for(int j =1; j<=i; ++j){
			scanf("%d",&tri[i][j]);
		}		
	}
	//print_tri();

	// calculate
	for(int i = N; i > 0; --i){
		for(int j =i; j > 0; --j){
			tri[i][j] += max( tri[i+1][j], tri[i+1][j+1]);;
		}		
	}

	//printf("result: %d\n", tri[1][1]);
	printf("%d\n", tri[1][1]);


}


// too slow
int dp(int i, int j)
{

	if(i > N || j > N) {return 0;}

	return tri[i][j] + max( dp(i+1,j), dp(i+1,j+1)); 

	
}

void print_tri(void)
{
	int count = 1;
	for(int i = 0; i <= N; ++i){
		for(int j =1; j<=i; ++j){
			printf("%d ",tri[i][j]);
		}		
		printf("\n");
	}
}