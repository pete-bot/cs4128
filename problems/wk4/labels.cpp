#include <iostream>
#include <string>
#include <cstdlib>

#define MAX_N 3000
int N;
char src[MAX_N];
char dst[MAX_N];


void initialise(void);
int process_strings(std::string source, std::string dest, int src_pos, int dst_pos, int inserted_letters);
inline int abs(int a){ return a < 0 ? a : -a;}
inline int min(int a, int b){return a<b ? a:b;}
int min_3(int a, int b, int c);


int main(int argc, char* argv[])
{
    initialise();
    return EXIT_SUCCESS;
}

void initialise(void)
{
    std::cin >> N;

    std::string a, b;
    std::cin >> a >> b;

    std::cout << process_strings(a,b,0,0,0) << std::endl;


    //std::cout << a << ", " << b << std::endl;
}

int process_strings(std::string src, std::string dst, int src_pos, int dst_pos, int inserted_letters)
{
	
	if( src_pos >= N || dst_pos >= N) {
	
		if(inserted_letters == 0){
			return abs(inserted_letters);
		} else {
			return 0;
		}
	
	}

	if( src[src_pos] == dst[dst_pos] ){
		return process_strings(src, dst, src_pos+1, dst_pos+1, inserted_letters);
	
	} else {
		
		int ins = 1 + process_strings(src, dst, src_pos+1, dst_pos, inserted_letters+1);
		int del = 1 + process_strings(src, dst, src_pos, dst_pos+1, inserted_letters-1);

		if ( dst[dst_pos] == 'o' && ( src[src_pos] == 'd'|| src[src_pos] == 'b' 
				|| src[src_pos] == 'p' || src[src_pos] == 'q' )) {
			
			int replace = 1 + process_strings(src, dst, src_pos+1, dst_pos+1, inserted_letters);
			return min_3(ins, del, replace);
		
		} else {
		
			return min(ins, del);
		
		}
		
	}

}

int min_3(int a, int b, int c)
{ 
	int min_2  = a < b ? a : b;
	return min_2 < c ? min_2 : c;
}