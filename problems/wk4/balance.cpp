#include <iostream>
#include <cstdlib>
#include <vector>

const int MAX_N = 20010;
const int RAVEEN_CONST = 1e9+7;


int num_nodes;
std::vector<int> edges[MAX_N];	// store edges
int sum[MAX_N]; 				// num nodes 'under' current node
int cut[MAX_N];					// the balance value at each node if that node is cut () 

void init(void);
void balance(void);
void dfs(int u, int parent);
void add_edge(int u, int v);
void print_edges( int num_nodes);
inline int maxi(int a, int b){return a<b?b:a;}
inline int mini(int a, int b){return a<b?a:b;}


int main(int argc, char* argv[])
{

	balance();

	return EXIT_SUCCESS;
}


// need to clean up after ourselves
void init(void)
{
	for(int i = 0; i <=num_nodes; ++i) {edges[i].clear();}
}

void balance(void)
{
	int tests, u, v;
	std::cin >> tests;
	while( tests-- ){
		std::cin >> num_nodes;
		for(int i = 1; i < num_nodes; ++i){
			std::cin >> u >> v;
			//std::cout << "considering: "<<u<<", "<<v<<std::endl;
			edges[u].push_back(v);
			edges[v].push_back(u);
		}

		dfs(1,-1);

		int min = RAVEEN_CONST;
		int min_index = -1;
		for(int i = 1; i <=num_nodes; ++i){
			if(cut[i] < min){min = cut[i]; min_index = i;}
		}

		//std::cout << "cut node: "<< min_index <<", min: " << min << std::endl;
		std::cout << min_index <<" " << min << std::endl;


		init();
	}

}


void dfs(int u, int parent)
{
	sum[u] = 1;
	cut[u] = 0;

	for(int i = 0; i < edges[u].size(); i++){
		int v = edges[u][i]; 
		if( v == parent){continue;}
		dfs(v,u);

		sum[u]+=sum[v];
		cut[u] = maxi(cut[u], sum[v]);
	}
	cut[u] = maxi(cut[u], num_nodes-sum[u]);
}


void add_edge(int u, int v)
{
	
}


void print_edges( int num_nodes)
{
	for(int i = 1; i <= num_nodes; ++i){
		std::cout << i << ": ";
		for(int j = 0; j < edges[i].size(); ++j){
			std::cout << edges[i][j] << ", ";
		}
		std::cout << std::endl;

	}
}