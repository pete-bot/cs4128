// C++ program to find length of longest increasing subsequence
// in O(n Log n) time
#include <iostream>
#include <string.h>
#include <stdio.h>
#include <cstdlib>
using namespace std;
 


const int MAX_SEQ = 10010;
//const int RAVEEN_CONST = 1e9+7;
int sequence[MAX_SEQ];
int lis[MAX_SEQ];   // longest INCREASING
int lds[MAX_SEQ];   // longest DECREASING

void wavio(void);
int get_lis(int A[], int size);
#define ARRAY_SIZE(A) sizeof(A)/sizeof(A[0])

inline void init_lis(int seq_num){ for(int i = 0; i < seq_num; ++i){ lis[i] = 1; } }
inline void init_lds(int seq_num){ for(int i = 0; i < seq_num; ++i){ lds[i] = 1; } }

inline void print_lis(int seq_num){ for(int i = 0; i < seq_num; ++i){ std::cout << lis[i] << " "; } std::cout << std::endl; }
inline void print_lds(int seq_num){ for(int i = 0; i < seq_num; ++i){ std::cout << lds[i] << " "; } std::cout << std::endl; }

inline void reset_seq(void){ memset(sequence, 0, sizeof(int)*MAX_SEQ);}
int get_min(int a, int b){return a< b?a:b;}


int main(int argc, char* argv[])
{

    wavio();

    
    return EXIT_SUCCESS;
}



void wavio(void)
{
    int seq_num = 0;
    
    while(std::cin >> seq_num){
        for(int i = 0; i < seq_num; ++i){
            std::cin >> sequence[i];
        }
        init_lis(seq_num);
        init_lds(seq_num);

        int inc = get_lis(sequence, seq_num);
        //get_lds(seq_num);
        //print_seq(seq_num);
        //int inc = get_lis(seq_num);
        //int dec = get_lds(seq_num);

        // print_lis(seq_num);
        // print_lds(seq_num);

        // std::cout << "seq_num: " << seq_num <<std::endl;
        // std::cout << "longest increasing: " << inc << std::endl;
        // std::cout << "longest decreasing: " << dec << std::endl;
        
        /*int min,max = 0;
        for(int i = 0; i < seq_num; ++i){
            min = get_min(lds[i],lis[i]);
            if(min > max){max = min;}
        }
        max = (max*2)-1;
        */
        std::cout << "max: "<< inc << std::endl; 
        // take min of these?
        std::cout << "----------------------------------------\n" << std::endl;



        reset_seq();
    }
}


 
// Binary search (note boundaries in the caller)
// A[] is ceilIndex in the caller
int CeilIndex(int A[], int l, int r, int key)
{
    while (r - l > 1)
    {
        int m = l + (r - l)/2;
        if (A[m]>=key)
            r = m;
        else
            l = m;
    }
    return r;
}
 
int get_lis(int A[], int size)
{
    // Add boundary case, when array size is one
 
    int *tailTable   = new int[size];
    int len; // always points empty slot
 
    memset(tailTable, 0, sizeof(tailTable[0])*size);
 
    tailTable[0] = A[0];
    len = 1;
    for (int i = 1; i < size; i++)
    {
        if (A[i] < tailTable[0])
            // new smallest value
            tailTable[0] = A[i];
 
        else if (A[i] > tailTable[len-1])
            // A[i] wants to extend largest subsequence
            tailTable[len++] = A[i];
 
        else
            // A[i] wants to be current end candidate of an existing
            // subsequence. It will replace ceil value in tailTable
            tailTable[CeilIndex(tailTable, -1, len-1, A[i])] = A[i];
    }
 
    delete[] tailTable;
    return len;
}
 
