#include <cstdio>
#include <cstdlib>
#include <vector>

const bool PAR = true;
const bool NO_PAR = false;

const int MAX_N = 100000;

struct employee{
    int pay;
    std::vector<int> children;
};


inline int min(int a, int b){ return a < b ? a: b;}

int get_pay(int node, bool node_included);
void holiday(void);
void print_workers(void);


int N;
employee workers[MAX_N];


int main(int argc, char* argv[])
{
	holiday();

    return EXIT_SUCCESS;
}


void holiday(void)
{
	int manager, dollars;
    scanf("%d", &N);

    for(int i = 1; i <= N; i++){
        scanf("%d %d", &manager, &dollars);
        workers[manager].children.push_back(i);
        workers[i].pay = dollars;
    }

    print_workers();
    printf("min pay: %d\n", get_pay(1, PAR ));
}


int get_pay(int node, bool node_included )
{
    if(workers[node].children.size() == 0){ return workers[node].pay;}

    if(node_included){
        if(workers[node].children.empty()){
            return workers[node].pay;
        } else {
            int sum = 0;
            for(unsigned int i = 0; i < workers[node].children.size(); ++i){
                sum+=min(get_pay(workers[node].children[i], PAR), get_pay(workers[node].children[i], NO_PAR));
            }
            return sum;
        }
        
    } else {
        if(workers[node].children.empty()){
            return 0;
        } else {
            int sum = 0;
            for(unsigned int i = 0; i < workers[node].children.size(); ++i){
                sum+=get_pay(workers[node].children[i], PAR);
            }
            return sum;
        }
    }
}

void print_workers(void)
{
  for(int i = 1; i <= N; ++i){
      printf("workers[%d]: %d, {", i, workers[i].pay);
      for(unsigned int j = 0; j < workers[i].children.size(); ++j){
          printf("%d", workers[i].children[j] );
          if(j+1 < workers[i].children.size()){printf(", ");}
      }

      printf("}\n");
  }
}
