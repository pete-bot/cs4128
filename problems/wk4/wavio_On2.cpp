#include <cstdio>
#include <cstdlib>
#include <cstring> 
#include <iostream>

const int MAX_SEQ = 10010;
//const int RAVEEN_CONST = 1e9+7;
int sequence[MAX_SEQ];
int lis[MAX_SEQ];	// longest INCREASING
int lds[MAX_SEQ];	// longest DECREASING

void wavio(void);
void print_seq(int seq_num);
int get_lis( int i);
int get_lds( int i, int seq_num);

inline void init_lis(int seq_num){ for(int i = 0; i < seq_num; ++i){ lis[i] = 1; } }
inline void init_lds(int seq_num){ for(int i = 0; i < seq_num; ++i){ lds[i] = 1; } }

inline void print_lis(int seq_num){ for(int i = 0; i < seq_num; ++i){ std::cout << lis[i] << " "; } std::cout << std::endl; }
inline void print_lds(int seq_num){ for(int i = 0; i < seq_num; ++i){ std::cout << lds[i] << " "; } std::cout << std::endl; }

inline void reset_seq(void){ memset(sequence, 0, sizeof(int)*MAX_SEQ);}

int get_min(int a, int b){return a< b?a:b;}

int main(int argc, char* argv[])
{

	wavio();

	
	return EXIT_SUCCESS;
}



void wavio(void)
{
	int seq_num = 0;
	
	while(std::cin >> seq_num){
		for(int i = 0; i < seq_num; ++i){
			std::cin >> sequence[i];
		}
		


		init_lis(seq_num);
		init_lds(seq_num);
		
		for(int i = 1, j = seq_num-2; i < seq_num; ++i, --j){
		
			get_lis(i);
			get_lds(j, seq_num);
		}	



		//print_seq(seq_num);
		//int inc = get_lis(seq_num);
		//int dec = get_lds(seq_num);

		// print_lis(seq_num);
		// print_lds(seq_num);

		// std::cout << "seq_num: " << seq_num <<std::endl;
		// std::cout << "longest increasing: " << inc << std::endl;
		// std::cout << "longest decreasing: " << dec << std::endl;
		
		int min,max = 0;
		for(int i = 0; i < seq_num; ++i){
			min = get_min(lds[i],lis[i]);
			if(min > max){max = min;}
		}
		max = (max*2)-1;
		std::cout << max << std::endl; 
		// take min of these?
		//std::cout << "----------------------------------------\n" << std::endl;



		reset_seq();
	}
}

// cal lis here

// bottom up implementation 
int get_lis( int i)
{
	
	//for(int i = 1; i < seq_num; ++i){
		for(int j = 0; j < i; ++j){
			if( (sequence[i] > sequence[j]) && lis[i] < lis[j]+1 ){
				lis[i] = lis[j]+1;
			}
		}
	//}
	

	/*
	int max = 0;
	for(int i = 0; i < seq_num; ++i){
		if (lis[i] > max) max = lis[i];
	}
	return max;
	*/
	return 0;
}
 

// bottom up implementation 
int get_lds( int i, int seq_num)
{
	
	
	//for(int i = seq_num-2; i >= 0; --i){
		for(int j = seq_num-1; j > i; --j){
			if( (sequence[i] > sequence[j]) && lds[i] < lds[j]+1 ){
				lds[i] = lds[j]+1;
			}
		}
	//}



	/*
	int max = 0;
	
	for(int i = 0; i < seq_num; ++i){
		if (lds[i] > max) max = lds[i];
	}
	
	return max;
	*/
	return 0;
}




void print_seq(int seq_num)
{
	for(int i = 0; i < seq_num; ++i ){
		std::cout << sequence[i] << " ";
	}
	printf("\n");
}