
	// check if hotter or colder
	// partition the room based 
	/*

	+-------------------+
	|\					|
	|  \				|
	|    \				|
	|  (1) \			|
	|        \			|
	|----------\		|
	|          | \		|
	|          |   \	|
	|          |     \	|
	|          |       \|
	+-------------------+

	*/


#include <cstdlib>
#include <algorithm>
#include <cstdio>
#include <cmath>
#include <vector>

# define x first
# define y second

const int MAX_STICKS = 100010;
const double EPS = 1e-8;

typedef std::pair<double, double> pt;
typedef std::pair< pt, pt > seg;
typedef std::pair < double, double> pt;

struct line {
	double a, b, c;
};

struct half_plane {
	line l;
	bool neg;
};


double TL_x = 0;
double TL_y = 10.0;

double TR_x = 10.0;
double TR_y = 10.0;

double BL_x = 0.0;
double BL_y = 0.0;

double BR_x = 10.0;
double BR_y = 0;

double x_in, y_in;
char state[10];


void solve_hotter(void);
pt operator -( pt a, pt b ) { return pt( a.x - b.x, a.y - b.y );}
bool zero( double x ) {	return fabs(x) <= EPS;}
double cross( pt a, pt b ) { return a.x * b.y - a.y * b.x;}
bool ccw ( pt a, pt b, pt c ) { return cross( b-a, c-a ) >= 0;}
pt intersect (line a, line b);
bool in_half_plane (half_plane p, pt q);
std::vector <pt> intersect_half_planes( std::vector <half_plane> half_planes );


