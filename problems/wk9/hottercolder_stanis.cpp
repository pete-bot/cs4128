#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <string>
#include <time.h>
#include <math.h>
#include <queue>
#include <stack>
#include <set>
#include <map>


using namespace std;
#define INF 10000000
#define eps 1e-8
#define pi acos(-1.0)

typedef long long ll;

int dcmp(double x){
    if(fabs(x)<eps)return 0;
    return x>0?1:-1;
}
struct Point{
    double x,y;
    Point(double _x=0,double _y=0){
        x=_x;y=_y;
    }
};

typedef Point Vec;
Point operator + (const Point &a,const Point &b){
    return Point(a.x+b.x,a.y+b.y);
}
Point operator - (const Point &a,const Point &b){
    return Point(a.x-b.x,a.y-b.y);
}
Point operator * (const Point &a,const double &p){
    return Point(a.x*p,a.y*p);
}
Point operator / (const Point &a,const double &p){
    return Point(a.x/p,a.y/p);
}
bool operator <(const Point &a,const Point &b){
    return a.x<b.x||(a.x==b.x&&a.y<b.y);
}

bool operator == (const Point &a,const Point &b){
    return dcmp(a.x-b.x)==0&&dcmp(a.y-b.y)==0;
}
double Dot(Point  a,Point b){
    return a.x*b.x+a.y*b.y;
}
double Length(Point a){
    return sqrt(Dot(a,a));
}

double Angle(Point a,Point b){
    return acos(Dot(a,b)/Length(a)/Length(b));
}

double angle(Point a){
    return atan2(a.y,a.x);
}

double Cross(Point a,Point b){
    return a.x*b.y-a.y*b.x;
}

Point vecunit(Point a){
    return a/Length(a);
}

Point Normal(Point a){
    return Point(-a.y,a.x)/Length(a);
}

Point Rotate(Point a,double rad){
    return Point(a.x*cos(rad)-a.y*sin(rad),a.x*sin(rad)+a.y*cos(rad));
}

double Area2(Point a,Point b,Point c){
    return Length(Cross(b-a,c-a));
}


struct Line{
    Point p,v;
    double ang;
    Line(){};
    Line(Point p,Point v):p(p),v(v){
        ang=atan2(v.y,v.x);
    }
    bool operator <(const Line &L) const {
        return ang<L.ang;
    }
};

bool OnLeft(const Line &L,const Point &p){
    return dcmp(Cross(L.v,p-L.p))>=0;
}

Point GetLineIntersection(Point p,Point v,Point q,Point w){
    Point u=p-q;
    double t=Cross(w,u)/Cross(v,w); 
    return p+v*t;
}

Point GetLineIntersection(Line a,Line b){
    return GetLineIntersection(a.p,a.v,b.p,b.v);
}

vector<Point> HPI(vector<Line> L){
    int n=L.size();
    sort(L.begin(),L.end());//All the half plane in accordance with the polar angle sort. 
    
    
    /*
    for(int i=0;i<n;i++){
        cout<<"han  "<<i<<" ";
        printf("%.2lf  %.2lf  %.2lf  %.2lf  %.2lf\n",L[i].p.x,L[i].p.y,L[i].v.x,L[i].v.y,L[i].ang);
    }
    */

    
    int first,last;
    vector<Point> p(n);
    vector<Line> q(n);
    vector<Point> ans;
    q[first=last=0]=L[0];
    for(int i=1;i<n;i++){
        while(first<last&&!OnLeft(L[i],p[last-1]))last--;//Remove the top of the half plane
        while(first<last&&!OnLeft(L[i],p[first]))first++;//Remove the bottom half plane
        q[++last]=L[i];//The half plane current if the double ended queue at the top. 
        if(fabs(Cross(q[last].v,q[last-1].v))<eps){//For the polar angle the same, selective retention of a. 
            last--;
            if(OnLeft(q[last],L[i].p))q[last]=L[i];
        }
        if(first<last)p[last-1]=GetLineIntersection(q[last-1],q[last]);//Calculation of the top of the queue half plane intersection. 
    }
    while(first<last&&!OnLeft(q[first],p[last-1]))last--;//Remove the top of the queue of useless half plane. 
    if(last-first<=1)return ans;//Half plane degradation
    p[last]=GetLineIntersection(q[last],q[first]);//The top of the queue and the first intersection calculation. 
    for(int i=first;i<=last;i++)ans.push_back(p[i]);//The queue of point. 
    return ans;
}
double PolyArea(vector<Point> p){
    int n=p.size();
    double ans=0;
    for(int i=1;i<n-1;i++)
        ans+=Cross(p[i]-p[0],p[i+1]-p[0]);
    return fabs(ans)/2;
}

int main(){
    Point one, two;
    vector<Line> half_planes;
    one = Point(0.0, 0.0); 
    two = Point(10.0, 0.0);
    half_planes.push_back(Line(one, two - one));
    one = Point(10.0, 0.0); 
    two = Point(10.0, 10.0);
    half_planes.push_back(Line(one, two - one));
    one = Point(10.0, 10.0); 
    two = Point(0.0, 10.0);
    half_planes.push_back(Line(one, two - one));
    one = Point(0.0, 10.0); 
    two = Point(0.0, 0.0);
    half_planes.push_back(Line(one,two - one));
    vector<Point> res = HPI(half_planes);
    
    
    //for(vector<Point>::iterator it = res.begin(); it != res.end(); it++){
    //    printf("[%lf %lf] ",(*it).x, (*it).y);
    //}
    Point prev = Point(0.0, 0.0);
    double x,y;
    char str[6];
    bool nosol = false;
    while(scanf("%lf %lf %s",&x, &y, str)!= EOF){
        if(nosol){
            printf("0.00\n");
            continue;
        }
        Point cur(x,y);
        Point mid = (prev + cur)/2;
        Vec perp = Normal(cur - prev);
        if(str[0] == 'S'){
            printf("0.00\n");
            nosol = true;
            continue;
        }else if(str[0] == 'H'){
            //printf("perp.x: %lf, perp.y: %lf\n", perp.x, perp.y);
            half_planes.push_back(Line(mid,Vec(-perp.x,-perp.y)));     
        }else if(str[0] == 'C'){
            half_planes.push_back(Line(mid,perp));
        }
        res = HPI(half_planes);
        double area = PolyArea(res);
        printf("%.2lf\n", area);
        prev = cur;
    }
    
    return 0;
}