#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <sstream>
#include <cstring>
#include <climits>
#include <string>
#include <ctime>
#include <cmath>
#include <queue>
#include <stack>
#include <set>
#include <map>


	/*
   0,10				  10,10
	+-------------------+
	|\					|
	|  \				|
	|    \				|
	|  (1) \			|
	|        \			|
	|----------\		|
	|          | \		|
	|          |   \	|
	|          |     \	|
	|          |       \|
	+-------------------+
   0,0				  10,0
	*/


//#################################################################
// 				USEFUL HPI CODE THAT STANIS FOUND XXX
// 		PUT IN NOTES FOR FINAL (good luck typing it all up though)
//#################################################################


#define INF 10000000
#define eps 1e-8
#define pi acos(-1.0)

typedef long long ll;

int dcmp(double x){
    if(fabs(x)<eps)return 0;
    return x>0?1:-1;
}
struct Point{
    double x,y;
    Point(double _x=0,double _y=0){
        x=_x;y=_y;
    }
};

typedef Point Vec;
Point operator + (const Point &a,const Point &b){
    return Point(a.x+b.x,a.y+b.y);
}
Point operator - (const Point &a,const Point &b){
    return Point(a.x-b.x,a.y-b.y);
}
Point operator * (const Point &a,const double &p){
    return Point(a.x*p,a.y*p);
}
Point operator / (const Point &a,const double &p){
    return Point(a.x/p,a.y/p);
}
bool operator <(const Point &a,const Point &b){
    return a.x<b.x||(a.x==b.x&&a.y<b.y);
}

bool operator == (const Point &a,const Point &b){
    return dcmp(a.x-b.x)==0&&dcmp(a.y-b.y)==0;
}
double Dot(Point  a,Point b){
    return a.x*b.x+a.y*b.y;
}
double Length(Point a){
    return sqrt(Dot(a,a));
}

double Angle(Point a,Point b){
    return acos(Dot(a,b)/Length(a)/Length(b));
}

double angle(Point a){
    return atan2(a.y,a.x);
}

double Cross(Point a,Point b){
    return a.x*b.y-a.y*b.x;
}

Point vecunit(Point a){
    return a/Length(a);
}

Point Normal(Point a){
    return Point(-a.y,a.x)/Length(a);
}

Point Orth(Point a, Point b)
{
	Point c = b-a;
	return Point(-(c.y),(c.x))/Length(c);
}

Point mid_point(Point a, Point b)
{
	return (a+b)/2.0;
}

Point rev(Point a)
{
	return Point(-a.x, -a.y);
}

Point Rotate(Point a,double rad){
    return Point(a.x*cos(rad)-a.y*sin(rad),a.x*sin(rad)+a.y*cos(rad));
}

double Area2(Point a,Point b,Point c){
    return Length(Cross(b-a,c-a));
}


struct Line{
    Point p,v;
    double ang;
    Line(){};
    Line(Point p,Point v):p(p),v(v){
        ang=atan2(v.y,v.x);
    }
    bool operator <(const Line &L) const {
        return ang<L.ang;
    }
};

bool OnLeft(const Line &L,const Point &p){
    return dcmp(Cross(L.v,p-L.p))>=0;
}

Point GetLineIntersection(Point p,Point v,Point q,Point w){
    Point u=p-q;
    double t=Cross(w,u)/Cross(v,w); 
    return p+v*t;
}

Point GetLineIntersection(Line a,Line b){
    return GetLineIntersection(a.p,a.v,b.p,b.v);
}

std::vector<Point> HPI(std::vector<Line> L){
    int n=L.size();
    std::sort(L.begin(),L.end());//All the half plane in accordance with the polar angle sort. 
    
    /*printf("p.x, p.y, v.x, v.y, angle (rads)\n");
    for(int i=0;i<n;i++){
        printf("%.2lf\t  %.2lf\t  %.2lf\t  %.2lf\t  %.2lf\t\n",L[i].p.x,L[i].p.y,L[i].v.x,L[i].v.y,L[i].ang);
    }
    */

    int first,last;
    std::vector<Point> p(n);
    std::vector<Line> q(n);
    std::vector<Point> ans;
    q[first=last=0]=L[0];
    for(int i=1;i<n;i++){
        while(first<last&&!OnLeft(L[i],p[last-1]))last--;//Remove the top of the half plane
        while(first<last&&!OnLeft(L[i],p[first]))first++;//Remove the bottom half plane
        q[++last]=L[i];//The half plane current if the double ended queue at the top. 
        if(fabs(Cross(q[last].v,q[last-1].v))<eps){//For the polar angle the same, selective retention of a. 
            last--;
            if(OnLeft(q[last],L[i].p))q[last]=L[i];
        }
        if(first<last)p[last-1]=GetLineIntersection(q[last-1],q[last]);//Calculation of the top of the queue half plane intersection. 
    }
    while(first<last&&!OnLeft(q[first],p[last-1]))last--;//Remove the top of the queue of useless half plane. 
    if(last-first<=1)return ans;//Half plane degradation
    p[last]=GetLineIntersection(q[last],q[first]);//The top of the queue and the first intersection calculation. 
    for(int i=first;i<=last;i++)ans.push_back(p[i]);//The queue of point. 
    return ans;
}


double PolyArea(std::vector<Point>& p){
    int n=p.size();
    double ans=0;
    for(int i=1;i<n-1;i++)
        ans+=Cross(p[i]-p[0],p[i+1]-p[0]);
    return fabs(ans)/2;
}


//#################################################################
// 				END USEFUL HPI CODE THAT STANIS FOUND XXX
//#################################################################


void init_set(std::vector<Line>& bounds);
void solve_hotter(void);
void print_points(std::vector<Point>& points);

char state[10];

int main(int argc, char* argv[])
{
	solve_hotter();
	return EXIT_SUCCESS;
}


void solve_hotter(void)
{
	std::vector<Line> bounds;
	init_set(bounds);
	std::vector<Point> points;

	Point curr(0.0,0.0), next;
	bool same_flag = false;
	while(scanf("%lf %lf %s", &next.x, &next.y, state) != EOF){
		if(state[0] == 'S') {same_flag = true;}
		if(same_flag){printf("0.00\n"); continue; }
		Point mid = mid_point(curr, next);
		Point norm = Orth(curr, next);
		
		if(state[0] == 'H'){
			bounds.push_back(Line(mid, rev(norm)));
		} else if (state[0] == 'C'){
			bounds.push_back(Line(mid, norm));
		}
		points = HPI(bounds);
		printf("%.2lf\n", PolyArea(points));
		curr = next;
	}

}

// init set bounds.
void init_set(std::vector<Line>& bounds)
{
	
	/*
			Initial bounds

		D: 0,10			   C: 10,10
			+-------------------+
			|\					|
			|  \				|
			|    \				|
			|  (1) \			|
			|        \			|
			|----------\		|
			|          | \		|
			|          |   \	|
			|          |     \	|
			|          |       \|
			+-------------------+
		A: 0,0			   B: 10,0
	*/
    // push into bounds the vectors that bound our problem.
	bounds.push_back(Line(Point(0.0,0.0), Point(10.0,0.0)));
	bounds.push_back(Line(Point(10.0,0.0), Point(0.0,10.0)));
	bounds.push_back(Line(Point(10.0,10.0), Point(-10.0,0.0)));
	bounds.push_back(Line(Point(0.0,10.0), Point(0.0,-10.0)));

}

void print_points(std::vector<Point>& points)
{
	printf("size of points: %lu\n", points.size());
	printf("points:");
	for(std::vector<Point>::iterator it = points.begin(); it != points.end(); it++){
        printf("[%lf %lf]\n",(*it).x, (*it).y);
    }
    printf("\n");
	
}



