#include "hotter.h"

int main(int argc, char* argv[])
{
	solve_hotter();
	return EXIT_SUCCESS;
}


void solve_hotter(void)
{

	while(scanf(" %lf %lf %s ", &x_in, &y_in, state)){
		printf("x: %.3lf, y: %.3lf, state: %s\n", x_in, y_in, state);
	}

}


pt intersect (line a, line b){
	//double d = a.a * b.b - a.b * b.a;
	double y = (a.a * b.c - a.c * b.a)/(a.b * b.a - a.a * b.b) ;
	double x = (a.c * b.b - a.b * b.c)/(a.b * b.a - a.a * b.b) ;
	
	return pt(x, y ) ;
}

bool in_half_plane ( half_plane p , pt q ) {
	if ( p.neg ) return p.l.a * q.x + p.l.b*q.y + p.l.c <= EPS;
	else return p.l.a * q.x + p.l.b * q.y + p.l.c >= - EPS;
}

/*

std::vector < pt > intersect_half_planes( vector <half_plane> half_planes ) {
	int n = half_planes.size();
	std::vector <pt> pts;

	for ( int i = 0; i<n; i++){
		for ( int j = i+1; j<n; j++) {
			
			pt p = intersect( half_planes[i].l, half_planes[j].l ) ;
			bool fail = false ;
			
			for ( int k = 0; k < n ; k ++){
				if (!in_half_plane (half_planes[k], p))	fail = true;
			}

			if (!fail) pts.push_back(p);
		}
	}
	
	vector <pt> res = pts ;
	if (pts.size() > 2)
	pts = convex_hull(res);
	return pts;
}



*/