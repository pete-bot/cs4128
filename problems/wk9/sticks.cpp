#include <cstdlib>
#include <algorithm>
#include <cstdio>
#include <cmath>
#include <vector>

# define x first
# define y second

const int MAX_STICKS = 100010;
const double EPS = 1e-8;

typedef std::pair<double, double> pt;
typedef std::pair < pt, pt > seg;

pt operator -( pt a, pt b ) { return pt( a.x - b.x, a.y - b.y );}
bool zero( double x ) {	return fabs(x) <= EPS;}
double cross( pt a, pt b ) { return a.x * b.y - a.y * b.x;}
bool ccw ( pt a, pt b, pt c ) { return cross( b-a, c-a ) >= 0;}


double dist (pt p , pt q );
bool intersect (seg a, seg b);
bool overlap (seg a , seg b);

seg sticks[MAX_STICKS];
//bool top[MAX_STICKS];


int stick_count,i,j;

void solve(void);


int main( int argc, char* argv[])
{
	solve();
	return EXIT_SUCCESS;
}

void solve(void)
{
	
	while(scanf(" %d ", &stick_count) != EOF){
		if(stick_count == 0) return;

		/*
		for(int i = 0; i <=stick_count; ++i){
			top[i] = true;
		}
		*/

		seg s;

		// reading in 
		for(int i = 1; i <= stick_count; ++i){
			//top[i] = true;
			scanf(" %lf %lf %lf %lf ", &sticks[i].first.x, &sticks[i].first.y, 
				&sticks[i].second.x, &sticks[i].second.y );
			// printf("reading:\n");
			// printf("[ %lf, %lf] -> [%lf, %lf]\n",  sticks[i].first.x, sticks[i].first.y, 
			//	sticks[i].second.x, sticks[i].second.y);
		
			/*
			for(int j = 1; j < i; ++j){
				if(!top[j]) continue;
				if(intersect(sticks[i], sticks[j])){
					top[j] = false;
				}
			}
			*/
		}
		/*
		bool first_flag = true;
		printf("Top sticks: ");
		for(int i = 1; i <= stick_count; ++i ){
			if(top[i] == true){
				if(!first_flag){
					printf(",");
				}
				printf(" %d", i );
				first_flag = false;
			}
		}
		*/

		// scan for intersections
		printf("Top sticks:");  
        for(i=1;i<stick_count;++i){  
            for(j=i+1;j<=stick_count && !intersect(sticks[i],sticks[j]) ;++j);
            if(j>stick_count)printf(" %d,",i);  
        }  
        printf(" %d.\n",stick_count);  
	
	}

}

// RAVEEN CODE TO FOLLOW 

bool overlap( seg a , seg b ) { 
	return zero( cross( a.y - a.x , b.x - a.x )) 
		&& zero( cross( a.y - a.x , b.y - a.x));
}

double dist( pt p , pt q ) {
	return sqrt(( p.x - q.x ) *( p.x - q.x ) +( p.y - q.y ) *( p.y - q.y ));
}

bool intersect(seg a, seg b) {
	
	if ( overlap (a, b ) ) {
		double maxDist = 0;
		maxDist = std::max(maxDist, dist(a.x, a.y));
		maxDist = std::max(maxDist, dist(a.x, b.x));
		maxDist = std::max(maxDist, dist(a.x, b.y));
		maxDist = std::max(maxDist, dist(a.y, b.x));
		maxDist = std::max(maxDist, dist(a.y, b.y));
		maxDist = std::max(maxDist, dist(b.x, b.y));
		return maxDist < dist( a.x , a.y ) + dist( b.x , b.y ) + EPS ;
	}
	return ccw( a.x , a.y , b.x ) != ccw( a.x , a.y , b.y )
		&& ccw( b.x , b.y , a.x ) != ccw( b.x , b.y , a.y );

}
/*
*/