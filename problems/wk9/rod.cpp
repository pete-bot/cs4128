#include <cstdlib>
#include <cstdio>
#include <cmath>

const double EPS = 1e-8;

inline int cmp_dbl(double x, double y)
{ 
	if ( fabs (x - y ) < EPS ) {
		return 0;
	} else if (x > y){
		return 1;
	} else {
		return -1;
	}
}


double L, delta_T, coeff, result;
void solve(void);




int main(int argc, char* argv[])
{
	solve();	
	return EXIT_SUCCESS;
}

void solve(void)
{
	
	while( scanf(" %lf %lf %lf ", &L, &delta_T, &coeff) != EOF){
		if( L < 0 || delta_T < 0 || coeff < 0){ return; }

		//printf("L: %0.3lf, delta_T: %lf, coeff: %lf\b\n"
		//	,L, delta_T, coeff );

		double arc = (1+delta_T*coeff)*L, arc_prime;
		
		double lo = 0, hi = L/2.0;
		double height = (hi+lo)/2.0;
		double radius, theta;

		while( (hi-lo) > EPS ){
			//printf("height: %lf\n", height);
			
			radius = height/2.0 + (L*L)/(8.0*height);
			
			theta = 2.0*asin(L/(2.0*radius));
			
			arc_prime = radius*theta;

			if (arc > arc_prime){
				lo = height;
			} else {
				hi = height;
			}

			height = (hi+lo)/2.0; 

		}
		printf("%.3lf\n", height);


	}



}

