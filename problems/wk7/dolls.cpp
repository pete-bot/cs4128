#include <cstdio>
#include <cstdlib>
#include <utility>
#include <vector>
#include <algorithm>
#include <cstring>


const int RAVEEN_CONST = 1e9+7;
const int MAX_N = 20010;

typedef std::pair<int, int> pii;
int tests, m;
int lis[MAX_N];


std::vector<pii> dolls;


void doll(void);
int get_lis(int size);
int CeilIndex(std::vector<pii> A, int l, int r, int key);


void print_lis(int size);
void print_dolls(void);
void reset(void);
bool cmp(pii a, pii b);



int main(int argc, char* argv[])
{

	doll();
	return EXIT_SUCCESS;
}

void doll(void)
{
	int w, h, num_dolls;
	scanf("%d", &tests);
	while(tests--){
		scanf("%d",&m);
		num_dolls = m;
		while(m--){
			scanf("%d %d", &w, &h);
			dolls.push_back(std::make_pair(w,h));

		}

		// sort vector
		std::sort(dolls.begin(), dolls.end(), cmp);

		// call longest inc. subseq here. 





		//print_dolls();
		// execute longest increasing subsequence here on dolls set.  
		int len = get_lis(num_dolls);
		//printf("lis: ");
		//print_lis(len);
		printf("%d\n", len);

		// reset dolls etc. 
		reset();
	}
}


int CeilIndex(int l, int r, int key)
{	
	int m;
    while (l < r){
        m = (l+r)/2;
        if (lis[m]>=key)
            l = m+1;
        else
            r = m;
    }
    return l;
}
 
// pass in length of vector
int get_lis(int size)
{
 
    int len = 1; // always points empty slot
       
    for (int i = 0; i < size; i++){
    	// printf("dol[%d]: %d, lis[%d]: %d\n", i, dolls[i].second, i, lis[len] );
        
        int index = CeilIndex(0, len, dolls[i].second); 

        if (index == len ){
        	lis[len++] = dolls[i].second; 
        	

        } else {
            
            lis[index] = dolls[i].second;
        }
    }

    return len;
}
 

void reset(void)
{
	dolls.clear();
	memset(lis, 0, sizeof(int)*MAX_N);
}

bool cmp(pii a, pii b)
{
	if(a.first == b.first){
		return a.second > b.second;
	} else {
		return a.first < b.first;
	}
}

void print_lis(int size)
{
	for(int i = 1; i <= size; ++i){
		printf("%d ", lis[i]);
	}
	printf("\n");
}

void print_dolls(void)
{
	for(int i = 0; i < dolls.size(); ++i){
		printf("(%d, %d), ", dolls[i].first, dolls[i].second  );
	}
	printf("\n");
}
