#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <vector>
#include <string>

const int MAX_N = 610;
const int RAVEEN_CONST = 1e9+7;

int tests, N, counter;

int graph[MAX_N][MAX_N];
int input_map[MAX_N][MAX_N];

bool seen[MAX_N];
int match[MAX_N];

void skimming(void);

bool bpm(int u);
int maxBPM(void);

void print_map(void);
void reset_graph(void);


void print_graph(void);


int main(int argc, char* argv[])
{

	skimming();

	return EXIT_SUCCESS;
}


void skimming(void)
{


	scanf("%d",&tests);
	
	for(int count = 1; count <= tests; ++count){
		scanf("%d", &N);
		
		// read in data
		char temp = 0;
		counter = 1;
		for(int i = 1; i <= N; ++i){
			for(int j = 1; j<= N; ++j ){
				scanf("%c", &temp);
				if(temp == '\n'){
					scanf("%c", &temp);
				}

				if(temp == '.'){
					input_map[i][j] = 0;
				} else if(temp == '#'){
					input_map[i][j] = counter++;
				}
			}
		}

		int start, end;
		// generate adjacency matrix 
		for(int i = 1; i <= N; ++i){
			for(int j = 1; j <=N; ++j){
				
				if(input_map[i][j] > 0){

					if(input_map[i-1][j] > 0 ){
						start = input_map[i][j];
						end = input_map[i-1][j];
						
						graph[start][end] = 1;
						graph[end][start] = 1;

					}

					if(input_map[i+1][j] > 0 ){
						start = input_map[i][j];
						end = input_map[i+1][j];
						graph[start][end] = 1;
						graph[end][start] = 1;					
					}	

					if(input_map[i][j-1] > 0 ){
						start = input_map[i][j];
						end = input_map[i][j-1];
						graph[start][end] = 1;
						graph[end][start] = 1;

					}

					if(input_map[i][j+1] > 0 ){
						start = input_map[i][j];
						end = input_map[i][j+1];
						
						graph[start][end] = 1;
						graph[end][start] = 1;
					}
				}
			}
			
		}

		//printf("N: %d\n", N);
		//print_map();
		//print_graph();

		int result = maxBPM();
		printf("Case %d: %d\n",count, result>>1);

		reset_graph();



	}
}


void reset_graph(void)
{
	memset(graph, 0, sizeof(int)*MAX_N*MAX_N);
	memset(input_map, 0, sizeof(int)*MAX_N*MAX_N);
}

 
// A DFS based recursive function that returns true if a
// matching for vertex u is possible
bool bpm(int u)
{
    for (int v = 1; v <= counter; v++){
        if (graph[u][v] && !seen[v]){
            seen[v] = true; 
 
            if (match[v] < 0 || bpm(match[v])){
                match[v] = u;
                return true;
            }
        }
    }
    return false;
}
 
// Returns maximum number of matching from M to N
int maxBPM(void)
{
    memset(match, -1, sizeof(match));
 
    int result = 0; 
    for (int u = 1; u <= counter; u++){
        memset(seen, 0, sizeof(seen));
 
        if (bpm(u)){
            result++;
        }
    }
    return result;
}


void print_map(void)
{
	int PRINT_MAX = counter+1;
	
	printf("  ");
	for(int i = 0; i <= PRINT_MAX; ++i){ 
		printf("%d ", i);
	}
	
	printf("\n");

	// print cost
	for(int i = 0; i <= PRINT_MAX; ++i){
		printf("%d ", i);
		for(int j = 0; j <= PRINT_MAX; ++j){
			if(input_map[i][j] == RAVEEN_CONST){
				printf("INF");
			} else if (input_map[i][j] > 0){
				printf("%d ", input_map[i][j]);
			} else {
				printf("  ");
			}
		}
		printf("\n");

	}
}

void print_graph(void)
{
	int PRINT_MAX = (counter+1);
	
	
	printf("   ");
	for(int i = 0; i <= PRINT_MAX; ++i){ 
		printf("%d ", i);
	}
	
	
	printf("\n");

	// print cost
	for(int i = 0; i <= PRINT_MAX; ++i){
		printf("%d  ", i);
		for(int j = 0; j <= PRINT_MAX; ++j){
			if(graph[i][j] == RAVEEN_CONST){
				printf("INF");
			} else {
				printf("%d ", graph[i][j]);		
			}
		}
		printf("\n");

	}
}