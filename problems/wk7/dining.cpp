#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <cstring>
#include <queue>


const int MAX_C = 210, MAX_F = 110, MAX_D = 110;
const int MAX_TOTAL = MAX_C + MAX_F + MAX_D+110;
const int RAVEEN_CONST = 1e9+7;

std::queue<int> q;
int graph[MAX_TOTAL][MAX_TOTAL];
int parent[MAX_TOTAL];
int flow[MAX_TOTAL];
int N,F,D, start, end, total, n;

void dining(void);
int fordFulkerson(void);
int bfs(void);
void print_graph(void);


int main(int argc, char* argv[])
{
	dining();

	return EXIT_SUCCESS;
}

void dining(void){

	int temp_F, temp_D, local_f, local_d;
	scanf("%d %d %d", &N, &F, &D);
	total = F+2*N+D+1;	// +1 is for 'sink' node
	start = 0;
	end = total;
	
	// set up the graph
	for(int i = 1; i <= F; ++i){graph[0][i]=1;} 				// set up source node
	for(int i = F+2*N+1; i <= F+2*N+D; ++i){ graph[i][total] = 1;}// set up sink node
	for(int i = 1; i <= N; ++i){ graph[F+2*i-1][F+2*i] = 1;}	// con. cows to 'themselves'

	for(int cow = 1; cow <=N; ++cow){
		scanf("%d %d", &temp_F, &temp_D);
		
		// food - need to be connected to cow_a
		while(temp_F--){
			scanf("%d", &local_f);
			graph[local_f][F+2*cow-1] = 1;
			//graph[cow+F][local_f] = 1;
		}
		// drinks need to be connected to sink (second cow - cow_b will connect to them)
		while(temp_D--){
			scanf("%d", &local_d);
			//graph[N+F+local_d][cow+F] = 1;
			graph[F+2*cow][F+2*N+local_d] = 1;
		}
	}


	printf("%d\n",fordFulkerson());
	//print_graph();


}

void init_graph()
{
	while(!q.empty())q.pop();
    memset(parent,-1,sizeof(parent));
}

// taken largely from geeks for geeks
int bfs(void)
{

	init_graph();

    parent[start]=0;
    flow[start]=RAVEEN_CONST;
    q.push(start);
    
    while(!q.empty())
    {
        int u =q.front();
        q.pop();
        if(u==end)break;


        for(int v=0;v<=total;v++)
        {
            if( v!=start && parent[v] == -1 && graph[u][v])
            {
                if(flow[u] < graph[u][v]){
                	flow[v] = flow[u];	
                } else {
                	flow[v] = graph[u][v];
                }
                q.push(v);
                parent[v]=u;
            }
        }
    }
    
    if(parent[end]==-1)return -1;
    return flow[end];

}

// taken from geeks for geeks
int fordFulkerson(void)
{

    int max_flow = 0;  // There is no flow initially
    int cost = 0, curr = 0, par = 0;
 	cost = bfs();

    while( cost != -1){
        
        max_flow+=cost;
        curr = end;
        
        while(curr != start){
        	//printf("par: %d, curr: %d, cost: %d\n", par, curr, cost);
        	par=parent[curr];
        	graph[par][curr]-=cost;
        	graph[curr][par]+=cost;
        	curr = par;
        }

        cost = bfs();
    }
 
    // Return the overall flow
    return max_flow;

}




void print_graph(void)
{
	int PRINT_MAX = 2*N+F+D+1;
	
	printf("\t");
	for(int i = 0; i <= PRINT_MAX; ++i){ 
		printf("%d\t", i);
	}
	
	printf("\n");

	// print cost
	for(int i = 0; i <= PRINT_MAX; ++i){
		printf("%d\t", i);
		for(int j = 0; j <= PRINT_MAX; ++j){
			if(graph[i][j] == RAVEEN_CONST){
				printf("INF\t");
			} else {
				printf("%d\t", graph[i][j]);
			}
		}
		printf("\n");

	}

}

