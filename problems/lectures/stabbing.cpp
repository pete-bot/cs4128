#include <iostream>
#include <algorithm>
#include <cstdlib>

using namespace std;

const static int INT_MAX = -200000000;

int main()
{
    int n;
    scanf("%d", &n );

    vector< pair<int,int>> intervals(n);

    for (int i = 0; i < n; ++i )
    {
        // we reverse the input order here, so that we can make use of the std sort
        scanf("%d%d", &intervals[i].second, &intervals[i].first);
    }

    // sorting by endpoint thanks to our inout arrangement (no predicate required)
    sort(intervals.begin(), intervals.end());

    int last = INT_MIN;


    // TODO: finish

    return EXIT_SUCCESS;
}
