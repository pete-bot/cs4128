#include <cstdio>
//#include <iostream>
#include <cstdlib>

#define ROOT 1
#define MAX_N 100010

// global vars
bool apples[MAX_N];     // bool - apple exists at that point
int parent[MAX_N];      // parent of current node (fork)
int count[MAX_N];       // cumulative count of nodes at point

void update_count(int node);
void update_count_value(int node, int value);


int main(int argc, char* argv[])
{

    int N, M, par, ch;
    scanf("%d", &N);
    //std::cin >> N;

    // init array of apples and count
    for(int i = 1; i <= N; ++i){
        apples[i] = true;
        count[i] = 1;
    }
    
    
    // read in variables, init parent array, update cumulative count
    for(int i = 1; i < N; ++i){
        scanf("%d %d", &par, &ch);
        //std::cin >> par >> ch;
        //std::cout << "par: " << par << ", " << "ch: " << ch << std::endl;
        parent[ch] = par;
        //update_count (par);

        while (par > 0){
            count[par]++;
            par = parent[par];
        }
    }

    /*
    std::cout << "count of fork" << std::endl;
    for (int i = 0; i < 10; ++i){
        std::cout << "fork: " << i << ", count: " << count[i] << std::endl;
    
    }
    */

    // read message
    scanf("%d",&M);
    //std::cin >> M;
    //std::cout << M << std::endl;
    char comm;
    int fork, value;

    for(int i = 0; i < M; ++i){
        scanf(" %c %d", &comm, &fork);
        //std::cin >> comm >> fork;

        // change apple state at fork
        if(comm == 'C'){
            if (apples[fork] == 0){
                value = 1;
                //update_count_value(fork, 1);
            } else {
                value = -1;
                //update_count_value(fork, -1);
            }

            while (fork > 0){
                count[fork] += value;
                fork = parent[fork];
            }



            apples[fork] = !apples[fork];

        // query apple count at fork
        } else {
            printf("%d\n", count[fork]);
            //std::cout << count[fork] << std::endl;
        }

    }

    return EXIT_SUCCESS;
}

void update_count(int node)
{
    while (node > 0){
        count[node]++;
        node = parent[node];
    }
}

void update_count_value(int node, int value)
{
    while (node > 0){
        count[node] += value;
        node = parent[node];
    }
}