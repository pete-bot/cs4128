
#include <iostream>
#include <cstdlib>
#include <map>
#include <iomanip>
#include <string>
#include <stack>
#include <cassert>
#include <queue>


typedef struct node_t *Node_ptr;

struct node_t{
	char element;
	Node_ptr left;
	Node_ptr right;
};

// functions
void process_string(const std::string& s);
int max (int a, int b);
void print_reverse_level_order(const Node_ptr& t);

std::stack<Node_ptr> stack;
std::stack<char> out;
std::queue<Node_ptr> q;

node_t nodes[10000];
int node_counter = 0;

int main(int argc, char* argv[])
{

	int num_lines = 0;
	std::string input;
	std::cin >> num_lines;

	while(num_lines--){
		std::cin >> input;
		process_string(input);
	}

	return EXIT_SUCCESS;
}

void process_string(const std::string& s)
{
	node_counter = 0;
	Node_ptr root = NULL;
	Node_ptr tmp = NULL;
	
	char c;
	for(unsigned int i = 0; i < s.size(); ++i ){
		c = s[i];
		
		
		tmp = &nodes[node_counter];
		node_counter++;
		tmp->element = c;
		tmp->left = NULL;
		tmp->right = NULL;

		// operator case 
		if(isupper(c)){
					
			tmp->left = stack.top();
			stack.pop();

			tmp->right = stack.top();
			stack.pop();
			
			stack.push(tmp);

		// 'lower case' case
		} else {
			stack.push(tmp);// push tmp on the stack 
		} 

	
	}

	root = stack.top();
	stack.pop();
	print_reverse_level_order(root);

	if(root != NULL){
		std::cout << std::endl;
	}
	//destroy_tree(root);

}



int max (int a, int b)
{
 return a > b? a : b; 
}

void print_reverse_level_order(const Node_ptr& root)
{
    if (root == NULL)  { return; }  // base case
    // enqueue root and initialize height
    q.push(root);
    Node_ptr current;
    while (!q.empty()) {
        current = q.front(); q.pop();
        
        out.push(current->element); 
        if (current->right != NULL) { q.push(current->right); }
        if (current->left != NULL) { q.push(current->left); }

    }
    
    while(!out.empty()){
    	std::cout << out.top();
    	out.pop();
    }

}
