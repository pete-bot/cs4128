#include <cstdlib>
#include <cassert>
#include <iostream>

#define N_MAX 30100


int parent[N_MAX];      // parent of node
int cubes_under[N_MAX]; // number of cubes under node, not including self
int stack_count[N_MAX]; // number of cubes above node, including self (total cubes in stack)

void init_parent(void);
int find_root(int a);
void move_cubes(int a, int b);

int main(int argc, char* argv[])
{
    init_parent();
    int P;
    std::cin >> P;

    char comm;
    int a, b;

    for(int i = 0; i < P; ++i){
        std::cin >> comm;
        if(comm == 'M'){
            std::cin >> a >> b;
            move_cubes(a, b);

        } else if (comm == 'C'){
            std::cin >> a;
            // update cubes at a
            find_root(a);
            std::cout << cubes_under[a] << std::endl;
        }
    }


    return EXIT_SUCCESS;
}

int find_root(int a)
{
    int tmp = parent[a];
    if(a != parent[a]){
        parent[a] = find_root(parent[a]);

        // apply path compression here
        cubes_under[a]+= cubes_under[tmp];
    }

    return parent[a];
}


void move_cubes(int a, int b)
{
    int a_root = find_root(a);
    int b_root = find_root(b);

    // prevent moving to own stack
    if(a_root == b_root){
        return;
    }

    parent[a] = b_root;

    cubes_under[a_root] += stack_count[b_root];
    stack_count[b_root] += stack_count[a_root];
}


void init_parent(void)
{
    for (int i = 0; i < N_MAX; ++i){
        parent[i] = i;
        stack_count[i] = 1;
    }
}

