#include <iostream>
#include <cstdlib>
#include <map>
#include <iomanip>
#include <string>


int main(int argc, char* argv[])
{
	// set precision value
	std::cout << std::fixed << std::setprecision(4);

	std::string input;
	std::map<std::string, int> forest;
	
	double treeCount = 0;
	std::pair<std::map<std::string,int>::iterator,bool> ret;
	while(std::getline(std::cin, input)){
	
		ret = forest.insert(std::make_pair(input, 0));
		(*ret.first).second++;
		treeCount++;

	}


	for(std::map<std::string,int>::iterator iter = forest.begin(); iter != forest.end(); ++iter){
		std::cout << iter->first << " " << 100.0*(static_cast<double>(iter->second)/treeCount) << std::endl;
	}


	return EXIT_SUCCESS;
}

