#include <cstdio>
//#include <iostream>
#include <cstdlib>
#include <cstring>

#define ROOT 1
#define MAX_N 100010

struct edge{
    int v;
    int next;
};

// global vars

int tree[MAX_N];
int label[MAX_N];
int parent[MAX_N];


int edge_count;
edge edges[MAX_N];
int left[MAX_N];
int right[MAX_N];
int id;

// fns
void update ( int i, int k);
int prefix_sum ( int i);
int get_fork_status(int fork);
void add_edge(int u, int v);
void build(int node, int pi);
int get_fork_status(int from, int to);

int main(int argc, char* argv[])
{


    int N, M, par, ch;
    scanf("%d", &N);

    memset(parent, -1, sizeof(parent));

    // read in variables, init parent array, update cumulative count
    for(int i = 1; i < N; ++i){
        scanf("%d %d", &par, &ch);
        par-=1;
        ch-=1;
        add_edge(par, ch);
    }


    // need to construct tree

    id = 0;
    // use DFS
    build(0,-1);
    memset(tree, 0, sizeof(tree) );

    // update label tree with values
    for(int i = 0; i < N; ++i ) update(label[i],1);






    // read message
    scanf("%d",&M);

    char comm;
    int fork;//, value;

    for(int i = 0; i < M; ++i){
        scanf(" %c %d", &comm, &fork);

        fork-=1;

        // change apple state at fork
        if(comm == 'C'){
            if (get_fork_status(label[fork], label[fork]) == 1){
                update(label[fork], -1);
            } else {
                update(label[fork], 1);
            }



        // query apple count at fork
        } else {
            printf("%d\n", get_fork_status(left[fork], right[fork]));
        }

    }

    return EXIT_SUCCESS;
}


// could optimise MAX_N here
void update ( int fork, int value) {
    fork+=1;
    while (fork <= MAX_N) {
        tree[fork] += value;
        fork += fork & (-fork);
    }
}


int prefix_sum( int fork) {
    int sum = 0;
    fork+=1;
    while (fork > 0) {
        sum += tree[fork];
        fork -= fork & (-fork);
    }
    return sum ;
}


int get_fork_status(int from, int to)
{
    return prefix_sum(to) - prefix_sum(from-1);
}


// need to think about

void add_edge(int u, int v)
{
    edges[edge_count].v = v;
    edges[edge_count].next = parent[u];
    parent[u] = edge_count++;
}

void build(int node, int pi){
  //printf("building\n");
  label[node] = left[node] = id++;
  for(int i = parent[node]; i!=-1; i = edges[i].next){
    int v = edges[i].v;
    if(v == pi) continue;
    build(v,node);
  }
  right[node] = id - 1;
}
