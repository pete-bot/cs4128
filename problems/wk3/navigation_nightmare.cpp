#include <iostream>
#include <cstdlib>  // abs
#include <cassert>
#include <vector>

#define MAX_FARMS 40010

#define x first
#define y second

typedef std::pair<int,int> point;

struct input_t {
    int start;
    int end;
    int distance;
    char direction;
};

// create our data structures
input_t inputs[MAX_FARMS];
int parents[MAX_FARMS];
int x_pos[MAX_FARMS];
int y_pos[MAX_FARMS];

// used for Find operation
point tmpDisplacement;

// fn prototypes
int union_find(int point);
int union_find_recursive(int point);
void process(int i);
int get_distance(int a, int b);
int absolute (int a);

// tests
void test_absolute(void);

int main( int argc, char* argv[]) {
    //test_absolute();

    // N is the number of lines, M is the number of connections between them
    int N, M;
    std::cin >> N >> M;
    //std::cout << "N: " << N << ", " << "M: " << M << std::endl;

    int start, end, distance;
    char direction;

    // read in the data
    for(int i = 1; i <= M; i++) {
        std::cin >> start >> end >> distance >> direction;

        input_t tmp = {start, end, distance, direction };

        // push the inputs to the array
        inputs[i] = tmp;
        // set the parent of each node to itself
        parents[i] = i;

        //update the x and y pos of each node to itself(easch is its own parent/root)
        x_pos[i] = 0;
        y_pos[i] = 0;
    }

    // process the queries
    int K;
    std::cin >> K;
    int F1, F2, I;
    int start_index = 1;

    for (int i = 1; i <= K; i++) {
        std::cin >> F1 >> F2 >> I;

        // skip case where the input must occur at 0 index
        if(I == 0) continue;

        // process inputs up to index I
        for (int j = start_index; j <= I; ++j) {
            process(j);
        }

        start_index = I;


        if ( union_find(F1) != union_find(F2) ) {
            std::cout << -1 << std::endl;
        } else {
            std::cout << get_distance(F1, F2) << std::endl;;
        }
    }

    return EXIT_SUCCESS;
}


int union_find(int point)
{
    tmpDisplacement.x = 0;
    tmpDisplacement.y = 0;
    return union_find_recursive(point);
}

// union find operation
int union_find_recursive(int point)
{
    // get position of parent
    int par_x =  x_pos[parents[point]];
    int par_y =  y_pos[parents[point]];

    // if point's parent is not itself
    // path compress
    if( parents[point] != point){
        int tmp = point;
        parents[point] = union_find_recursive(parents[point]);

        tmpDisplacement.x +=par_x;
        tmpDisplacement.y +=par_y;
        x_pos[tmp] += tmpDisplacement.x;
        y_pos[tmp] += tmpDisplacement.y;
    }
    return parents[point];
}

void process(int i)
{
    int start, end, distance;
    start = inputs[i].start;
    end = inputs[i].end;
    distance = inputs[i].distance;
    char direction = inputs[i].direction;

    // std::cout << start << ", " << end << ", " << distance
    //              << ", " << direction << std::endl;

    int parent_start = union_find(start);
    int parent_end = union_find(end);

    x_pos[parent_end] = x_pos[start] - x_pos[end];
    y_pos[parent_end] = y_pos[start] - y_pos[end];

    if(direction == 'N'){
        y_pos[parent_end] += distance;

    } else if (direction == 'E'){
        x_pos[parent_end] += distance;

    } else if (direction == 'S'){
        y_pos[parent_end] -= distance;

    } else if (direction == 'W'){
        x_pos[parent_end] -= distance;
    }

    parents[parent_end] = parent_start;
}

int get_distance(int start, int end)
{
    int x_dist = x_pos[end] - x_pos[start];
    int y_dist = y_pos[end] - y_pos[start];

    return absolute(x_dist) + absolute(y_dist);
}

int absolute (int a)
{
    return a < 0 ? -1*a : a;
}

void test_absolute(void)
{
    assert(absolute( -1) == 1);
    assert(absolute( 0) == 0);
    assert(absolute( 1) == 1);
    assert(absolute( -21) == 21);
    assert(absolute( -3) == 3);
    assert(absolute( 3) == 3);
    assert(absolute( 10) == 10);
    assert(absolute( -100) == 100);
}
