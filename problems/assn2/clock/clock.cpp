#include <bits/stdc++.h>

int main(int argc, char* argv[])
{

	int num = 0, count = 0;
	char temp[10];
	char min_hi, min_lo, sec_hi, sec_lo;

	while(scanf("%d", &num)!= EOF){
		while( count < num ){
			
			scanf(" %c%c:%c%c ", &min_hi, &min_lo, &sec_hi, &sec_lo);
			count++;

			printf("mins: %c%c, sec: %c%c\n", min_hi, min_lo, sec_hi, sec_lo);
		}
		count = 0;
	}


	return EXIT_SUCCESS;
}

