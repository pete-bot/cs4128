# fuzzer for Key Insight

from random import randint, choice, shuffle
from string import ascii_lowercase


def gen_testcase(block_size, n_blocks):
    ciphertext = ''
    plaintext = ''

    for _ in xrange(n_blocks):
        plaintext_block = ''.join(choice(ascii_lowercase[:8]) for _ in xrange(block_size))
        ciphertext_block = []

        for i in cipherkey:
            ciphertext_block.extend(plaintext_block[i])

        ciphertext += ''.join(ciphertext_block)
        plaintext += plaintext_block

    print block_size
    print plaintext
    print ciphertext


n_tests = 500  # randint(1, 5)

for _ in xrange(n_tests):
    block_size = randint(1, 33)
    n_blocks = randint(1, 3)

    cipherkey = range(block_size)
    shuffle(cipherkey)

    gen_testcase(block_size, n_blocks)
