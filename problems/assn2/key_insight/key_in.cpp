#include <bits/stdc++.h>
#include <array>

const int RAVEEN_CONST = 1e9+7;
const int MAX_K = 101;
const int ALPHA_MAX = 26;
std::array<std::vector<int>, MAX_K> P;

bool no_key, seen[ALPHA_MAX] = {false};
unsigned long long k, size, global_count;
std::string plain, cipher;

void print_key(void);
void substitution_explore(void);
void print_block_check(void);
long get_key_count(void);
void clean_vectors(void);
bool is_valid(void);
unsigned long fac(unsigned long a);



int main(int argc, char* argv[])
{

	while(std::cin >> k){
		std::cin >> plain >> cipher;

		size = plain.size();
		/*
		if(plain.size() != cipher.size()){
			std::cout << "Incompatible plain/cipher text." << std::endl;
			continue;
		}
		*/

		if(!is_valid()){
			std::cout << "0" << std::endl;
			continue;
		} 

		no_key = false;
		global_count = 0;

		// populate initial positions
		for(int i = 0; i < k; ++i, ++global_count){
			for(int j = 0; j < k; ++j){
				if(plain[i] == cipher[j]) P[i].push_back(j);
			}
		}

		//print_key();
		
		// need to check subsequent block match, iterate through remaining plain/cipher
		for(int i = global_count; i < size; i+=k){
			for(int j = i; j < i+k; ++j ){
				
				for(int p_it = 0; p_it < P[j%k].size(); ++p_it){
					
					if(plain[j] != cipher[i+P[j%k][p_it]]){
						P[j%k][p_it] = -1;
					}
				}

				auto it = std::remove(P[j%k].begin(), P[j%k].end(), -1);
				P[j%k].erase(it, P[j%k].end());
				
				if(P[j%k].size() == 0) {
					no_key = true; 
					break;
				}
			}
			if(no_key) break;
		}

		//print_key();

		if(no_key){
			std::cout << 0 << std::endl;
			clean_vectors();
			continue;
		}

		std::cout << get_key_count() << std::endl;
		clean_vectors();
	
	}


	return EXIT_SUCCESS;
}


unsigned long fac(unsigned long n)
{
	unsigned long ret = 1;
	for(unsigned int i = 1; i <= n; ++i){
	    ret *= i;
	}
	return ret;
}

void clean_vectors(void)
{
	for(int i = 0; i <= k; ++i){
		P[i].clear();
	}
	for (int i = 0; i < ALPHA_MAX; ++i){
		seen[i] = false;
	}
}

long get_key_count(void)
{
	//std::cout << "k: " << k << std::endl;
	int max = fac(P[0].size());
	seen[cipher[P[0][0]]-'a'] = true;
	//std::cout << "max: " << max << std::endl;
	for (int i = 1; i < k; ++i){
		//std::cout << "max: "<<max<<", P[" << i << "].size(): " 
		//	<< P[i].size() << std::endl;
		//if((signed int)P[i].size()>max) max = P[i].size(); 
		char tmp = cipher[P[i][0]];
		//std::cout << "checking: " << tmp << std::endl;
		if(!seen[cipher[P[i][0]]-'a']){
			max*=fac(P[i].size());
			seen[cipher[P[i][0]]-'a'] = true;
			//std::cout << "executing: " <<tmp << std::endl;
		
		}
		//std::cout << "max: " << max << std::endl;
	}
	return max; 

}


void print_block_check(void)
{
	for(int i = global_count; i < size; i+=k){
		std::cout << "checking block: ";
		for(int j = i; j < i+k; ++j ){
			std::cout << plain[j];
		}
		std::cout << std::endl;
	}

}

void print_key(void)
{
	// check remaining pos
	for(int i = 0; i < k; ++i){
		std::cout << "| ";
		for(int j = 0; j < P[i].size(); ++j){
			std::cout << P[i][j] << " ";
		}
		if(P[i].size() == 0) std::cout << "X ";
	}
	std::cout << std::endl; 
		
}

bool is_valid(void)
{
	if(plain.size() != cipher.size()) return false;

	std::string plain_tmp(plain);
	std::string cipher_tmp(cipher);
	
	std::sort(plain_tmp.begin(), plain_tmp.end() );
	std::sort(cipher_tmp.begin(), cipher_tmp.end());
	
	for(int i = 0; i < plain_tmp.size(); ++i){
		if(plain_tmp[i] != cipher_tmp[i]) return false;
	}

	//std::cout << plain_tmp << ", " << cipher_tmp << std::endl;

	return true;



}

void substitution_explore(void)
{
	std::cout << "block_size: " << k << std::endl; 
	for(int i = 0; i < plain.size(); ++i){std::cout << plain[i] << "\t";}
	std::cout << std::endl;
	// for(int i = 0; i < plain.size(); ++i){std::cout << "v\t";}
	// std::cout << std::endl;
	for(int i = 0; i < plain.size(); ++i){std::cout << cipher[i] << "\t";}
	std::cout << std::endl;
	
	for(int i = 0; i < plain.size(); ++i){
		std::cout << (plain[i]-'a') -(cipher[i]-'a') << "\t";

	}
	std::cout << std::endl;
	std::cout << std::endl;
}