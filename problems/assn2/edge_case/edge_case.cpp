#include <bits/stdc++.h>
//#include <cstdint>

const int MAX_N = 10010;



void solve();
std::string add (std::string &s1, std::string &s2);


int main()
{
	solve();

	return EXIT_SUCCESS;
}

void solve()
{
	std::string cache[MAX_N] = {"0", "0", "3", "4"};
	int last_calculated_index = 3; 	
	
	int in, count = 0;
	while(std::cin >> in){

		if(in <= last_calculated_index){	
			std::cout << cache[in]<<std::endl; 
			continue;
		}
	
		
		count  = last_calculated_index+1;
		while(count <= in){
			/*std::cout << "cache[" << count << "] = " <<
				cache[count-1] << "+" << cache[count-2] << std::endl;
			*/
			cache[count] = add(cache[count-1], cache[count-2]);
			count++;
		}

		last_calculated_index = count-1;
		//std::cout << "last_index: "<<last_calculated_index << std::endl;
		std::cout << cache[in]<<std::endl;
		


	} 




}

void print_set(std::string s[MAX_N]){

}

std::string add (std::string &s1, std::string &s2)
{
	int carry=0,sum,i;

	std::string  min=s1, max=s2, result = "";

	// Finds the bigger string
	if (s1.length()>s2.length()){
		max = s1;
		min = s2;
	} else {
		max = s2;
		min = s1;
	}

	// Fills the result for overlapping regions of sum
	for (i = min.length()-1; i>=0; i--){
		sum = min[i] + max[i + max.length() - min.length()] + carry - 2*'0';

		carry = sum/10;
		sum %=10;

		result = (char)(sum + '0') + result;
	}

	// Summates the previous carry and the remaining digits of bigger string
	i = max.length() - min.length()-1;

	while (i>=0){
		sum = max[i] + carry - '0';
		carry = sum/10;
		sum%=10;

		result = (char)(sum + '0') + result;
		i--;
	}

	// Adds the carry if remaining to the last digit e.g 999+1=1000
	if (carry!=0){
	 result = (char)(carry + '0') + result;
	}

	return result;
}