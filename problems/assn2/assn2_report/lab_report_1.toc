\contentsline {section}{\numberline {1}Introduction}{1}{section.1}
\contentsline {section}{\numberline {2}System Overview}{2}{section.2}
\contentsline {section}{\numberline {3}Hardware}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}Chassis}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Power Supply and Distribution}{6}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Control and Interfaces}{7}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Peripherals}{8}{subsection.3.4}
\contentsline {section}{\numberline {4}Software}{10}{section.4}
\contentsline {subsection}{\numberline {4.1}Operating system and ROS}{10}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Motor and Low Level Control}{11}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Manual Control Input}{12}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Laser Scan and Hector SLAM}{13}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Camera}{15}{subsection.4.5}
\contentsline {section}{\numberline {5}System Operation}{16}{section.5}
\contentsline {subsection}{\numberline {5.1}System Integration and Operation}{16}{subsection.5.1}
\contentsline {paragraph}{Set up hardware}{16}{section*.2}
\contentsline {section}{\numberline {6}Results and Conclusion}{22}{section.6}
\contentsline {subsection}{\numberline {6.1}Results}{22}{subsection.6.1}
\contentsline {paragraph}{Design Strengths}{22}{section*.3}
\contentsline {paragraph}{Design Weaknesses}{22}{section*.4}
\contentsline {paragraph}{Future Development}{23}{section*.5}
\contentsline {subsection}{\numberline {6.2}Conclusion}{23}{subsection.6.2}
