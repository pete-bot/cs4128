
Some of the overarching design and development decisions that we made at the start of the project were:

\begin{enumerate}
\item Developing and testing each node separately before combining them, to make debugging easier.

\item Developing the vision system ourselves, without hefty OpenCV libraries

\item Concurrent work on each node, aided by the separate development
\end{enumerate}


The structure of our solution can be seen in figure \ref{structure} and is outlined below.
%\begin{figure}[H]
%	\centering
%	\includegraphics[]{ai_flow.png}
%	\caption{AI flow}\label{structure}
%\end{figure}
There is an overarching control node, AI. This node receives the initial list of beacons to visit and dictates robot behaviour according to the flow shown in figure \ref{f2}. Initially, the robot explores the maze using a wall following algorithm in order to locate all beacons. If the next beacon to be visited is seen, the robot visits it before continuing exploration. Once all beacons have been located, and unvisited beacons are visited. All beacon visit paths are calculated using an A* path finding algorithm.

The vision node, Vision, subscribes to the $/camera/rgb\_image/color$ topic from the Kinect, and publishes to the $Beacon\_msg$ topic, which the AI subscribes to. The incoming frames are throttled by ROS to 10Hz, simply to ease processing, as we do not need to read images at 30fps. For each image, the vision node scans it for beacons and then publishes results to the topic. It also calculates the range and bearing of the beacon from the pixel coordinates. This process is outlined below in Section 2.1.

The motion node is in charge of waypoint-based motion. While the ai directly controls wall following motion, all A*-calculated paths are published as a series of map coordinates. The motion node transforms these to local coordinates and drives the robot there. We expand upon this in Section 2.4.

%% SAY SOMETHING ABOUT COST MAP





\begin{algorithm}[H]
		\caption{Creating a Cost Map from a SLAM map}
		\begin{algorithmic}
			\Function{CostMap}{$A,B$}
				\State
				\If{M[$A$.length][$B$.length] $>$ -1}
					\State \Return{M[$A$.length][$B$.length]}
				\ElsIf{$A$.length $>$ $B$.length}
					\State M[$A$.length][$B$.length] $\gets$ 0
					\State \Return 0 \Comment subsequence is longer - no match possible
				\ElsIf{$A$.length == 0}
					\State M[$A$.length][$B$.length] $\gets$ 1
					\State \Return 1 \Comment match found
				\Else
					\State $count \gets $ \Call{RecurseOccurences}{A, B[1:]} \Comment search again with first letter of B removed
					\If{$A$[1] == $B$[1]} \Comment possible match, remove first letters of both to look for it
						\State $count \gets count$ + \Call{RecurseOccurences}{A[1:],B[1:]}
					\EndIf
					\State M[$A$.length][$B$.length] $\gets count$
					\State \Return $count$
				\EndIf
			\EndFunction
		\end{algorithmic}
	\end{algorithm}
