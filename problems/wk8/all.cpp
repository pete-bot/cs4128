#include <cstdlib>
//#include <cstdio>
#include <iostream>
#include <string>

void all(void);
bool is_subsequence(std::string s, std::string t);

int main(int argc, char* argv[])
{
    all();
    return EXIT_SUCCESS;
}


void all(void)
{
    // get strings
    // check for subseq
    std::string s;
    std::string t;

    while(std::cin >> s >> t ){
        //std::cout << "s: "<< s << ", t: " << t << std::endl;
        if(is_subsequence(s,t)){
            std::cout << "Yes" << std::endl;
        } else {
            std::cout << "No"  << std::endl;
        }
    }


}


bool is_subsequence(std::string s, std::string t)
{
    int s_count = 0;
    for(int i = 0; i < t.size(); ++i){
        if(s[s_count] == t[i] ){
            //std::cout << "s["<< s_count <<"]: " <<s[s_count] << ", t[" << i << "]: " << t[i] << std::endl;
            ++s_count;
        }
    }

    if( s_count == s.size() ){return true;}

    return false;
}


