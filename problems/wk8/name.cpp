#include <cstdlib>
#include <iostream>
#include <queue>
#include <stack>
#include <string>
#define LINE "------------------------------------"

const int MAX_N = 400010;

std::string input;
int pre[MAX_N];

void name(void);
void kmp(void);
void print_results(void);

int main(int argc, char* argv[])
{
	name();
	return EXIT_SUCCESS;
}

void name(void)
{
	while(std::cin >> input){
		//std::cout << input << std::endl;
		kmp();
		print_results();
		
		//std::cout << LINE << std::endl; 
	}

}


/*
void kmp (void) {
	int n = input.size();

	pre[0] = pre[1] = 0;
	
	for (int i = 1; i<n ; i++) {
		
		int j = pre[i];
		
		while(j>0 && input[i] != input[j]){
			j = pre [j];
		}
		
		pre[i+1] = (input[i] == input[j]) ? j+1 : 0;
	}
}
*/

// use 'failure function' of KMP 
void kmp (void) {
	int n = input.size () ;
	
	for ( int i = 1; i < n ; i ++) {
		
		int j = i ;
		
		while ( j > 0 && input[i] != input[pre[j-1]]){
			j = pre [j-1];
		}

		pre[i] = (j>0) ? pre[j-1]+1 : 0;
	}
}


void print_results(void)
{
	int i = input.size();
	std::stack<int> p_stack;
	p_stack.push(i);
	while(pre[i-1] != 0){
		p_stack.push(pre[i-1]);
		i = pre[i-1]; 
	}

	while(!p_stack.empty()){
		std::cout << p_stack.top();
		p_stack.pop();
		if(!p_stack.empty()){std::cout<< " ";}
	}
	std::cout << std::endl;
}
