#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <vector>

const int PRIME = 15013;

const int MAX_N = 100010;


std::vector<int> hash[PRIME];
int snowflakes[MAX_N][6];
int sum[MAX_N];
int n;


void snow(void);
bool equal_flake(int a, int b);
void print_flake(int a[]);
void print_flakes(void);


int main(int argc, char* argv[])
{
	snow();

	return EXIT_SUCCESS;
}

void snow(void)
{
	int sum;
	scanf("%d", &n);
	for(int i = 0; i < n; ++i){

		scanf("%d %d %d %d %d %d", 
			&snowflakes[i][0], &snowflakes[i][1], &snowflakes[i][2], 
			&snowflakes[i][3], &snowflakes[i][4], &snowflakes[i][5]);
			
			sum = snowflakes[i][0]+ snowflakes[i][1]+ snowflakes[i][2]+ 
			snowflakes[i][3]+ snowflakes[i][4]+ snowflakes[i][5];

			sum = sum%PRIME;
			hash[sum].push_back(i);
	}

	bool result = false;
	for(int i = 0; i < PRIME; ++i){
		for(int j = 0; j < hash[i].size(); ++j){
			for(int k = j+1; k < hash[i].size(); ++k){
				result = equal_flake(hash[i][j], hash[i][k]);
				if(result) break;
			}
			if(result) break;

		}
		if (result) break;

	}

	
	if (result){
		printf("Twin snowflakes found.\n");
	}else {
		printf("No two snowflakes are alike.\n");
	}

	//print_flakes();

}

bool equal_flake(int a, int b)
{
	
	// check forward
	// check outer pos
	int count = 0; 
	for(int i = 0; i < 6; ++i){
		// printf("::\n");
		for(int j = i; count < 6; j=(j+1)%6, count++){
			// printf("checking string %d\n", j);
			// printf("comp: %d == %d\n", snowflakes[a][count], snowflakes[b][j] );
			if(snowflakes[a][count] != snowflakes[b][j]){
				break;
			}
			if( count == 5 ) { 
				//print_flake(snowflakes[a]);
				//print_flake(snowflakes[b]);
				return true;

			}
		}
		count = 0;
	}

	count = 0;
	// check reverse
	for(int i =	 0; i < 6; ++i){
		//printf("::\n");
		for(int j = i; count < 6; j=((j-1)+6)%6, count++){
			//printf("checking string %d\n", j);
			//printf("comp: %d == %d\n", a[count], b[j] );

			if(snowflakes[a][count] != snowflakes[b][j]){
				break;
			}
			if( count == 5 ) {
				//print_flake(snowflakes[a]);
				//print_flake(snowflakes[b]);
				return true;
			}
				
		}
		count = 0;
	}


	return false;
}


void print_flake(int a[]){
	for(int i = 0; i < 6; ++i){
		printf("%d ",a[i]);

	}
	printf("\n");
}

void print_flakes(void)
{
	for(int i = 0; i < n; ++i){
		printf("%d %d %d %d %d %d\n",  
			snowflakes[i][0], snowflakes[i][1], snowflakes[i][2], 
			snowflakes[i][3], snowflakes[i][4], snowflakes[i][5]);
	}
}