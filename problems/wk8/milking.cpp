
#include <cstdio>
#include <algorithm>
#include <cstring>

#define REP(i, n) for (int i = 0; i < (int)(n); ++i)

const int RAVEEN_CONST = 1e9+7; 
const int MAXN = 20010;
char S[MAXN];
int num_elts, num_times;
int N, gap;
int sa[MAXN], pos[MAXN], tmp[MAXN], lcp[MAXN];

void solve(void);
bool sufCmp(int i, int j);
void buildSA();
void buildLCP();
void print_lcp(void);
void get_result(void);




int main(int argc, char* argv[])
{
	solve();

	return EXIT_SUCCESS;
}


void solve(void)
{

	scanf("%d %d", &num_elts, &num_times);
	//printf("N: %d, K: %d\n", num_elts, num_times);
	int i;
	for(i = 0; i < num_elts; ++i){
		scanf("%c", &S[i]);
		scanf("%c", &S[i]);
	}
	S[++i] = '\0';

	//printf("input: %s\n", S);

	buildSA();
	buildLCP();

	//print_lcp();
	get_result();

}

void get_result(void)
{
	int max = -RAVEEN_CONST;
	for(int i = 0; i < num_elts; ++i){
		if(lcp[i] > max) max = lcp[i];
	}
	printf("%d\n", max);
}


void print_lcp(void)
{

	for(int i = 0; i < num_elts; ++i){
		printf("%d: %d\n",i, lcp[i]);
	}

}


bool sufCmp(int i, int j)
{
	if (pos[i] != pos[j])
		return pos[i] < pos[j];
	i += gap;
	j += gap;
	return (i < N && j < N) ? pos[i] < pos[j] : i > j;
}


void buildSA()
{
	N = num_elts;
	REP(i, N) sa[i] = i, pos[i] = S[i];
	for (gap = 1;; gap *= 2)
	{
		std::sort(sa, sa + N, sufCmp);
		REP(i, N - 1) tmp[i + 1] = tmp[i] + sufCmp(sa[i], sa[i + 1]);
		REP(i, N) pos[sa[i]] = tmp[i];
		if (tmp[N - 1] == N - 1) break;
	}
}


void buildLCP()
{
	for (int i = 0, k = 0; i < N; ++i){ 
		if (pos[i] <= N - num_times){
			for (int j = sa[pos[i] + num_times - 1]; 
				S[i + k] == S[j + k];)
			++k;
			lcp[pos[i]] = k;
			if(k>0) --k;
		}
	}
}
