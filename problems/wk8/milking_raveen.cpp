#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <algorithm>

const int MAXN = 20010;
char s[MAXN];
int pos[MAXN], rank[MAXN], tmp[MAXN], lcp[MAXN];
int cmpsz, length;
int num_elts, num_times;


void construct_sa (void);
bool sufcmp (int i, int j);
void solve(void);

int main(int argc, char* argv[])
{
	solve();


	return EXIT_SUCCESS;
}



void solve(void)
{

	scanf("%d %d", &num_elts, &num_times);
	printf("N: %d, K: %d\n", num_elts, num_times);
	int i;
	for(i = 0; i < num_elts; ++i){
		scanf("%c", &s[i]);
		while(s[i] == 10 || s[i] == 12 || s[i] == 13 ){
			scanf("%c", &s[i]);
		}
	}
	s[++i] = '\0';

	printf("input: %s\n", s);

	construct_sa();

	for(int i = 0; i < num_elts; ++i){
		printf("%d: %d\n",i, lcp[i]);
	}

}


bool sufcmp (int i, int j) {
return rank [ i ] == rank [ j ] ? (i+cmpsz < length && j+cmpsz <
	length ? rank[i+cmpsz]<rank[j+cmpsz] : i>j) : rank[i] < rank[j];
}

void construct_sa (void) {
	length = num_elts;
	for ( int i = 0; i < length ; i ++) {
		pos [ i ] = i ;
		rank [ i ] = s [ i ];
	}


	for ( cmpsz = 1; cmpsz >> 1 < length ; cmpsz += cmpsz ) {
		std::sort( pos, pos+length ,sufcmp) ;
		for ( int i = 1; i < length ; i ++)
			tmp[i] = tmp[i-1] + sufcmp( pos [i-1], pos[i]);
		for ( int i = 0; i < length ; i ++)
			rank[ pos[i]] = tmp[i];
	}

	lcp [0] = 0;
	for ( int i = 0, h = 0; i<length ; i++){
		if ( rank[i] > 0) {
			for ( int j = pos[rank[i]-1]; s[i+h] == s[j+h]; h++) ;
			lcp[rank[i]] = h ;
			
			if ( h > 0) h--;
			
		}
	}

}
